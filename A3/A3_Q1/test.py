import variable_elimination as ve
#####################################
# Cancer Example, used for debugging
#####################################
# f0 = [
#     ['M', "Value"],
#     [True, 0.08],
#     [False, 0.92],
# ]
#
# f1 = [
#     ['C', "Value"],
#     [True, 0.32],
#     [False, 0.68],
# ]
#
# f2 = [
#     ['C', 'A', "Value"],
#     [True, True, 0.8],
#     [True, False, 0.2],
#     [False, True, 0.15],
#     [False, False, 0.85]
# ]
#
# f3 = [
#     ['M', 'C', 'B', "Value"],
#     [True, True, True, 0.61],
#     [True, True, False, 0.39],
#     [True, False, True, 0.52],
#     [True, False, False, 0.48],
#     [False, True, True, 0.78],
#     [False, True, False, 0.22],
#     [False, False, True, 0.044],
#     [False, False, False, 0.956],
# ]
#
#
# f4 = [
#     ['B', 'R', "Value"],
#     [True, True, 0.98],
#     [True, False, 0.02],
#     [False, True, 0.01],
#     [False, False, 0.99]
# ]
#
# f5 = [
#     ['R', 'D', "Value"],
#     [True, True, 0.96],
#     [True, False, 0.04],
#     [False, True, 0.001],
#     [False, False, 0.999]
# ]
#
# factors = [f0, f1, f2, f3, f4, f5]
# query_vars = ["M"]
# hidden_vars = ["M", "C", "A", "B", "R", "D"]
# evidences = [["D", True], ["A", False]]
#
# ve.pretty_print_factor(ve.inference(factors, query_vars, hidden_vars, evidences))
######################################################################################
f0 = [
    ["Trav", "Value"],
    [True, 0.05],
    [False, 0.95],
]

f1 = [
    ["Trav", "Fraud", "Value"],
    [True, True, 0.01],
    [True, False, 0.99],
    [False, True, 0.004],
    [False, False, 0.996]
]

f2 = [
    ["Trav", "Fraud", "FP", "Value"],
    [True, True, True, 0.9],
    [True, True, False, 0.1],
    [True, False, True, 0.9],
    [True, False, False, 0.1],
    [False, True, True, 0.1],
    [False, True, False, 0.9],
    [False, False, True, 0.01],
    [False, False, False, 0.99],
]

f3 = [
    ["Fraud", "OC", "IP", "Value"],
    [True, True, True, 0.02],
    [True, True, False, 0.98],
    [True, False, True, 0.011],
    [True, False, False, 0.989],
    [False, True, True, 0.01],
    [False, True, False, 0.99],
    [False, False, True, 0.001],
    [False, False, False, 0.999],
]

f4 = [
    ["OC", "Value"],
    [True, 0.6],
    [False, 0.4],
]

f5 = [
    ["OC", "CRP", "Value"],
    [True, True, 0.1],
    [True, False, 0.9],
    [False, True, 0.01],
    [False, False, 0.99]
]


factors = [f0, f1, f2, f3, f4, f5]
hidden_vars = ["Trav", "FP", "Fraud", "IP", "OC", "CRP"]
query_vars = ["Fraud"]
evidences = [["FP", False], ["IP", True], ["CRP", True]]

result = ve.inference(factors, query_vars, hidden_vars, evidences)
print("Result: ")
ve.pretty_print_factor(result)
