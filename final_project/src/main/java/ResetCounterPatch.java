import com.evacipated.cardcrawl.modthespire.lib.SpireInsertPatch;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch;
import com.megacrit.cardcrawl.actions.common.EndTurnAction;

@SpirePatch(
        clz = EndTurnAction.class,
        method = "update",
        paramtypez = {}
)
public class ResetCounterPatch {

    @SpireInsertPatch(
            rloc = 0
    )
    public static void ResetCount(EndTurnAction __instance) {
        AutoPlayerPatch.count = 0;
        AutoPlayerPatch.done = false;
        AutoPlayerPatch.started = false;
        AutoPlayerPatch.turn_over = false;
    }
}
