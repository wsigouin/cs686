import com.evacipated.cardcrawl.modthespire.lib.SpireInsertPatch;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch;
import com.megacrit.cardcrawl.characters.AbstractPlayer;

@SpirePatch(
        clz = AbstractPlayer.class,
        method = "onVictory",
        paramtypez = {}
)
public class ResetCombatPatch {

    @SpireInsertPatch(
            rloc = 0
    )
    public static void ResetCount(AbstractPlayer __instance) {
        AutoPlayerPatch.count = 0;
        AutoPlayerPatch.done = false;
        AutoPlayerPatch.started = false;
        AutoPlayerPatch.turn_over = false;
    }
}
