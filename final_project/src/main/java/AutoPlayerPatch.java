import STSSimulator.SimulatorUtility;
import com.evacipated.cardcrawl.modthespire.lib.*;
import com.evacipated.cardcrawl.modthespire.patcher.PatchingException;
import com.megacrit.cardcrawl.actions.GameActionManager;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.rooms.AbstractRoom;
import javassist.CannotCompileException;
import javassist.CtBehavior;

import java.util.ArrayList;

@SpirePatch(
        clz = GameActionManager.class,
        method = "update",
        paramtypez = {}
)
public class AutoPlayerPatch {

    static int count = 0;
    static boolean done = false;
    static boolean started = false;
    static boolean turn_over = false;

    @SpireInsertPatch(
            locator = Locator.class
    )
    public static void AutoPlay(GameActionManager __instance) {
        if (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT && AbstractDungeon.getCurrRoom().monsters.monsters.size() != 0 && !AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
            if (count >= 30 && !turn_over) {
                count++;


                if (!done && !started) {
                    SimulatorUtility.init();
                    System.out.println("Deciding....");
                    started = true;
                    AutoPlayerThread thread = new AutoPlayerThread(true);
                    thread.start();
                } else if (done) {
                    System.out.println("Done!");

                    count = 0;
                    done = false;
                    started = false;
                }


//                long currTime = System.currentTimeMillis();
//
//                ArrayList<MonteCarlo.SimulationThread> threads = new ArrayList<>();
//                ArrayList<GameState> states = new ArrayList<>();
//                for(int i = 0; i < numTasks; i++){
//                    states.add(rootState.deepClone());
//                }
//
//                for(int i = 0; i < numTasks; i++){
//                    MonteCarlo.SimulationThread newThread = new MonteCarlo.SimulationThread(i, states.get(i), null, 1000, 100);
//                    threads.add(newThread);
//                    newThread.start();
//                }
//
//                for (Thread thread : threads) {
//                    thread.join();
//                }
//                System.out.println(System.currentTimeMillis() - currTime);
//                System.out.println(bestRun);

//                boolean cardPlayed = true;
//                while(cardPlayed) {
//                    cardPlayed = false;
//                    for (AbstractCard card : cards) {
//                        if (card.costForTurn <= currEnergy && card.canUse(player, monsters.getRandomMonster())) {
//                            if (card.type == AbstractCard.CardType.ATTACK) {
//                                DamageInfo dInfo = new DamageInfo(player, card.damage, card.damageTypeForTurn);
//                                cardPlayed = true;
//                                System.out.println("Playing " + card.name);
//
//                            } else {
//                                card.use(player, null);
//                                currEnergy -= card.costForTurn;
//                                cardPlayed = true;
//                                System.out.println("Playing " + card.name);
//                            }
//                            cards.remove(card);
//                            player.cardsPlayedThisTurn++;
//                            break;
//                        }
//
//                    }
//                }
//                System.out.println("Would end turn now. " + cards.size());

            } else {
                count++;
            }
        }
    }


    private static class Locator extends SpireInsertLocator {
        public int[] Locate(CtBehavior ctMethodToPatch) throws CannotCompileException, PatchingException {
            Matcher finalMatcher = new Matcher.MethodCallMatcher(
                    "com.megacrit.cardcrawl.actions.GameActionManager", "getNextAction");

            return LineFinder.findInOrder(ctMethodToPatch, new ArrayList<Matcher>(), finalMatcher);
        }
    }


}
