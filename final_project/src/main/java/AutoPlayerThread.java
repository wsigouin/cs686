import MonteCarlo.MCAction;
import MonteCarlo.MCTree;
import MonteCarlo.PlayableAction;
import STSSimulator.GameState;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;


public class AutoPlayerThread extends Thread {
    private boolean autoplay;

    public AutoPlayerThread(boolean autoplay){
        this.autoplay = autoplay;
    }


    @Override
    public void run() {
        try {
            MCTree tree = new MCTree(new GameState(AbstractDungeon.player, AbstractDungeon.getMonsters()));

            long currTime = System.currentTimeMillis();

            PlayableAction a = tree.search();
            if(autoplay) {

                System.out.println(System.currentTimeMillis() - currTime);

                if (a.action == MCAction.Action.PLAY_CARD) {
                    System.out.print("Play " + AbstractDungeon.player.hand.group.get(a.card).name);
                    if (a.target != -1) {
                        System.out.print(" on " + AbstractDungeon.getMonsters().monsters.get(a.target).name + "[" + a.target + "]\n");
                        AbstractDungeon.player.useCard(AbstractDungeon.player.hand.group.get(a.card), AbstractDungeon.getMonsters().monsters.get(a.target), 0);
                    } else {
                        AbstractDungeon.player.useCard(AbstractDungeon.player.hand.group.get(a.card), null, 0);
                        System.out.println();
                    }
                } else {
                    AutoPlayerPatch.turn_over = true;
                    AbstractDungeon.getCurrRoom().endTurn();
                    System.out.println("End Turn");
                }
            }
            else{
                AutoPlayerPatch.turn_over = true;
            }

        } catch (InterruptedException | IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
            return;
        }

        AutoPlayerPatch.done = true;

    }


}
