package MonteCarlo;

import STSSimulator.GameState;

import java.util.ArrayList;

public class MCNode implements Comparable {
    public GameState state;
    public MCAction action;
    public double reward;
    public MCNode parent;
    public ArrayList<MCNode> children = new ArrayList<>();
    public boolean isTerminal;
    public int depth;

    public MCNode(MCAction action, GameState state, MCNode parent, int depth) {
        this.action = action;
        this.state = state;
        this.parent = parent;
        isTerminal = state.player.isDead || state.monsters.getAliveMonsters().size() == 0;
        this.depth = depth;

    }

    public MCNode hasChild(MCNode node) {
        for (MCNode child : children) {
            if (child.compareTo(node) == 0) {
                return child;
            }
        }
        return null;
    }

    @Override
    public int compareTo(Object o) {
        MCNode otherNode = (MCNode) o;

        if (this.action.compareTo(otherNode.action) != 0 || this.state.compareTo(otherNode.state) != 0) {
            if (this.reward > otherNode.reward) {
                return 1;
            } else {
                return -1;
            }
        }

        return 0;
    }

    @Override
    public String toString() {

        switch (action.action) {
            case PLAY_CARD:
                if (action.target != -1) {
                    return "\t" + parent.state.player.hand.getPlayableCards().get(action.card).name + " on " + parent.state.monsters.getAliveMonsters().get(action.target).name + " with value " + this.reward;
                } else {
                    return "\t" + parent.state.player.hand.getPlayableCards().get(action.card).name + " with value " + this.reward;
                }
            case END_TURN:
                return "\tEnded Turn with value " + this.reward;
            case NONE:
                return "None";
        }
        return "??";
    }


}
