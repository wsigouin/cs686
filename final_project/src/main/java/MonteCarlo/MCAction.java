package MonteCarlo;

public class MCAction implements Comparable {
    public int target = -1;
    public Action action;
    public int card;

    public MCAction(Action action) {
        this.action = action;
    }

    public MCAction(Action action, int card) {
        this(action);
        this.card = card;
    }

    public MCAction(Action action, int card, int target) {
        this(action, card);
        this.target = target;
    }

    @Override
    public int compareTo(Object o) {
        MCAction otherAction = (MCAction) o;
        if (this.card != otherAction.card || this.target != otherAction.target || this.action != otherAction.action) {
            return -1;
        }
        return 0;
    }

    public enum Action {
        PLAY_CARD,
        END_TURN,
        NONE
    }

}
