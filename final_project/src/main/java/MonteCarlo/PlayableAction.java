package MonteCarlo;

public class PlayableAction {
    public MCAction.Action action;
    public int card;
    public int target;

    public PlayableAction(MCAction.Action action, int card, int target) {
        this.action = action;
        this.card = card;
        this.target = target;
    }


}
