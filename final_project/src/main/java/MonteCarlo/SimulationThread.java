package MonteCarlo;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;

import java.util.ArrayList;

public class SimulationThread extends Thread {

    private final int id;
    private final GameState state;
    private final int maxRunDepth;
    private ArrayList<Double> result;
    private final int maxRuns;
    int actionCount;

    public SimulationThread(int id, GameState state, ArrayList<Double> result, int maxRuns, int maxRunDepth, int actionCount) {
        this.id = id;
        this.state = state;
        this.result = result;
        this.maxRuns = maxRuns;
        this.maxRunDepth = maxRunDepth;
        this.actionCount = actionCount;
    }

    @Override
    public void run() {
        GameState leafState = state.deepClone();
        int worstState = 100;
        double totalState = 0;
        for (int j = 0; j < maxRuns; j++) {
            int turnCount = 0;
            for (int i = 0; i < maxRunDepth; i++) {
                if(MCTree.verbose) {
                    System.out.println("----------------" + turnCount + "--------------------------");
                    System.out.println(leafState);
                }
                turnCount++;
                try {
                    if (!leafState.player.takeRandomTurn(i == 0)) {
                        break;
                    }


                    if (!leafState.monsters.takeTurn()) {
                        break;
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                    return;
                }
            }

            totalState += (leafState.player.currentHealth * 3) - turnCount + (leafState.player.gold);

            leafState = state.deepClone();
        }
        result.set(id, totalState);
    }


}
