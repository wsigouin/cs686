package MonteCarlo;

import STSSimulator.GameState;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.random.Random;

import java.util.ArrayList;

public class MCTree {
    public MCNode root;
    private final int NUM_ITERATIONS = 100;
    private Random random = new Random();
    private final int NUM_THREADS = 8;
    private final int OPS_PER_THREAD = 50;
    private final int MAX_DEPTH = 100;
    public static boolean verbose = false;


    public MCTree(GameState state) {
        this.root = new MCNode(new MCAction(MCAction.Action.NONE), state, null, 0);
    }


    public PlayableAction search() throws InterruptedException {
        if (root.state.player.hand.getPlayableCards().size() == 0) {
            return new PlayableAction(MCAction.Action.END_TURN, -1, -1);
        }
        long minTime = Long.MAX_VALUE;
        long totalTime = 0;
        for (int i = 0; i < NUM_ITERATIONS; i++) {
            long startTime = System.currentTimeMillis();
            MCNode node = treePolicy();

            simulate(node);
            long runtime =  System.currentTimeMillis()  - startTime;
            if(runtime < minTime){
                minTime = runtime;
            }
            totalTime += runtime;
        }

        root.children.sort(MCNode::compareTo);
        System.out.println(root.isTerminal);
        System.out.println("Root children " + root.children.size());
        for (MCNode child : root.children) {
            System.out.println(child);
        }
        System.out.println(NUM_ITERATIONS + " average time : " + totalTime/NUM_ITERATIONS + " best time " + minTime );
        return getPlayableAction(root.children.get(root.children.size() - 1).action);
    }


    private MCNode treePolicy() {
        MCNode currNode = root;
        while (!currNode.isTerminal) {
            MCAction action = getNextAction(currNode.state);
            MCNode newChild = new MCNode(action, currNode.state.applyAction(action), currNode, currNode.depth + 1);

            MCNode potentialChild = currNode.hasChild(newChild);
            if (potentialChild == null) {
                currNode.children.add(newChild);
                return newChild;
            } else {
                currNode = potentialChild;
            }
        }

        return currNode;
    }

    private MCAction getNextAction(GameState state) {
        ArrayList<AbstractCardAP> playableCards = state.player.hand.getPlayableCards();
        ArrayList<AbstractMonsterAP> monsters = state.monsters.getAliveMonsters();

        while (true) {
            int card_random = random.random.nextInt(playableCards.size() + 1);

            if (card_random >= playableCards.size()) {
                return new MCAction(MCAction.Action.END_TURN);
            } else {
                if (playableCards.get(card_random).target == AbstractCard.CardTarget.ENEMY) {
                    int monster_random = random.random.nextInt(monsters.size());
                    if (state.player.hand.getPlayableCards().get(card_random).canUse(state.player, monsters.get(monster_random))) {
                        return new MCAction(MCAction.Action.PLAY_CARD, card_random, monster_random);
                    } else {
                        continue;
                    }

                }
                return new MCAction(MCAction.Action.PLAY_CARD, card_random, -1);
            }
        }

    }

    private void simulate(MCNode node) throws InterruptedException {

        ArrayList<SimulationThread> threads = new ArrayList<>();
        ArrayList<GameState> states = new ArrayList<>();
        ArrayList<Double> vals = new ArrayList<>();

        for (int i = 0; i < NUM_THREADS; i++) {
            states.add(node.state.deepClone());
            vals.add(0.0);
        }

        for (int i = 0; i < NUM_THREADS; i++) {
            SimulationThread newThread = new SimulationThread(i, states.get(i), vals, OPS_PER_THREAD, MAX_DEPTH, node.depth);
            threads.add(newThread);
            newThread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }


        double total = 0;

        for (Double val : vals) {
            total += val;
        }

        node.reward = total / (NUM_THREADS * OPS_PER_THREAD);


    }

    private PlayableAction getPlayableAction(MCAction action) {
        if (action.action == MCAction.Action.END_TURN) {
            return new PlayableAction(action.action, -1, -1);
        } else {
            AbstractCardAP card = root.state.player.hand.getPlayableCards().get(action.card);
            AbstractMonsterAP target;
            if (action.target != -1) {
                target = root.state.monsters.getAliveMonsters().get(action.target);
                return new PlayableAction(action.action, root.state.player.hand.group.indexOf(card), root.state.monsters.monsters.indexOf(target));
            } else {
                return new PlayableAction(action.action, root.state.player.hand.group.indexOf(card), -1);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        for (MCNode child : root.children) {
            if (child.action.action == MCAction.Action.PLAY_CARD) {
                ret.append(child.parent.state.player.hand.getPlayableCards().get(child.action.card).name);
                if (child.action.target != -1) {
                    ret.append(" on ").append(child.parent.state.monsters.getAliveMonsters().get(child.action.target).name).append("[").append(child.action.target).append("]");
                }
            } else {
                ret.append("End Turn");
            }
            ret.append(" ").append(child.reward).append("\n");
        }
        return ret.toString();
    }

    private void backprop(MCNode node) {
        MCNode currNode = node.parent;
        while (currNode != null && currNode != root) {
            double total = 0;
            for (MCNode child : currNode.children) {
                total += child.reward;
            }

            currNode.reward = total / currNode.children.size();
        }
    }


}
