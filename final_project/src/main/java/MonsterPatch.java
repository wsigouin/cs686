//import com.badlogic.gdx.math.MathUtils;
//import com.evacipated.cardcrawl.modthespire.lib.*;
//import com.evacipated.cardcrawl.modthespire.patcher.PatchingException;
//import com.megacrit.cardcrawl.actions.GameActionManager;
//import com.megacrit.cardcrawl.cards.AbstractCard;
//import com.megacrit.cardcrawl.cards.CardQueueItem;
//import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
//import com.megacrit.cardcrawl.monsters.AbstractMonster;
//import com.megacrit.cardcrawl.rooms.AbstractRoom;
//import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
//import javassist.CannotCompileException;
//import javassist.CtBehavior;
//
//import java.lang.reflect.Field;
//import java.util.ArrayList;
//
//@SpirePatch(
//        clz= AbstractMonster.class,
//        method="calculateDamage",
//        paramtypez = {int.class}
//)
//public class MonsterPatch {
//    @SpireInsertPatch(
//            locator = Locator.class,
//            localvars = {"this.intentDmg", "this.intentMultiAmt", "this.isMultiDmg", "tmp"}
//
//    )
//    public static void PatchMonster(AbstractMonster __instance, int dmg, int intentDmg, int intentMultiAmt, boolean isMultiDmg, float tmp) {
//        int index = -1;
//
//        for (AbstractMonster monster : AbstractDungeon.getCurrRoom().monsters.monsters){
//            if(monster == __instance){
//                index = AbstractDungeon.getCurrRoom().monsters.monsters.indexOf(monster);
//                break;
//            }
//        }
//
//
//        dmg = MathUtils.floor(tmp);
//        if (dmg < 0) {
//            dmg = 0;
//        }
//
//        while(AutoPlayerPatch.monsterDamage.size() <= AbstractDungeon.getCurrRoom().monsters.monsters.size()){
//            AutoPlayerPatch.monsterDamage.add(0);
//        }
//
//        if(isMultiDmg) {
//            AutoPlayerPatch.monsterDamage.set(index, dmg * intentMultiAmt);
//        }
//        else {
//            AutoPlayerPatch.monsterDamage.set(index, dmg);
//        }
//        System.out.println("Monster " + index + " doing " + dmg);
//
//    }
//
//        private static class Locator extends SpireInsertLocator {
//        public int[] Locate(CtBehavior ctMethodToPatch) throws CannotCompileException, PatchingException {
//            Matcher finalMatcher = new Matcher.MethodCallMatcher(
//                    "com.badlogic.gdx.math.MathUtils", "floor");
//
//            return LineFinder.findInOrder(ctMethodToPatch, new ArrayList<Matcher>(), finalMatcher);
//        }
//    }
//
//    //    @SpireInsertPatch(
////            locator = Locator.class,
////            localvars = {"tmp"}
////
////    )
////    public static void PatchMonster(AbstractMonster __instance, int dmg, float tmp) {
////        int index = -1;
////
////        for (AbstractMonster monster : AbstractDungeon.getCurrRoom().monsters.monsters){
////            if(monster == __instance){
////                index = AbstractDungeon.getCurrRoom().monsters.monsters.indexOf(monster);
////                break;
////            }
////        }
////
////        dmg = MathUtils.floor(tmp);
////        if (dmg < 0) {
////            dmg = 0;
////        }
////
////        AutoPlayerPatch.monsterDamage.set(index, dmg);
////    }
////
////    private static class Locator extends SpireInsertLocator {
////        public int[] Locate(CtBehavior ctMethodToPatch) throws CannotCompileException, PatchingException {
////            Matcher finalMatcher = new Matcher.MethodCallMatcher(
////                    "com.badlogic.gdx.math.MathUtils", "floor");
////
////            return LineFinder.findInOrder(ctMethodToPatch, new ArrayList<Matcher>(), finalMatcher);
////        }
////    }
//}
