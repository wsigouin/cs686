package STSSimulator.relics;

import STSSimulator.GameState;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.relics.AbstractRelic;

public abstract class AbstractRelicAP extends AbstractRelic {
    public GameState state;
    public String nameAP;

    public AbstractRelicAP(AbstractRelic oldRelic, GameState state) {
        super(oldRelic.relicId, "", oldRelic.tier, null);
        this.counter = oldRelic.counter;
        this.nameAP = oldRelic.name;
        this.state = state;
    }

    public void onMonsterDeath(AbstractMonsterAP m) {
    }

    public boolean canPlay(AbstractCardAP card) {
        return true;
    }

    public void onUseCard(AbstractCardAP targetCard, UseCardAction useCardAction) {
    }

    public void onExhaust(AbstractCardAP card) {
    }

    public void onCardDraw(AbstractCardAP drawnCard) {
    }


}
