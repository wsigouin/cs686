package STSSimulator;

import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.cards.red.AngerAP;
import STSSimulator.cards.status.BurnAP;
import STSSimulator.cards.status.DazedAP;
import STSSimulator.cards.status.SlimedAP;
import STSSimulator.powers.AbstractPowerAP;
import STSSimulator.powers.NoDrawPowerAP;
import STSSimulator.relics.AbstractRelicAP;
import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.relics.AbstractRelic;
import com.rits.cloning.Cloner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class SimulatorUtility {
    public static Cloner cloner = new Cloner();
    public static ArrayList<AbstractCardAP> cardDB = new ArrayList<>();

    public static void init() {
        SlimedAP card = new SlimedAP(null);
        cardDB.add(card);
        DazedAP card2 = new DazedAP(null);
        cardDB.add(card2);
        BurnAP card3 = new BurnAP(null);
        cardDB.add(card3);
        AngerAP card4 = new AngerAP(null);
        cardDB.add(card4);
    }


    public static void postPlayCard(AbstractCardAP card, AbstractPlayerAP player, ArrayList<AbstractMonsterAP> monsters) {
        boolean exhaustCard = false;
        if (card.exhaustOnUseOnce || card.exhaust) {
            exhaustCard = true;
        }

        applyCardPlayPowers(card, player.powers);
        Iterator var3;

        var3 = player.relics.iterator();

        while (var3.hasNext()) {
            AbstractRelicAP r = (AbstractRelicAP) var3.next();
            if (!card.dontTriggerOnUseCard) {
                //TODO: target should be second arg
                r.onUseCard(card, null);
            }
        }

        applyCardPlayCards(card, player.hand.group);
        applyCardPlayCards(card, player.discardPile.group);
        applyCardPlayCards(card, player.drawPile.group);

        var3 = monsters.iterator();

        while (var3.hasNext()) {
            AbstractMonsterAP m = (AbstractMonsterAP) var3.next();
            applyCardPlayPowers(card, m.powers);
            m.checkForPowerRemoval();
        }


        applyCardPlayPowers(card, player.powers);
        Iterator var1;

        var1 = monsters.iterator();

        while (var1.hasNext()) {
            AbstractMonsterAP m = (AbstractMonsterAP) var1.next();
            applyCardPlayPowers(card, m.powers);
            m.checkForPowerRemoval();
        }

        card.freeToPlayOnce = false;
        if (card.purgeOnUse) {
            player.cardInUse = null;
            return;
        }

        if (card.type == AbstractCard.CardType.POWER) {
            player.hand.empower(card);
            player.hand.applyPowers();
            player.cardInUse = null;
            return;
        }

        ///////////
        var1 = player.powers.iterator();

        while (var1.hasNext()) {
            AbstractPowerAP p = (AbstractPowerAP) var1.next();
            if (!card.dontTriggerOnUseCard) {
                p.onAfterUseCard(card);
            }
        }
        player.checkForPowerRemoval();

        var1 = monsters.iterator();

        while (var1.hasNext()) {
            AbstractMonsterAP m = (AbstractMonsterAP) var1.next();
            var3 = m.powers.iterator();

            while (var3.hasNext()) {
                AbstractPowerAP p = (AbstractPowerAP) var3.next();
                if (!card.dontTriggerOnUseCard) {
                    p.onAfterUseCard(card);
                }
            }
            m.checkForPowerRemoval();
        }

        card.freeToPlayOnce = false;
        if (card.purgeOnUse) {
            player.cardInUse = null;
            return;
        }

        if (card.type == AbstractCard.CardType.POWER) {
            player.hand.empower(card);
            player.hand.applyPowers();
            player.cardInUse = null;
            return;
        }

        player.cardInUse = null;
        if (!card.exhaust) {
            player.hand.moveToDiscardPile(card);
        } else {
            card.exhaustOnUseOnce = false;
            if (player.hasRelic("Strange Spoon") && card.type != AbstractCard.CardType.POWER) {
                if (AbstractDungeon.cardRandomRng.randomBoolean()) {
                    player.hand.moveToDiscardPile(card);
                } else {
                    player.hand.moveToExhaustPile(card);
                }
            } else {
                player.hand.moveToExhaustPile(card);
            }
        }

    }

    private static void applyCardPlayPowers(AbstractCardAP card, ArrayList<AbstractPowerAP> powers) {
        if (!card.dontTriggerOnUseCard) {
            int originalSize = powers.size();
            for(int i = 0; i < powers.size() || i < originalSize; i++ ) {
                AbstractPowerAP p = powers.get(i);
                p.onUseCard(card, null);
                if(powers.size() < originalSize || p != powers.get(i)){
                    originalSize = powers.size();
                    i--;
                }
            }
        }
    }





    private static void applyCardPlayCards(AbstractCardAP card, ArrayList<AbstractCardAP> cards) {

        if (!card.dontTriggerOnUseCard) {
            int originalSize = cards.size();
            for(int i = 0; i < cards.size() || i < originalSize; i++ ) {
                AbstractCardAP c = cards.get(i);
                c.triggerOnCardPlayed(card);
                if(cards.size() < originalSize || c != cards.get(i)){
                    originalSize = cards.size();
                    i--;
                }
            }
        }


    }

    public static void doDamage(DamageInfoAP info, AbstractMonsterAP target) {
        target.damage(info);
    }

    public static void doDamage(DamageInfoAP info, AbstractPlayerAP target) {
        target.damage(info);
    }

    public static void gainBlock(AbstractCreature target, int amount, GameState state) {
        if (target instanceof AbstractPlayerAP) {
            AbstractPlayerAP player = (AbstractPlayerAP) target;
            float tmp = (float) amount;
            Iterator var3;
            AbstractRelicAP r;
            for (var3 = player.relics.iterator(); var3.hasNext(); tmp = (float) r.onPlayerGainedBlock(tmp)) {
                r = (AbstractRelicAP) var3.next();
            }

            if (tmp > 0.0F) {
                var3 = player.powers.iterator();

                while (var3.hasNext()) {
                    AbstractPowerAP p = (AbstractPowerAP) var3.next();
                    p.onGainedBlock(tmp);
                }
            }
            for (AbstractMonsterAP abstractMonsterAP : state.monsters.monsters) {

                Iterator var6 = abstractMonsterAP.powers.iterator();
                while (var6.hasNext()) {
                    AbstractPowerAP p = (AbstractPowerAP) var6.next();
                    p.onPlayerGainedBlock(tmp);
                }
                abstractMonsterAP.checkForPowerRemoval();
            }

            player.currentBlock += MathUtils.floor(tmp);
            if (player.currentBlock > 999) {
                player.currentBlock = 999;
            }

            for (AbstractCardAP abstractCardAP : state.player.hand.group) {
                AbstractCardAP c = abstractCardAP;
                c.applyPowers();
            }
        } else {
            AbstractMonsterAP monster = (AbstractMonsterAP) target;
            monster.currentBlock += MathUtils.floor(amount);

            if (monster.currentBlock > 999) {
                monster.currentBlock = 999;
            }
        }

    }

    public static void applyPower(AbstractCreatureAP target, AbstractCreatureAP source, AbstractPowerAP powerToApply, int stacks, GameState state) {

        if (powerToApply instanceof NoDrawPowerAP && target.hasPower(powerToApply.ID)) {
            return;
        }

        if (source != null) {

            for(int i = 0; i < source.powers.size(); i++){
                AbstractPowerAP pow = (AbstractPowerAP) source.powers.get(i);
                pow.onApplyPower(powerToApply, target, source);

            }

        }

        if (state.player.hasRelic("Champion Belt") && source != null && source.isPlayer && target != source && powerToApply.ID.equals("Vulnerable") && !target.hasPower("Artifact")) {
            state.player.getRelic("Champion Belt").onTrigger(target);
        }

        if (target instanceof AbstractMonsterAP && target.isDeadOrEscaped()) {
            return;
        }

        if (state.player.hasRelic("Ginger") && target.isPlayer && powerToApply.ID.equals("Weakened")) {
            return;
        }

        if (state.player.hasRelic("Turnip") && target.isPlayer && powerToApply.ID.equals("Frail")) {
            return;
        }

        if (target.hasPower("Artifact") && powerToApply.type == AbstractPowerAP.PowerType.DEBUFF) {
            target.getPower("Artifact").onSpecificTrigger();
            return;
        }

        boolean hasBuffAlready = false;

        for(int i = 0; i < target.powers.size(); i++){
            AbstractPowerAP power = target.powers.get(i);
            if (powerToApply.ID.equals(power.ID)){
                power.stackPower(stacks);
                hasBuffAlready = true;
                onModifyPower(state);
                break;
            }
        }

        if(!hasBuffAlready){
            target.powers.add(powerToApply);
            Collections.sort(target.powers);
            powerToApply.onInitialApplication();

            onModifyPower(state);
        }
        target.checkForPowerRemoval();

    }

    public static int countStrikes(GameState state) {
        int count = 0;
        Iterator var1 = state.player.hand.group.iterator();

        AbstractCardAP c;
        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            if (isStrike(c)) {
                ++count;
            }
        }

        var1 = state.player.drawPile.group.iterator();

        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            if (isStrike(c)) {
                ++count;
            }
        }

        var1 = state.player.discardPile.group.iterator();

        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            if (isStrike(c)) {
                ++count;
            }
        }

        return count;
    }

    private static boolean isStrike(AbstractCardAP c) {
        return c.hasTag(AbstractCard.CardTags.STRIKE);
    }

    private static void onModifyPower(GameState state) {
//        if (state.monsters != null) {
//            for (AbstractMonsterAP abstractMonsterAP : state.monsters.monsters) {
//                AbstractMonsterAP m = abstractMonsterAP;
//                m.applyPowers();
//            }
//        }
    }

    public static void reducePower(AbstractCreatureAP target, AbstractCreature source, String power, int amount, GameState state) {
        reducePower(target, source, power, null, amount, state);
    }

    public static void reducePower(AbstractCreatureAP target, AbstractCreature source, String power, AbstractPowerAP powerInstance, int amount, GameState state) {
        AbstractPowerAP reduceMe = null;
        if (power != null) {
            reduceMe = target.getPower(power);
        } else if (powerInstance != null && target.powers.contains((AbstractPowerAP) powerInstance)) {
            reduceMe = powerInstance;
        }

        if (reduceMe != null) {
            if (amount < reduceMe.amount) {
                reduceMe.reducePower(amount);
                target.checkForPowerRemoval();
                onModifyPower(state);
            } else {
                reduceMe.removed = true;
            }
        }
    }

    public static boolean removePower(AbstractCreatureAP target, AbstractCreature source, String power, AbstractPowerAP powerInstance, GameState state) {
        if (target.isDeadOrEscaped()) {
            return false;
        }

        ArrayList<AbstractPowerAP> powers;

        if (target instanceof AbstractPlayerAP) {
            powers = ((AbstractPlayerAP) target).powers;
        } else {
            powers = ((AbstractMonsterAP) target).powers;
        }


        AbstractPowerAP removeMe = null;
        if (power != null) {
            removeMe = target.getPower(power);
        } else if (powerInstance != null && powers.contains((AbstractPowerAP) powerInstance)) {
            removeMe = powerInstance;
        }


        if (removeMe != null) {
            removeMe.onRemove();
            powers.remove((AbstractPowerAP) removeMe);
            onModifyPower(state);
            return true;
        }
        return false;
    }

    public static boolean isCombatOver(GameState state) {
        return state.monsters.areAllDeadOrEscaped() || state.player.isDeadOrEscaped();
    }


}