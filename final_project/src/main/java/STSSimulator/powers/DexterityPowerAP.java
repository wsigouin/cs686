package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class DexterityPowerAP extends AbstractPowerAP {

    public DexterityPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);

        if (this.amount >= 999) {
            this.amount = 999;
        }

        if (this.amount <= -999) {
            this.amount = -999;
        }

        this.canGoNegative = true;
    }

    public DexterityPowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Dexterity", state);

        if (this.amount >= 999) {
            this.amount = 999;
        }

        if (this.amount <= -999) {
            this.amount = -999;
        }

        this.canGoNegative = true;
    }


    public void stackPower(int stackAmount) {
        this.amount += stackAmount;
        if (this.amount == 0) {
            this.removed = true;
        }

        if (this.amount >= 999) {
            this.amount = 999;
        }

        if (this.amount <= -999) {
            this.amount = -999;
        }

    }

    public void reducePower(int reduceAmount) {
        this.fontScale = 8.0F;
        this.amount -= reduceAmount;
        if (this.amount == 0) {
            this.removed = true;
        }

        if (this.amount >= 999) {
            this.amount = 999;
        }

        if (this.amount <= -999) {
            this.amount = -999;
        }

    }

    public float modifyBlock(float blockAmount) {
        return (blockAmount += (float) this.amount) < 0.0F ? 0.0F : blockAmount;
    }


}
