package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class VulnerablePowerAP extends AbstractPowerAP {
    private boolean justApplied = false;

    public VulnerablePowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
        this.justApplied = false;

    }

    public VulnerablePowerAP(AbstractCreatureAP newOwner, int amount, boolean isSourceMonster, GameState state) {
        super(amount, newOwner, "Vulnerable", state);
        if (isSourceMonster) {
            this.justApplied = true;
        }
        this.isTurnBased = true;
        this.priority = 99;
        this.state = state;
    }

    public void atEndOfRound() {
        if (this.justApplied) {
            this.justApplied = false;
        } else {
            if (this.amount == 0) {
                this.removed = true;
            } else {
                SimulatorUtility.reducePower(this.owner, this.owner, "Vulnerable", null, 1, state);
            }

        }
    }

    public float atDamageReceive(float damage, DamageInfo.DamageType type) {
        if (type == DamageInfo.DamageType.NORMAL) {
            if (this.owner.isPlayer && state.player.hasRelic("Odd Mushroom")) {
                return damage * 1.25F;
            } else {
                return this.owner != null && !this.owner.isPlayer && state.player.hasRelic("Paper Frog") ? damage * 1.75F : damage * 1.5F;
            }
        } else {
            return damage;
        }
    }

}
