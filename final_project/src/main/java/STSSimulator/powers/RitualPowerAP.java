package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class RitualPowerAP extends AbstractPowerAP {
    private boolean skipFirst = true;

    public RitualPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
        this.skipFirst = false;
    }

    public RitualPowerAP(AbstractCreatureAP owner, int strAmt, GameState state) {
        super(strAmt, owner, "Ritual", state);
        this.ID = "Ritual";
    }


    public void atEndOfRound() {
        if (!this.skipFirst) {
            SimulatorUtility.applyPower(this.owner, this.owner, new StrengthPowerAP(this.owner, 3, state), 3, state);
        } else {
            this.skipFirst = false;
        }

    }
}
