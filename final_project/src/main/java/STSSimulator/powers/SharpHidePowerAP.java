package STSSimulator.powers;


import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class SharpHidePowerAP extends AbstractPowerAP {

    public SharpHidePowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Sharp Hide", state);
    }

    public SharpHidePowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public void onUseCard(AbstractCardAP card, UseCardAction action) {
        if (card.type == AbstractCard.CardType.ATTACK) {
            SimulatorUtility.doDamage(new DamageInfoAP(this.owner, this.amount), state.player);
        }

    }
}
