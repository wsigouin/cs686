package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class MetallicizePowerAP extends AbstractPowerAP {
    public MetallicizePowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public MetallicizePowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Metallicize", state);
    }

    public void atEndOfTurn(boolean isPlayer) {
        SimulatorUtility.gainBlock(this.owner, this.amount, state);
    }

}

