package STSSimulator.powers;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;

import java.util.Iterator;

public class FireBreathingPowerAP extends AbstractPowerAP {

    public FireBreathingPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public FireBreathingPowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Fire Breathing", state);
        this.ID = "Fire Breathing";
        this.owner = owner;
        this.amount = amount;
    }

    public void atEndOfTurn(boolean isPlayer) {
        int count = 0;
        for(AbstractCardAP c : state.player.cardsPlayedThisTurnList) {
            if (c.type == AbstractCard.CardType.ATTACK) {
                ++count;
            }
        }

       for(AbstractMonsterAP monster : state.monsters.getAliveMonsters()){
           SimulatorUtility.doDamage(new DamageInfoAP(state.player, count, DamageInfo.DamageType.THORNS), monster);
       }

    }


}
