package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.powers.AbstractPower;
import org.clapper.util.logging.Logger;

public abstract class AbstractPowerAP extends AbstractPower {
    public GameState state;
    public boolean removed = false;
    public AbstractCreatureAP owner;
    public String ID;
    public String name;
    private Logger logger;

    public AbstractPowerAP(int amount, AbstractCreatureAP newOwner, String ID, GameState state) {
        this.amount = amount;
        this.owner = newOwner;
        this.ID = ID;
        this.name = ID;
        this.state = state;
    }

    public AbstractPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        this.amount = oldPower.amount;
        this.owner = newOwner;
        this.ID = oldPower.ID;
        this.name = ID;
        this.state = state;
    }

    @Override
    public void stackPower(int stackAmount) {
        if (this.amount == -1) {
            logger.info(this + " does not stack");
        } else {
            this.fontScale = 8.0F;
            this.amount += stackAmount;
        }
    }


    public void onAfterUseCard(AbstractCardAP card) {
    }

    public void onUseCard(AbstractCardAP card, UseCardAction action) {
    }

    public void onExhaust(AbstractCardAP card) {
    }

    public void onAfterCardPlayed(AbstractCardAP usedCard) {
    }

}
