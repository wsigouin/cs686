package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class BerserkPowerAP extends AbstractPowerAP {
    public BerserkPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public BerserkPowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Berserk", state);
        this.name = "Berserk";
        this.ID = "Berserk";
    }

    public void atStartOfTurn() {
        state.player.currEnergy += 1;
    }

}
