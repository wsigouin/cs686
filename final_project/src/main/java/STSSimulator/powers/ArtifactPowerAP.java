package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.actions.common.ReducePowerAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.powers.ArtifactPower;

public class ArtifactPowerAP extends AbstractPowerAP {
    public ArtifactPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }


    public ArtifactPowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Artifact", state);

    }

    public void onSpecificTrigger() {
        System.out.println("ARTIFACT TRIGGERED");
        if (this.amount <= 0) {
            SimulatorUtility.removePower(this.owner, this.owner, "Artifact", this, state);
        } else {
            SimulatorUtility.reducePower(this.owner, this.owner, "Artifact", this, 1, state);
        }

    }
}
