package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class AngerPowerAP extends AbstractPowerAP {

    public AngerPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public AngerPowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Anger", state);
    }

    @Override
    public void onUseCard(AbstractCardAP card, UseCardAction action) {
        if (card.type == AbstractCard.CardType.SKILL) {
            SimulatorUtility.applyPower(this.owner, this.owner, new StrengthPowerAP(this.owner, this.amount, state), this.amount, state);
        }

    }
}
