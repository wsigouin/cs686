package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class EvolvePowerAP extends AbstractPowerAP {

    public EvolvePowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public EvolvePowerAP(AbstractCreatureAP owner, int drawAmt, GameState state) {
        super(drawAmt, owner, "Evolve", state);
        this.name = "Evolve";
        this.ID = "Evolve";
        this.owner = owner;
    }

}
