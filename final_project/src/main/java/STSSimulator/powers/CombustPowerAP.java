package STSSimulator.powers;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class CombustPowerAP extends AbstractPowerAP {
    public CombustPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public CombustPowerAP(AbstractCreatureAP newOwner, GameState state) {
        super(1, newOwner, "Combust", state);
    }

    public void atEndOfTurn(boolean isPlayer) {
        if (state.monsters.getAliveMonsters().size() != 0) {
            state.player.damage(new DamageInfoAP(null, this.amount / 5, DamageInfo.DamageType.HP_LOSS));
            for (AbstractMonsterAP monster : state.monsters.getAliveMonsters()) {
                SimulatorUtility.doDamage(new DamageInfoAP(this.owner, this.amount), monster);
            }
        }

    }


    public void stackPower(int stackAmount) {
        this.amount += stackAmount;
    }
}
