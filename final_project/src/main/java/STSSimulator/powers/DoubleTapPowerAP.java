package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardQueueItem;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;
public class DoubleTapPowerAP extends AbstractPowerAP {
    public DoubleTapPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public DoubleTapPowerAP(AbstractCreatureAP newOwner, int amount,  GameState state) {
        super(amount, newOwner, "Double Tap", state);
    }


    public void atEndOfTurn(boolean isPlayer) {
        if (isPlayer) {
            this.removed = true;
        }
    }


}
