package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class SplitPowerAP extends AbstractPowerAP {
    private boolean skipFirst = true;

    public SplitPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public SplitPowerAP(AbstractCreatureAP owner, GameState state) {
        super(1, owner, "Split", state);
        this.ID = "Split";
        this.owner = owner;
    }


    public float atDamageReceive(float damage, DamageInfo.DamageType damageType) {
        if (owner.currentHealth <= owner.maxHealth / 2) {
            ((AbstractMonsterAP) owner).setMove((byte) 3);
        }
        return damage;
    }


}
