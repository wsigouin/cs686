package STSSimulator.powers;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;

import java.util.Iterator;

public class FlameBarrierPowerAP extends AbstractPowerAP {
    DamageInfoAP thornsInfo;


    public FlameBarrierPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public FlameBarrierPowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Flame Barrier", state);
        this.thornsInfo = new DamageInfoAP(this.owner, this.amount, DamageInfo.DamageType.THORNS);
    }

    public int onAttacked(DamageInfo info, int damageAmount) {
        if (info.owner != null && info.type != DamageInfo.DamageType.THORNS && info.type != DamageInfo.DamageType.HP_LOSS && info.owner != this.owner) {
            SimulatorUtility.doDamage(this.thornsInfo, (AbstractMonsterAP)info.owner);
        }
        return damageAmount;
    }

    public void atStartOfTurn() {
        this.removed = true;
    }


}




