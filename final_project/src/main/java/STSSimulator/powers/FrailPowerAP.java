package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class FrailPowerAP extends AbstractPowerAP {
    public boolean justApplied;

    public FrailPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public FrailPowerAP(AbstractCreatureAP newOwner, int amount, boolean isSourceMonster, GameState state) {
        super(amount, newOwner, "Frail", state);
        if (isSourceMonster) {
            this.justApplied = true;
        }
        this.isTurnBased = true;
        this.priority = 10;
        this.state = state;
    }


    public void atEndOfRound() {
        if (this.justApplied) {
            this.justApplied = false;
        } else {
            if (this.amount == 0) {
                this.removed = true;
            } else {
                SimulatorUtility.reducePower(this.owner, this.owner, "Frail", null, 1, state);
            }

        }
    }

    public float modifyBlock(float blockAmount) {
        return blockAmount * 0.75F;
    }


}
