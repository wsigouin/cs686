package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class CorruptionPowerAP extends AbstractPowerAP {

    public CorruptionPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public CorruptionPowerAP(AbstractCreatureAP newOwner, GameState state) {
        super(-1, newOwner, "Corruption", state);
    }

    public void onUseCard(AbstractCardAP card, UseCardAction action) {
        if (card.type == AbstractCard.CardType.SKILL) {
            card.exhaust = true;
            state.player.hand.moveToExhaustPile(card);
        }

    }


}
