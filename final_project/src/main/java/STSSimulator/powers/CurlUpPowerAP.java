package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.powers.AbstractPower;


public class CurlUpPowerAP extends AbstractPowerAP {
    public static final String POWER_ID = "Curl Up";
    private boolean triggered = false;

    public CurlUpPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public int onAttacked(DamageInfo info, int damageAmount) {
        if (!this.triggered && damageAmount < this.owner.currentHealth && damageAmount > 0 && info.owner != null && info.type == DamageInfo.DamageType.NORMAL) {
            this.triggered = true;
            SimulatorUtility.gainBlock(this.owner, this.amount, state);
            this.removed = true;
        }

        return damageAmount;
    }
}
