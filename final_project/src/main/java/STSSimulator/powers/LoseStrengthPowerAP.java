package STSSimulator.powers;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;

import java.util.Iterator;

public class LoseStrengthPowerAP extends AbstractPowerAP {

    public LoseStrengthPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public LoseStrengthPowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Flex", state);
        this.type = PowerType.DEBUFF;
    }



    public void atEndOfTurn(boolean isPlayer) {
        SimulatorUtility.applyPower(this.owner, this.owner, new StrengthPowerAP(this.owner, -this.amount, this.state), -this.amount, state);
        this.removed = true;
    }


}