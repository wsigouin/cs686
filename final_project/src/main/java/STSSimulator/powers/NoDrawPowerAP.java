package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class NoDrawPowerAP extends AbstractPowerAP {

    public NoDrawPowerAP(AbstractCreatureAP newOwner, GameState state) {
        super(-1, newOwner, "No Draw", state);
        this.type = PowerType.DEBUFF;
    }

    public NoDrawPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public void atEndOfTurn(boolean isPlayer) {
        this.removed = true;
    }

}
