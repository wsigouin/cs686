package STSSimulator.powers;


import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class EntanglePowerAP extends AbstractPowerAP {
    public EntanglePowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public EntanglePowerAP(AbstractCreatureAP newOwner, GameState state) {
        super(1, newOwner, "Entangled", state);
    }

    public void atEndOfRound() {
        if (owner.isPlayer) {
            this.removed = true;
        }
    }

}
