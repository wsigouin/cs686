package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.powers.StrengthPower;

public class DemonFormPowerAP extends AbstractPowerAP {
    public DemonFormPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public DemonFormPowerAP(AbstractCreatureAP newOwner, int amount, GameState state) {
        super(amount, newOwner, "Demon Form", state);
    }

    public void atStartOfTurnPostDraw() {
        SimulatorUtility.applyPower(state.player, state.player, new StrengthPowerAP(this.owner, this.amount, state), this.amount, state);
    }


}
