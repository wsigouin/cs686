package STSSimulator.powers;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class WeakPowerAP extends AbstractPowerAP {
    private boolean justApplied = false;

    public WeakPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
        this.justApplied = false;
    }

    public WeakPowerAP(AbstractCreatureAP newOwner, int amount, boolean isSourceMonster, GameState state) {
        super(amount, newOwner, "Weakened", state);
        if (isSourceMonster) {
            this.justApplied = true;
        }
        this.isTurnBased = true;
        this.priority = 99;
    }

    public void atEndOfRound() {
        if (this.justApplied) {
            this.justApplied = false;
        } else {
            if (this.amount == 0) {
                this.removed = true;
            } else {
                SimulatorUtility.reducePower(this.owner, this.owner, "Weakened", null, 1, state);
            }

        }
    }

    public float atDamageGive(float damage, DamageInfoAP.DamageType type) {
        if (type == DamageInfoAP.DamageType.NORMAL) {
            return !this.owner.isPlayer && state.player.hasRelic("Paper Crane") ? damage * 0.6F : damage * 0.75F;
        } else {
            return damage;
        }
    }


}
