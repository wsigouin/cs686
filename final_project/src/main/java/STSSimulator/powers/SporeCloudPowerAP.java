package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class SporeCloudPowerAP extends AbstractPowerAP {
    public SporeCloudPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public SporeCloudPowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Spore Cloud", state);
    }

    public void onDeath() {
        SimulatorUtility.applyPower(state.player, null, new VulnerablePowerAP(state.player, this.amount, true, state), this.amount, state);
    }

}
