package STSSimulator.powers;


import STSSimulator.GameState;
import STSSimulator.actors.AbstractCreatureAP;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class BrutalityPowerAP extends AbstractPowerAP {
    public BrutalityPowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public BrutalityPowerAP(AbstractCreatureAP newOwner, int drawAmount, GameState state) {
        super(drawAmount, newOwner, "Brutality", state);
    }

    public void atStartOfTurnPostDraw() {
        state.player.draw();
        state.player.currentHealth -= 1;
    }


}
