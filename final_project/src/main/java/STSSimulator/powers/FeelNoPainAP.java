package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class FeelNoPainAP extends AbstractPowerAP {

    public FeelNoPainAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public FeelNoPainAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Feel No Pain", state);
        this.owner = owner;
        this.amount = amount;
    }

    public void onExhaust(AbstractCard card) {
        owner.addBlock(this.amount);
    }


}
