package STSSimulator.powers;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class DarkEmbracePowerAP extends AbstractPowerAP {
    public DarkEmbracePowerAP(AbstractPower oldPower, AbstractCreatureAP newOwner, GameState state) {
        super(oldPower, newOwner, state);
    }

    public DarkEmbracePowerAP(AbstractCreatureAP owner, int amount, GameState state) {
        super(amount, owner, "Dark Embrace", state);
    }


    public void onExhaust(AbstractCardAP card) {
        if (state.monsters.getAliveMonsters().size() != 0) {
            state.player.draw(this.amount);
        }

    }

}
