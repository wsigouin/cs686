package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;

public class HemokinesisAP extends AbstractCardAP {

    public HemokinesisAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public HemokinesisAP(GameState state) {
        super("Hemokinesis", 1, AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 14;
        this.baseMagicNumber = 3;
        this.magicNumber = this.baseMagicNumber;
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        state.player.damage(new DamageInfoAP(null, this.magicNumber, DamageInfo.DamageType.HP_LOSS));
        SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage), monster);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeDamage(4);
            this.upgradeMagicNumber(-1);
        }

    }

    @Override
    public AbstractCardAP makeCopy() {
        return new HemokinesisAP(state);
    }
}