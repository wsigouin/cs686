package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.EvolvePowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class EvolveAP extends AbstractCardAP {

    public EvolveAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public EvolveAP(GameState state) {
        super("Evolve", 1,  AbstractCard.CardType.POWER, AbstractCard.CardTarget.SELF, state);
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.applyPower(player, player, new EvolvePowerAP(player, this.magicNumber, state), this.magicNumber, state);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeMagicNumber(1);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new EvolveAP(state);
    }
}




