package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class DropkickAP extends AbstractCardAP {

    public DropkickAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public DropkickAP(GameState state) {
        super("Dropkick", 1,  AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 5;
    }



    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        if (monster.hasPower("Vulnerable")) {
            state.player.draw();
            state.player.currEnergy += 1;
        }
        SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage, this.damageTypeForTurn), monster);

    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeDamage(3);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new DropkickAP(state);
    }
}




