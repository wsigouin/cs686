package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;

//TODO: Actually implement
public class PerfectedStrikeAP extends AbstractCardAP {

    public PerfectedStrikeAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public PerfectedStrikeAP(GameState state) {
        super("Perfected Strike", 2,  AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 6;
        this.baseMagicNumber = 2;
        this.magicNumber = this.baseMagicNumber;
        this.tags.add(AbstractCard.CardTags.STRIKE);
        this.state = state;
    }


    public void use(AbstractPlayerAP p, AbstractMonsterAP m) {
        SimulatorUtility.doDamage(new DamageInfoAP(p, this.damage, this.damageTypeForTurn), m);
    }

    public AbstractCardAP makeCopy() {
        return new PerfectedStrikeAP(this.state);
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeMagicNumber(1);
        }
    }

}
