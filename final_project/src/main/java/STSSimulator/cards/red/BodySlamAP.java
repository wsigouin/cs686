package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class BodySlamAP extends AbstractCardAP {

    public BodySlamAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public BodySlamAP(GameState state) {
        super("Body Slam", 1,  AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 0;
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        this.baseDamage = player.currentBlock;
        this.calculateCardDamage(monster);
        SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage, this.damageTypeForTurn), monster);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBaseCost(0);
        }
    }

    @Override
    public AbstractCardAP makeCopy() {
        return new BodySlamAP(state);
    }
}