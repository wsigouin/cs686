package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class BloodForBloodAP extends AbstractCardAP {
    public BloodForBloodAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }


    public BloodForBloodAP(GameState state) {
        super("Blood for Blood", 4, AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 18;
    }

    public void tookDamage() {
        this.updateCost(-1);
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage, this.damageTypeForTurn), monster);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            if (this.cost < 4) {
                this.upgradeBaseCost(this.cost - 1);
                if (this.cost < 0) {
                    this.cost = 0;
                }
            } else {
                this.upgradeBaseCost(3);
            }

            this.upgradeDamage(4);
        }
    }

    @Override
    public AbstractCardAP makeCopy() {
        return new BloodForBloodAP(state);
    }
}
