package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.FireBreathingPowerAP;
import STSSimulator.powers.LoseStrengthPowerAP;
import STSSimulator.powers.StrengthPowerAP;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.ExhaustAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.vfx.TextAboveCreatureEffect;
import jdk.dynalink.linker.support.SimpleLinkRequest;

public class InflameAP extends AbstractCardAP {

    public InflameAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public InflameAP(GameState state) {
        super("Inflame", 1,  AbstractCard.CardType.POWER, AbstractCard.CardTarget.SELF, state);
        this.baseMagicNumber = 2;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.applyPower(player, player, new StrengthPowerAP(player, this.magicNumber, state), this.magicNumber, state);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeMagicNumber(1);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new InflameAP(state);
    }
}




