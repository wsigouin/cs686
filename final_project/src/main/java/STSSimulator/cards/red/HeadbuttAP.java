package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.FireBreathingPowerAP;
import STSSimulator.powers.LoseStrengthPowerAP;
import STSSimulator.powers.StrengthPowerAP;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.ExhaustAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.vfx.TextAboveCreatureEffect;
import jdk.dynalink.linker.support.SimpleLinkRequest;

import java.util.Iterator;

public class HeadbuttAP extends AbstractCardAP {

    public HeadbuttAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {

    }

    @Override
    public void upgrade() {

    }

    @Override
    public AbstractCardAP makeCopy() {
        return null;
    }
}




