package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.BerserkPowerAP;
import STSSimulator.powers.VulnerablePowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class BerserkAP extends AbstractCardAP {

    public BerserkAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public BerserkAP(GameState state) {
        super("Berserk", 0,  AbstractCard.CardType.POWER, AbstractCard.CardTarget.SELF, state);
        this.baseMagicNumber = 3;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.applyPower(state.player, state.player, new VulnerablePowerAP(state.player, this.magicNumber, false, state), this.magicNumber, state);
        SimulatorUtility.applyPower(state.player, state.player, new BerserkPowerAP(state.player, 1, state), 1, state);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeMagicNumber(-1);
        }
    }

    public AbstractCardAP makeCopy() {
        return null;
    }
}
