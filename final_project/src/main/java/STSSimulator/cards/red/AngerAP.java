package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class AngerAP extends AbstractCardAP {

    public AngerAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public AngerAP(GameState state) {
        super("Anger", 0, AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 8;
        this.baseMagicNumber = 2;
        this.magicNumber = this.baseMagicNumber;
        this.nameAP = "BASH";
    }

    public void use(AbstractPlayerAP p, AbstractMonsterAP m) {
        SimulatorUtility.doDamage(new DamageInfoAP(p, this.damage, this.damageTypeForTurn), m);
        AngerAP newCard = (AngerAP) this.makeCopy();
        newCard.state = state;
        state.player.discardPile.addCard(newCard);
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeDamage(2);
        }

    }

    public AbstractCardAP makeCopy() {
        AngerAP c = SimulatorUtility.cloner.deepClone((AngerAP) SimulatorUtility.cardDB.get(3));
        c.state = state;
        return c;
    }
}
