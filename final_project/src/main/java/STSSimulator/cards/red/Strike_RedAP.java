package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;

public class Strike_RedAP extends AbstractCardAP {



    public Strike_RedAP(AbstractCard card, GameState state) {
        super(card, state);
    }

    public Strike_RedAP(GameState state) {
        super("Strike_R", 1, AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 6;
        this.tags.add(AbstractCard.CardTags.STRIKE);
        this.nameAP = "Strike";
    }

    public void use(AbstractPlayerAP p, AbstractMonsterAP m) {
        SimulatorUtility.doDamage(new DamageInfoAP(p, this.damage, this.damageTypeForTurn), m);
    }


    public AbstractCardAP makeCopy() {
        return new Strike_RedAP(this.state);
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeDamage(3);
        }

    }
}