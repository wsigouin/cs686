package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;

public class IronWaveAP extends AbstractCardAP {



    public IronWaveAP(AbstractCard card, GameState state) {
        super(card, state);
    }

    public IronWaveAP(GameState state) {
        super("Iron Wave", 1,  AbstractCard.CardType.ATTACK,  AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 5;
        this.baseBlock = 5;
    }

    public void use(AbstractPlayerAP p, AbstractMonsterAP m) {
        SimulatorUtility.gainBlock(p, this.block, this.state);
        SimulatorUtility.doDamage(new DamageInfoAP(p, this.damage, this.damageTypeForTurn), m);
    }



    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeDamage(2);
            this.upgradeBlock(2);
        }

    }

    public AbstractCardAP makeCopy() {
        return new IronWaveAP(this.state);
    }

}