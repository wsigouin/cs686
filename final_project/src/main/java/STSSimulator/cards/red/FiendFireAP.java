package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.ExhaustAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.vfx.TextAboveCreatureEffect;

import java.util.ArrayList;

public class FiendFireAP extends AbstractCardAP {

    public FiendFireAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public FiendFireAP(GameState state) {
        super("Fiend Fire",2, AbstractCard.CardType.ATTACK,  AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 7;
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        ArrayList<AbstractCardAP> cards = new ArrayList<>(player.hand.group);

        for(AbstractCardAP card : cards){
            SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage), monster);
        }

        for(AbstractCardAP card : cards){
            player.hand.moveToExhaustPile(card);
        }

    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeDamage(3);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new FiendFireAP(state);
    }
}




