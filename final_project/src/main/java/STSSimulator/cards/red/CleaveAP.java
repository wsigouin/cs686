package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class CleaveAP extends AbstractCardAP {

    public CleaveAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public CleaveAP(GameState state) {
        super("Cleave", 1,  AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ALL_ENEMY, state);
        this.baseDamage = 8;
        this.isMultiDamage = true;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP m) {
        for (AbstractMonsterAP monster : state.monsters.getAliveMonsters()) {
            SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage, this.damageTypeForTurn), monster);
        }
    }


    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeDamage(3);
        }

    }

    @Override
    public AbstractCardAP makeCopy() {
        return new CleaveAP(state);
    }
}
