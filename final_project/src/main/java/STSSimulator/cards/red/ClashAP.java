package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

import java.util.Iterator;

public class ClashAP extends AbstractCardAP {

    public ClashAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public ClashAP(GameState state) {
        super("Clash", 0, AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 14;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage, this.damageTypeForTurn), monster);
    }


    public boolean canUse(AbstractPlayerAP p, AbstractMonsterAP m) {
        boolean canUse = super.canUse(p, m);
        if (!canUse) {
            return false;
        } else {
            Iterator var4 = p.hand.group.iterator();

            while (var4.hasNext()) {
                AbstractCardAP c = (AbstractCardAP) var4.next();
                if (c.type != AbstractCard.CardType.ATTACK) {
                    canUse = false;
                }
            }

            return canUse;
        }
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeDamage(4);
        }

    }

    @Override
    public AbstractCardAP makeCopy() {
        return new ClashAP(state);
    }
}
