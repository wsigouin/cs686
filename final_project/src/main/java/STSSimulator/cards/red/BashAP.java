package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.VulnerablePowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class BashAP extends AbstractCardAP {

    public BashAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }


    public BashAP(GameState state) {
        super("Bash", 2, AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 8;
        this.baseMagicNumber = 2;
        this.magicNumber = this.baseMagicNumber;
        this.nameAP = "BASH";
    }

    public void use(AbstractPlayerAP p, AbstractMonsterAP m) {
        SimulatorUtility.doDamage(new DamageInfoAP(p, this.damage, this.damageTypeForTurn), m);
        SimulatorUtility.applyPower(m, p, new VulnerablePowerAP(m, this.magicNumber, false, state), this.magicNumber, state);
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeDamage(2);
            this.upgradeMagicNumber(1);
        }

    }

    public AbstractCardAP makeCopy() {
        return new BashAP(this.state);
    }
}
