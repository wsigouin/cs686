package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.CombustPowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class CombustAP extends AbstractCardAP {

    public CombustAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public CombustAP(GameState state) {
        super("Combust", 1, AbstractCard.CardType.POWER, AbstractCard.CardTarget.SELF, state);
        this.baseMagicNumber = 5;
        this.magicNumber = this.baseMagicNumber;
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.applyPower(state.player, state.player, new CombustPowerAP(state.player, state), 1, state);

    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeMagicNumber(2);
        }
    }

    @Override
    public AbstractCardAP makeCopy() {
        return new CombustAP(state);
    }
}