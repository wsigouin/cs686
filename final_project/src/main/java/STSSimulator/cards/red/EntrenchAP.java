package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class EntrenchAP extends AbstractCardAP {

    public EntrenchAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public EntrenchAP(GameState state) {
        super("Entrench",  2, AbstractCard.CardType.SKILL, AbstractCard.CardTarget.SELF, state);
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        player.addBlock(player.currentBlock);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBaseCost(1);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new EntrenchAP(state);
    }
}




