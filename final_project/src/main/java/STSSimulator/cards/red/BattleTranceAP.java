package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.NoDrawPowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class BattleTranceAP extends AbstractCardAP {

    public BattleTranceAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }


    public BattleTranceAP(GameState state) {
        super("Battle Trance", 0,  AbstractCard.CardType.SKILL, AbstractCard.CardTarget.NONE, state);
        this.baseMagicNumber = 3;
        this.magicNumber = this.baseMagicNumber;
    }

    public AbstractCardAP makeCopy() {
        return new BattleTranceAP(state);
    }

    @Override
    public void use(AbstractPlayerAP p, AbstractMonsterAP m) {
        state.player.draw(this.magicNumber);
        SimulatorUtility.applyPower(state.player, state.player, new NoDrawPowerAP(p, state), -1, state);
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeMagicNumber(1);
        }

    }
}
