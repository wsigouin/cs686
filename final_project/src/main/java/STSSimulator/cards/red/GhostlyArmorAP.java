package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.FireBreathingPowerAP;
import STSSimulator.powers.LoseStrengthPowerAP;
import STSSimulator.powers.StrengthPowerAP;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.ExhaustAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.vfx.TextAboveCreatureEffect;
import jdk.dynalink.linker.support.SimpleLinkRequest;

public class GhostlyArmorAP extends AbstractCardAP {

    public GhostlyArmorAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public GhostlyArmorAP(GameState state) {
        super("Ghostly Armor",  1,AbstractCard.CardType.SKILL, AbstractCard.CardTarget.SELF, state);
        this.isEthereal = true;
        this.baseBlock = 10;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.gainBlock(player, this.block, this.state);

    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBlock(3);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new GhostlyArmorAP(state);
    }
}




