package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;

public class ImperviousAP extends AbstractCardAP {

    public ImperviousAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }


    public ImperviousAP(GameState state) {
        super("Impervious", 2, AbstractCard.CardType.SKILL, AbstractCard.CardTarget.SELF, state);
        this.baseBlock = 30;
        this.exhaust = true;
    }


    public AbstractCardAP makeCopy() {
        return new ImmolateAP(this.state);
    }

    @Override
    public void use(AbstractPlayerAP p, AbstractMonsterAP m) {
        SimulatorUtility.gainBlock(p, this.block, this.state);
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBlock(10);

        }

    }

}
