package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.FireBreathingPowerAP;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.ExhaustAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.vfx.TextAboveCreatureEffect;
import jdk.dynalink.linker.support.SimpleLinkRequest;

public class FireBreathingAP extends AbstractCardAP {

    public FireBreathingAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public FireBreathingAP(GameState state) {
        super("Fire Breathing",  1, AbstractCard.CardType.POWER, AbstractCard.CardTarget.SELF, state);
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.applyPower(player, player, new FireBreathingPowerAP(player, 1, state), 1, state);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBaseCost(0);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new FireBreathingAP(state);
    }
}




