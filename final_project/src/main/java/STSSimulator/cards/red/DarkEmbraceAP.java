package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.BerserkPowerAP;
import STSSimulator.powers.DarkEmbracePowerAP;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DarkEmbracePower;

public class DarkEmbraceAP extends AbstractCardAP {

    public DarkEmbraceAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }


    public DarkEmbraceAP(GameState state) {
        super("Dark Embrace", 2,  AbstractCard.CardType.POWER, AbstractCard.CardTarget.SELF, state);
    }

    public void use(AbstractPlayer p, AbstractMonster m) {
        SimulatorUtility.applyPower(state.player, state.player, new DarkEmbracePowerAP(state.player, 1, state), 1, state);
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {

    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBaseCost(1);
        }
    }

    @Override
    public AbstractCardAP makeCopy() {
        return new DarkEmbraceAP(state);
    }
}
