package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class ArmamentsAP extends AbstractCardAP {
    public ArmamentsAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
        this.cardTarget = true;
    }


    public ArmamentsAP(GameState state) {
        super("Armaments", 1, AbstractCard.CardType.SKILL,  AbstractCard.CardTarget.SELF, state);
        this.baseBlock = 5;
        this.nameAP = "Defend";
    }


    public AbstractCardAP makeCopy() {
        return new ArmamentsAP(state);
    }

    @Override
    public void use(AbstractPlayerAP p, AbstractMonsterAP m) {
        SimulatorUtility.gainBlock(p, this.block, this.state);
        //TODO
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBlock(3);
        }

    }
}
