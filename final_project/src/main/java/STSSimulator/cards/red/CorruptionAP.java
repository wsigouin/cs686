package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.CorruptionPowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class CorruptionAP extends AbstractCardAP {

    public CorruptionAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }


    public CorruptionAP(GameState state) {
        super("Corruption", 3, AbstractCard.CardType.POWER, AbstractCard.CardTarget.SELF, state);
        this.baseMagicNumber = 3;
        this.magicNumber = this.baseMagicNumber;
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        if (!state.player.hasPower("Corruption")) {
            SimulatorUtility.applyPower(state.player, state.player, new CorruptionPowerAP(state.player, state), -1, state);
        }

    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBaseCost(2);
        }

    }

    @Override
    public AbstractCardAP makeCopy() {
        return new CorruptionAP(state);
    }
}
