package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.StrengthPowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class DisarmAP extends AbstractCardAP {

    public DisarmAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public DisarmAP(GameState state) {
        super("Disarm", 1,  AbstractCard.CardType.SKILL, AbstractCard.CardTarget.ENEMY, state);
        this.baseMagicNumber = 2;
        this.magicNumber = this.baseMagicNumber;
        this.exhaust = true;
    }



    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.applyPower(monster, player, new StrengthPowerAP(monster, -this.magicNumber, state), -this.magicNumber, state);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeMagicNumber(1);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new DisarmAP(state);
    }
}
