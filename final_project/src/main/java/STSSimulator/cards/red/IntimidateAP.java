package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.WeakPowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class IntimidateAP extends AbstractCardAP {

    public IntimidateAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public IntimidateAP(GameState state) {
        super("Intimidate", 0,  AbstractCard.CardType.SKILL,  AbstractCard.CardTarget.ALL_ENEMY, state);
        this.exhaust = true;
        this.baseMagicNumber = 1;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP m) {
        for (AbstractMonsterAP monster : state.monsters.getAliveMonsters()) {
            SimulatorUtility.applyPower(monster, player, new WeakPowerAP(monster, this.magicNumber, false, state), this.magicNumber, state);
        }
    }


    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeMagicNumber(1);
        }

    }

    @Override
    public AbstractCardAP makeCopy() {
        return new IntimidateAP(state);
    }
}
