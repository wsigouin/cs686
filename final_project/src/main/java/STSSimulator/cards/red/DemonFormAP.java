package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.DemonFormPowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class DemonFormAP extends AbstractCardAP {

    public DemonFormAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }


    public DemonFormAP(GameState state) {
        super("Demon Form", 3, AbstractCard.CardType.POWER, AbstractCard.CardTarget.NONE, state);
        this.baseMagicNumber = 2;
        this.magicNumber = this.baseMagicNumber;
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.applyPower(state.player, state.player, new DemonFormPowerAP(state.player, this.magicNumber, state), this.magicNumber, state);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeMagicNumber(1);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new DemonFormAP(state);
    }
}