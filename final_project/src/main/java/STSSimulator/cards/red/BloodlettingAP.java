package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;

public class BloodlettingAP extends AbstractCardAP {

    public BloodlettingAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public BloodlettingAP(GameState state) {
        super("Bloodletting", 0,  AbstractCard.CardType.SKILL, AbstractCard.CardTarget.SELF, state);
        this.baseMagicNumber = 1;
        this.magicNumber = this.baseMagicNumber;
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        state.player.damage(new DamageInfoAP(null, 3, DamageInfo.DamageType.HP_LOSS));
        state.player.currEnergy += this.magicNumber;
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeMagicNumber(1);
        }

    }

    @Override
    public AbstractCardAP makeCopy() {
        return new BloodlettingAP(state);
    }
}