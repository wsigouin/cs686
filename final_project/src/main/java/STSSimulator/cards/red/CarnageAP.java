package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class CarnageAP extends AbstractCardAP {

    public CarnageAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public CarnageAP(GameState state) {
        super("Carnage", 2,  AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 20;
        this.isEthereal = true;
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.doDamage(new DamageInfoAP(state.player, this.damage, this.damageTypeForTurn), monster);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeDamage(8);
        }
    }

    @Override
    public AbstractCardAP makeCopy() {
        return new CarnageAP(state);
    }
}

