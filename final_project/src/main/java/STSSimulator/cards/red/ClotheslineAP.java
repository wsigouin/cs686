package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.WeakPowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class ClotheslineAP extends AbstractCardAP {

    public ClotheslineAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public ClotheslineAP(GameState state) {
        super("Clothesline", 2, AbstractCard.CardType.ATTACK,  AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 12;
        this.baseMagicNumber = 2;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage, this.damageTypeForTurn), monster);
        SimulatorUtility.applyPower(monster, player, new WeakPowerAP(monster, this.magicNumber, false, state), this.magicNumber, state);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeDamage(2);
            this.upgradeMagicNumber(1);
        }
    }

    @Override
    public AbstractCardAP makeCopy() {
        return new ClotheslineAP(state);
    }
}



