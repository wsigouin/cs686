package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.BrutalityPowerAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class BrutalityAP extends AbstractCardAP {

    public BrutalityAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public BrutalityAP(GameState state) {
        super("Brutality", 0, AbstractCard.CardType.POWER, AbstractCard.CardTarget.SELF, state);
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.applyPower(state.player, state.player, new BrutalityPowerAP(state.player, 1, state), 1, state);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBaseCost(0);
        }
    }

    @Override
    public AbstractCardAP makeCopy() {
        return new BrutalityAP(state);
    }
}