package STSSimulator.cards.red;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.CardStrings;

public class Defend_RedAP extends AbstractCardAP {

    public Defend_RedAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }


    public Defend_RedAP(GameState state) {
        super("Defend_R", 1,  AbstractCard.CardType.SKILL, AbstractCard.CardTarget.SELF, state);
        this.baseBlock = 5;
        this.nameAP = "Defend";
    }


    public AbstractCardAP makeCopy() {
        return new Defend_RedAP(this.state);
    }

    @Override
    public void use(AbstractPlayerAP p, AbstractMonsterAP m) {
        SimulatorUtility.gainBlock(p, this.block, this.state);
    }

    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeBlock(3);
        }

    }

}
