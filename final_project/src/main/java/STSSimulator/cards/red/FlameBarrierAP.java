package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.FireBreathingPowerAP;
import STSSimulator.powers.FlameBarrierPowerAP;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.ExhaustAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.red.FlameBarrier;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.vfx.TextAboveCreatureEffect;
import jdk.dynalink.linker.support.SimpleLinkRequest;

public class FlameBarrierAP extends AbstractCardAP {

    public FlameBarrierAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public FlameBarrierAP(GameState state) {
        super("Flame Barrier",  2,  AbstractCard.CardType.SKILL, AbstractCard.CardTarget.SELF, state);
        this.baseBlock = 12;
        this.baseMagicNumber = 4;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        player.addBlock(this.block);
        SimulatorUtility.applyPower(player, player, new FlameBarrierPowerAP(player, this.magicNumber, state),  this.magicNumber, state);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBlock(4);
            this.upgradeMagicNumber(2);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new FlameBarrierAP(state);
    }
}




