package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.vfx.TextAboveCreatureEffect;

public class FeedAP extends AbstractCardAP {
    private int maxHpAmt = 3;

    public FeedAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public FeedAP(GameState state) {
        super("Feed", 1, AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 10;
        this.exhaust = true;
        this.baseMagicNumber = this.maxHpAmt;
        this.magicNumber = this.baseMagicNumber;
        this.tags.add(AbstractCard.CardTags.HEALING);
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage), monster);
        if((monster.isDeadOrEscaped() || monster.currentHealth <= 0) && !monster.hasPower("Minion")){
            player.maxHealth += this.magicNumber;
            player.currentHealth += this.magicNumber;
        }

    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBaseCost(1);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new FeedAP(state);
    }
}




