package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.cards.status.BurnAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;

public class ImmolateAP extends AbstractCardAP {

    public ImmolateAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public ImmolateAP(GameState state) {
        super("Immolate", 2, AbstractCard.CardType.ATTACK,  AbstractCard.CardTarget.ALL_ENEMY, state);
        this.baseDamage = 21;
        this.isMultiDamage = true;
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        for (AbstractMonsterAP m : state.monsters.getAliveMonsters()) {
            SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage, this.damageTypeForTurn), m);
        }

        player.discardPile.addCard(new BurnAP(state));
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeDamage(7);
        }

    }

    @Override
    public AbstractCardAP makeCopy() {
        return new ImmolateAP(state);
    }
}