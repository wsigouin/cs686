package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.vfx.TextAboveCreatureEffect;

public class FeelNoPainAP extends AbstractCardAP {
    private int maxHpAmt = 3;

    public FeelNoPainAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public FeelNoPainAP(GameState state) {
        super("Feel No Pain",  1, AbstractCard.CardType.POWER,  AbstractCard.CardTarget.SELF, state);
        this.baseMagicNumber = 3;
        this.magicNumber = this.baseMagicNumber;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.applyPower(player, player, new STSSimulator.powers.FeelNoPainAP(player, this.magicNumber, state), this.magicNumber, state);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeBaseCost(1);
        }
    }


    @Override
    public AbstractCardAP makeCopy() {
        return new FeelNoPainAP(state);
    }
}





