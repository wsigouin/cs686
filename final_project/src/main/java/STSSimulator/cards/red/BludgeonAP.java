package STSSimulator.cards.red;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class BludgeonAP extends AbstractCardAP {

    public BludgeonAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public BludgeonAP(GameState state) {
        super("Bludgeon", 3,  AbstractCard.CardType.ATTACK, AbstractCard.CardTarget.ENEMY, state);
        this.baseDamage = 32;
    }


    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        SimulatorUtility.doDamage(new DamageInfoAP(player, this.damage, this.damageTypeForTurn), monster);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.upgradeName();
            this.upgradeDamage(10);
        }

    }

    @Override
    public AbstractCardAP makeCopy() {
        return new BludgeonAP(state);
    }
}