package STSSimulator.cards;


import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.red.ClashAP;
import STSSimulator.cards.red.HeavyBladeAP;
import STSSimulator.cards.red.PerfectedStrikeAP;
import STSSimulator.relics.AbstractRelicAP;
import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.powers.StrengthPower;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

public abstract class AbstractCardAP {
    public AbstractCard.CardType type;
    public int cost;
    public int costForTurn;
    public int price;
    public int chargeCost;
    public boolean isCostModified;
    public boolean isCostModifiedForTurn;
    public boolean retain;
    public boolean dontTriggerOnUseCard;
    public AbstractCard.CardRarity rarity;
    public AbstractCard.CardColor color;
    public boolean isInnate;
    public boolean isLocked;
    public boolean showEvokeValue;
    public int showEvokeOrbCount;
    public ArrayList<String> keywords;
    protected boolean isUsed;
    public boolean upgraded;
    public int timesUpgraded;
    public int misc;
    public int energyOnUse;
    public boolean isSeen;
    private boolean upgradedCost;
    public boolean upgradedDamage;
    public boolean upgradedBlock;
    public boolean upgradedMagicNumber;
    public UUID uuid;
    public boolean isSelected;
    public boolean exhaust;
    public boolean isEthereal;
    public ArrayList<AbstractCard.CardTags> tags;
    public int[] multiDamage;
    protected boolean isMultiDamage;
    public int baseDamage;
    public int baseBlock;
    public int baseMagicNumber;
    public int baseHeal;
    public int baseDraw;
    public int baseDiscard;
    public int damage;
    public int block;
    public int magicNumber;
    public int heal;
    public int draw;
    public int discard;
    public boolean isDamageModified;
    public boolean isBlockModified;
    public boolean isMagicNumberModified;
    protected DamageInfo.DamageType damageType;
    public DamageInfo.DamageType damageTypeForTurn;
    public AbstractCard.CardTarget target;
    public boolean purgeOnUse;
    public boolean exhaustOnUseOnce;
    public boolean exhaustOnFire;
    public boolean freeToPlayOnce;


    public String originalName;
    public String name;
    public String rawDescription;
    public String cardID;

    public boolean inBottleFlame;
    public boolean inBottleLightning;
    public boolean inBottleTornado;


    public GameState state;
    public String nameAP;
    public boolean cardTarget = false;

    public AbstractCardAP(AbstractCard oldCard, GameState state) {
        this.chargeCost = oldCard.chargeCost;
        this.isCostModified = oldCard.isCostModified;
        this.isCostModifiedForTurn = oldCard.isCostModifiedForTurn;
        this.retain = oldCard.retain;
        this.dontTriggerOnUseCard = oldCard.dontTriggerOnUseCard;
        this.isInnate = oldCard.isInnate;
        this.isLocked = oldCard.isLocked;
        this.upgraded = oldCard.upgraded;
        this.timesUpgraded = oldCard.timesUpgraded;
        this.misc = oldCard.misc;
        this.upgradedCost = oldCard.upgradedCost;
        this.upgradedDamage = oldCard.upgradedDamage;
        this.upgradedBlock = oldCard.upgradedBlock;
        this.upgradedMagicNumber = oldCard.upgradedMagicNumber;
        this.isSelected = oldCard.isSelected;
        this.exhaust = oldCard.exhaust;
        this.isEthereal = oldCard.isEthereal;
        this.tags = oldCard.tags;
        this.isMultiDamage = oldCard.upgradedDamage;
        this.baseDamage = oldCard.baseDamage;
        this.baseBlock = oldCard.baseBlock;
        this.baseMagicNumber = oldCard.baseMagicNumber;
        this.baseHeal = oldCard.baseHeal;
        this.baseDraw = oldCard.baseDraw;
        this.baseDiscard = oldCard.baseDiscard;
        this.damage = oldCard.damage;
        this.block = oldCard.block;
        this.magicNumber = oldCard.magicNumber;
        this.heal = oldCard.heal;
        this.draw = oldCard.draw;
        this.discard = oldCard.discard;
        this.isDamageModified = oldCard.isDamageModified;
        this.isBlockModified = oldCard.isBlockModified;
        this.isMagicNumberModified = oldCard.isMagicNumberModified;
        this.target = oldCard.target;
        this.purgeOnUse = oldCard.purgeOnUse;
        this.exhaustOnUseOnce = oldCard.exhaustOnUseOnce;
        this.exhaustOnFire = oldCard.exhaustOnFire;
        this.freeToPlayOnce = oldCard.freeToPlayOnce;
        this.originalName = oldCard.originalName;
        this.name = oldCard.name;
        this.cardID = oldCard.cardID;

        this.cost = oldCard.cost;
        this.costForTurn = oldCard.costForTurn;
        this.type = oldCard.type;
        this.target = oldCard.target;
        this.damageType = oldCard.damageTypeForTurn;
        this.damageTypeForTurn = oldCard.damageTypeForTurn;
        this.state = state;
    }

    public AbstractCardAP(String id, int cost, AbstractCard.CardType type, AbstractCard.CardTarget target, GameState state) {
        this.chargeCost = -1;
        this.isCostModified = false;
        this.isCostModifiedForTurn = false;
        this.retain = false;
        this.dontTriggerOnUseCard = false;
        this.isInnate = false;
        this.isLocked = false;
        this.showEvokeValue = false;
        this.showEvokeOrbCount = 0;
        this.keywords = new ArrayList();
        this.isUsed = false;
        this.upgraded = false;
        this.timesUpgraded = 0;
        this.misc = 0;
        this.isSeen = true;
        this.upgradedCost = false;
        this.upgradedDamage = false;
        this.upgradedBlock = false;
        this.upgradedMagicNumber = false;
        this.isSelected = false;
        this.exhaust = false;
        this.isEthereal = false;
        this.tags = new ArrayList();
        this.isMultiDamage = false;
        this.baseDamage = -1;
        this.baseBlock = -1;
        this.baseMagicNumber = -1;
        this.baseHeal = -1;
        this.baseDraw = -1;
        this.baseDiscard = -1;
        this.damage = -1;
        this.block = -1;
        this.magicNumber = -1;
        this.heal = -1;
        this.draw = -1;
        this.discard = -1;
        this.isDamageModified = false;
        this.isBlockModified = false;
        this.isMagicNumberModified = false;
        this.target = target;
        this.purgeOnUse = false;
        this.exhaustOnUseOnce = false;
        this.exhaustOnFire = false;
        this.freeToPlayOnce = false;
        this.inBottleLightning = false;
        this.inBottleTornado = false;
        this.originalName = id;
        this.name = id;
        this.cardID = id;

        this.cost = cost;
        this.costForTurn = cost;
        this.rawDescription = rawDescription;
        this.type = type;
        this.color = color;
        this.rarity = rarity;
        this.target = target;
        this.damageType = null;
        this.damageTypeForTurn = null;
        this.state = state;
    }

    public void calculateCardDamage(AbstractMonsterAP target) {
        this.applyPowersToBlock();
        AbstractPlayerAP player = state.player;
        this.isDamageModified = false;
        if (!this.isMultiDamage && target != null) {
            float tmp = (float) this.baseDamage;
            if (this instanceof PerfectedStrikeAP) {
                if (this.upgraded) {
                    tmp += (float) (3 * SimulatorUtility.countStrikes(state));
                } else {
                    tmp += (float) (2 * SimulatorUtility.countStrikes(state));
                }

                if (this.baseDamage != (int) tmp) {
                    this.isDamageModified = true;
                }
            }

            if (player.hasRelic("WristBlade") && (this.costForTurn == 0 || this.freeToPlayOnce)) {
                tmp += 3.0F;
                if (this.baseDamage != (int) tmp) {
                    this.isDamageModified = true;
                }
            }

            tmp = calculateDamageGive(player, tmp);
            Iterator var9;
            AbstractPower p;

            if (target != null) {
                for (var9 = target.powers.iterator(); var9.hasNext(); tmp = p.atDamageReceive(tmp, this.damageTypeForTurn)) {
                    p = (AbstractPower) var9.next();
                }
            }

            var9 = player.powers.iterator();

            while (var9.hasNext()) {
                p = (AbstractPower) var9.next();
                tmp = p.atDamageFinalGive(tmp, this.damageTypeForTurn);
                if (this.baseDamage != (int) tmp) {
                    this.isDamageModified = true;
                }
            }

            if (target != null) {
                var9 = target.powers.iterator();

                while (var9.hasNext()) {
                    p = (AbstractPower) var9.next();
                    tmp = p.atDamageFinalReceive(tmp, this.damageTypeForTurn);
                    if (this.baseDamage != (int) tmp) {
                        this.isDamageModified = true;
                    }
                }
            }

            if (tmp < 0.0F) {
                tmp = 0.0F;
            }

            this.damage = MathUtils.floor(tmp);
        } else {
            ArrayList<AbstractMonsterAP> m = state.monsters.monsters;
            float[] tmp = new float[m.size()];

            int i;
            for (i = 0; i < tmp.length; ++i) {
                tmp[i] = (float) this.baseDamage;
            }

            Iterator var6;
            AbstractPower p;
            for (i = 0; i < tmp.length; ++i) {
                if (player.hasRelic("WristBlade") && (this.costForTurn == 0 || this.freeToPlayOnce)) {
                    tmp[i] += 3.0F;
                    if (this.baseDamage != (int) tmp[i]) {
                        this.isDamageModified = true;
                    }
                }

                var6 = player.powers.iterator();

                while (var6.hasNext()) {
                    p = (AbstractPower) var6.next();
                    tmp[i] = p.atDamageGive(tmp[i], this.damageTypeForTurn);
                    if (this.baseDamage != (int) tmp[i]) {
                        this.isDamageModified = true;
                    }
                }
            }

            //TODO: monster dying logic
            for (i = 0; i < tmp.length; ++i) {
                var6 = (m.get(i)).powers.iterator();

                while (var6.hasNext()) {
                    p = (AbstractPower) var6.next();
                    if (!(m.get(i)).isDying && !(m.get(i)).isEscaping) {
                        tmp[i] = p.atDamageReceive(tmp[i], this.damageTypeForTurn);
                    }
                }
            }

            for (i = 0; i < tmp.length; ++i) {
                var6 = player.powers.iterator();

                while (var6.hasNext()) {
                    p = (AbstractPower) var6.next();
                    tmp[i] = p.atDamageFinalGive(tmp[i], this.damageTypeForTurn);
                    if (this.baseDamage != (int) tmp[i]) {
                        this.isDamageModified = true;
                    }
                }
            }

            for (i = 0; i < tmp.length; ++i) {
                var6 = (m.get(i)).powers.iterator();

                while (var6.hasNext()) {
                    p = (AbstractPower) var6.next();
                    if (!(m.get(i)).isDying && !(m.get(i)).isEscaping) {
                        tmp[i] = p.atDamageFinalReceive(tmp[i], this.damageTypeForTurn);
                    }
                }
            }

            getMultiDamage(tmp);
        }

    }

    private void applyPowersToBlock() {
        this.isBlockModified = false;
        float tmp = (float) this.baseBlock;
        Iterator var2 = state.player.powers.iterator();

        while (var2.hasNext()) {
            AbstractPower p = (AbstractPower) var2.next();
            tmp = p.modifyBlock(tmp);
            if (this.baseBlock != MathUtils.floor(tmp)) {
                this.isBlockModified = true;
            }
        }

        if (tmp < 0.0F) {
            tmp = 0.0F;
        }

        this.block = MathUtils.floor(tmp);
    }

    public void applyPowers() {
        this.applyPowersToBlock();
        AbstractPlayerAP player = state.player;
        this.isDamageModified = false;
        if (!this.isMultiDamage) {
            float tmp = (float) this.baseDamage;
            if (this instanceof PerfectedStrikeAP) {
                if (this.upgraded) {
                    tmp += (float) (3 * SimulatorUtility.countStrikes(state));
                } else {
                    tmp += (float) (2 * SimulatorUtility.countStrikes(state));
                }

                if (this.baseDamage != (int) tmp) {
                    this.isDamageModified = true;
                }
            }

            if (state.player.hasRelic("WristBlade") && (this.costForTurn == 0 || this.freeToPlayOnce)) {
                tmp += 3.0F;
                if (this.baseDamage != (int) tmp) {
                    this.isDamageModified = true;
                }
            }

            tmp = calculateDamageGive(player, tmp);
            Iterator var3;
            AbstractPower p;

            var3 = player.powers.iterator();

            while (var3.hasNext()) {
                p = (AbstractPower) var3.next();
                tmp = p.atDamageFinalGive(tmp, this.damageTypeForTurn);
                if (this.baseDamage != (int) tmp) {
                    this.isDamageModified = true;
                }
            }
            player.checkForPowerRemoval();

            if (tmp < 0.0F) {
                tmp = 0.0F;
            }

            this.damage = MathUtils.floor(tmp);
        } else {
            ArrayList<AbstractMonsterAP> m = state.monsters.monsters;
            float[] tmp = new float[m.size()];

            int i;
            for (i = 0; i < tmp.length; ++i) {
                tmp[i] = (float) this.baseDamage;
            }

            Iterator var5;
            AbstractPower p;
            for (i = 0; i < tmp.length; ++i) {
                if (state.player.hasRelic("WristBlade") && (this.costForTurn == 0 || this.freeToPlayOnce)) {
                    tmp[i] += 3.0F;
                    if (this.baseDamage != (int) tmp[i]) {
                        this.isDamageModified = true;
                    }
                }

                var5 = player.powers.iterator();

                while (var5.hasNext()) {
                    p = (AbstractPower) var5.next();
                    tmp[i] = p.atDamageGive(tmp[i], this.damageTypeForTurn);
                    if (this.baseDamage != (int) tmp[i]) {
                        this.isDamageModified = true;
                    }
                }
                player.checkForPowerRemoval();
            }

            for (i = 0; i < tmp.length; ++i) {
                var5 = player.powers.iterator();

                while (var5.hasNext()) {
                    p = (AbstractPower) var5.next();
                    tmp[i] = p.atDamageFinalGive(tmp[i], this.damageTypeForTurn);
                    if (this.baseDamage != (int) tmp[i]) {
                        this.isDamageModified = true;
                    }
                }
                player.checkForPowerRemoval();

            }

            getMultiDamage(tmp);
        }

    }

    private void getMultiDamage(float[] tmp) {
        int i;
        for (i = 0; i < tmp.length; ++i) {
            if (tmp[i] < 0.0F) {
                tmp[i] = 0.0F;
            }
        }

        this.multiDamage = new int[tmp.length];

        for (i = 0; i < tmp.length; ++i) {
            this.multiDamage[i] = MathUtils.floor(tmp[i]);
        }

        this.damage = this.multiDamage[0];
    }

    private float calculateDamageGive(AbstractPlayerAP player, float tmp) {
        Iterator var9 = player.powers.iterator();
        AbstractPower p;
        while (var9.hasNext()) {
            p = (AbstractPower) var9.next();
            //TODO: Powers like StrengthPower need to be overwritten
            if (this instanceof HeavyBladeAP && p instanceof StrengthPower) {
                if (this.upgraded) {
                    tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
                    tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
                }

                tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
                tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
            }

            tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
            if (this.baseDamage != (int) tmp) {
                this.isDamageModified = true;
            }
        }
        return tmp;
    }

    public abstract void use(AbstractPlayerAP player, AbstractMonsterAP monster);

    public void use(AbstractPlayer var1, AbstractMonster var2) {
        System.out.println("USING INCORRECT FUNCTION!!");
    }


    public boolean hasEnoughEnergy() {
        if (state.player.hasPower("Entangled") && this.type == AbstractCard.CardType.ATTACK) {
            return false;
        } else if (this.freeToPlayOnce) {
            return true;
        } else {
            if (state.player.currEnergy >= this.costForTurn) {
                return true;
            }

            Iterator var1 = state.player.relics.iterator();
            AbstractRelicAP r;

            do {
                if (!var1.hasNext()) {
                    break;
                }
                r = (AbstractRelicAP) var1.next();
            } while (r.canPlay(this));

            return false;
        }
    }

    public boolean canUse(AbstractPlayerAP p, AbstractMonsterAP m) {
        if(!canUse()){
            return false;
        }

        boolean ret =  this.cardPlayable(m) && this.hasEnoughEnergy();
        return ret;
    }

    public boolean canUse() {
        if (this.type == AbstractCard.CardType.STATUS) {
            if(state.player.hasRelic("Medical Kit") && !this.cardID.equals("Slimed")){
                return true;
            }
            return false;
        }

        if (this.type == AbstractCard.CardType.CURSE) {
            if (state.player.hasRelic("Blue Candle")) {
                return true;
            }

            if (!this.cardID.equals("Pride")) {
                return false;

            }
        }


        if(this instanceof ClashAP){
            boolean onlyAttacks = true;
            for(AbstractCardAP card : state.player.hand.group){
                if(card.type != AbstractCard.CardType.ATTACK){
                    onlyAttacks = false;
                    break;
                }
            }
            if(!onlyAttacks){
                return false;
            }
        }

        return hasEnoughEnergy();
    }

    public boolean cardPlayable(AbstractMonsterAP m) {
        return (this.target != AbstractCard.CardTarget.ENEMY && this.target != AbstractCard.CardTarget.SELF_AND_ENEMY || m == null || !m.isDeadOrEscaped()) && state.monsters.getAliveMonsters().size() != 0;
    }


    protected void useMedicalKit(AbstractPlayerAP p) {
        this.exhaust = true;
    }

    public void tookDamage() {
    }

    public void didDiscard() {
    }

    public void updateCost(int amt) {
        if (this.color == AbstractCard.CardColor.CURSE && !this.cardID.equals("Pride") || this.type == AbstractCard.CardType.STATUS && !this.cardID.equals("Slimed")) {
            System.out.println("Curses/Statuses cannot have their costs modified");
        } else {
            int tmpCost = this.cost;
            int diff = this.cost - this.costForTurn;
            tmpCost += amt;
            if (tmpCost < 0) {
                tmpCost = 0;
            }

            if (tmpCost != this.cost) {
                this.isCostModified = true;
                this.cost = tmpCost;
                this.costForTurn = this.cost - diff;
                if (this.costForTurn < 0) {
                    this.costForTurn = 0;
                }
            }
        }

    }

    public void setCostForTurn(int amt) {
        if (this.costForTurn >= 0) {
            this.costForTurn = amt;
            if (this.costForTurn < 0) {
                this.costForTurn = 0;
            }

            if (this.costForTurn != this.cost) {
                this.isCostModifiedForTurn = true;
            }
        }

    }

    public void modifyCostForTurn(int amt) {
        if (this.costForTurn > 0) {
            this.costForTurn += amt;
            if (this.costForTurn < 0) {
                this.costForTurn = 0;
            }

            if (this.costForTurn != this.cost) {
                this.isCostModifiedForTurn = true;
            }
        }

    }

    public void modifyCostForCombat(int amt) {
        if (this.costForTurn > 0) {
            this.costForTurn += amt;
            if (this.costForTurn < 0) {
                this.costForTurn = 0;
            }

            if (this.cost != this.costForTurn) {
                this.isCostModified = true;
            }

            this.cost = this.costForTurn;
        } else if (this.cost >= 0) {
            this.cost += amt;
            if (this.cost < 0) {
                this.cost = 0;
            }

            this.costForTurn = 0;
            if (this.cost != this.costForTurn) {
                this.isCostModified = true;
            }
        }

    }

    public void resetAttributes() {
        this.block = this.baseBlock;
        this.isBlockModified = false;
        this.damage = this.baseDamage;
        this.isDamageModified = false;
        this.magicNumber = this.baseMagicNumber;
        this.isMagicNumberModified = false;
        this.damageTypeForTurn = this.damageType;
        this.costForTurn = this.cost;
        this.isCostModifiedForTurn = false;
    }

    public abstract void upgrade();

    protected void upgradeDamage(int amount) {
        this.baseDamage += amount;
        this.upgradedDamage = true;
    }

    protected void upgradeBlock(int amount) {
        this.baseBlock += amount;
        this.upgradedBlock = true;
    }

    protected void upgradeMagicNumber(int amount) {
        this.baseMagicNumber += amount;
        this.magicNumber = this.baseMagicNumber;
        this.upgradedMagicNumber = true;
    }

    protected void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = this.name + "+";
    }

    protected void upgradeBaseCost(int newBaseCost) {
        int diff = this.costForTurn - this.cost;
        this.cost = newBaseCost;
        if (this.costForTurn > 0) {
            this.costForTurn = this.cost + diff;
        }

        if (this.costForTurn < 0) {
            this.costForTurn = 0;
        }

        this.upgradedCost = true;
    }

    public boolean canUpgrade() {
        if (this.type == AbstractCard.CardType.CURSE) {
            return false;
        } else if (this.type == AbstractCard.CardType.STATUS) {
            return false;
        } else {
            return !this.upgraded;
        }
    }

    public boolean hasTag(AbstractCard.CardTags tagToCheck) {
        return this.tags.contains(tagToCheck);
    }

    public void onMoveToDiscard() {
    }

    public void triggerWhenDrawn() {
    }

    public void triggerWhenCopied() {
    }

    public void triggerOnEndOfPlayerTurn() {
        if (this.isEthereal) {
            this.exhaust = true;
            state.player.hand.removeCard(this);
            state.player.exhaustPile.addCard(this);
        }
    }

    public void triggerOnEndOfTurnForPlayingCard() {
    }

    public void triggerOnOtherCardPlayed(AbstractCardAP c) {
    }

    public void triggerOnGainEnergy(int e, boolean dueToCard) {
    }

    public void triggerOnManualDiscard() {
    }

    public void triggerOnCardPlayed(AbstractCardAP cardPlayed) {
    }

    public void onPlayCard(AbstractCardAP c, AbstractMonsterAP m) {
    }

    public void atTurnStart() {
    }

    public void triggerOnExhaust() {
    }

    public void clearPowers() {
        this.resetAttributes();
        this.isDamageModified = false;
    }

    public int applyPowerOnBlockHelper(int base) {
        float tmp = (float) base;

        AbstractPower p;
        for (Iterator var2 = state.player.powers.iterator(); var2.hasNext(); tmp = p.modifyBlock(tmp)) {
            p = (AbstractPower) var2.next();
        }

        return MathUtils.floor(tmp);
    }

    public abstract AbstractCardAP makeCopy();

}
