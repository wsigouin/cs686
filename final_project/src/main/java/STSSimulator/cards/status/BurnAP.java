package STSSimulator.cards.status;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class BurnAP extends AbstractCardAP {

    private int actualBurnDamage = 2;


    public BurnAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public BurnAP(GameState state) {
        super("Burn", -2, AbstractCard.CardType.STATUS, AbstractCard.CardTarget.NONE, state);
        if (this.upgraded) {
            actualBurnDamage = 6;
        }
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        if (!this.dontTriggerOnUseCard && player.hasRelic("Medical Kit")) {
            this.useMedicalKit(player);
        } else {
            SimulatorUtility.doDamage(new DamageInfoAP(player, this.actualBurnDamage), state.player);
        }
    }

    public void triggerWhenDrawn() {
        if (state.player.hasPower("Evolve") && !state.player.hasPower("No Draw")) {
            state.player.draw();
        }

    }

    public void triggerOnEndOfTurnForPlayingCard() {
        this.dontTriggerOnUseCard = true;
        this.use(state.player, null);
    }

    @Override
    public void upgrade() {
        if (!this.upgraded) {
            this.actualBurnDamage = 6;
        }
    }

    @Override
    public AbstractCardAP makeCopy() {
        return new BurnAP(state);
    }

}
