package STSSimulator.cards.status;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class SlimedAP extends AbstractCardAP {
    public SlimedAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);

    }

    public SlimedAP(GameState state) {
        super("Slimed", 1, AbstractCard.CardType.STATUS,  AbstractCard.CardTarget.SELF, state);
        this.exhaust = true;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        System.out.println("USING A STATUS YOU FUCKING TWAT");
    }

    public void triggerWhenDrawn() {
        if (state.player.hasPower("Evolve") && !state.player.hasPower("No Draw")) {
            state.player.draw();
        }

    }

    @Override
    public void upgrade() {

    }

    @Override
    public AbstractCardAP makeCopy() {
       return new SlimedAP(state);
    }
}
