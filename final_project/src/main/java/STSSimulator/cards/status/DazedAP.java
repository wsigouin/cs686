package STSSimulator.cards.status;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import com.megacrit.cardcrawl.cards.AbstractCard;

public class DazedAP extends AbstractCardAP {
    public DazedAP(AbstractCard oldCard, GameState state) {
        super(oldCard, state);
    }

    public DazedAP(GameState state) {
        super("Dazed", -2, AbstractCard.CardType.STATUS, AbstractCard.CardTarget.NONE, state);
        this.isEthereal = true;
    }

    @Override
    public void use(AbstractPlayerAP player, AbstractMonsterAP monster) {
        System.out.println("USING A STATUS YOU FUCKING TWAT");
        if (state.player.hasRelic("Medical Kit")) {
            this.useMedicalKit(state.player);
        } else {
            state.player.hand.moveToExhaustPile(this);
        }
    }

    public void triggerWhenDrawn() {
        if (state.player.hasPower("Evolve") && !state.player.hasPower("No Draw")) {
            state.player.draw();
        }

    }

    @Override
    public void upgrade() {

    }

    @Override
    public AbstractCardAP makeCopy() {
       return new BurnAP(state);
    }
}
