package STSSimulator.actors;

import MonteCarlo.MCTree;
import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.fields.CardGroupAP;
import STSSimulator.powers.AbstractPowerAP;
import STSSimulator.relics.AbstractRelicAP;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.potions.AbstractPotion;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.relics.AbstractRelic;
import com.megacrit.cardcrawl.ui.panels.EnergyPanel;

import java.util.ArrayList;
import java.util.Iterator;

public class AbstractPlayerAP extends AbstractCreatureAP implements Comparable {
    public int currEnergy;
    public CardGroupAP masterDeck;
    public CardGroupAP drawPile;
    public CardGroupAP hand;
    public CardGroupAP discardPile;
    public CardGroupAP exhaustPile;
    public int gameHandSize;
    public int masterHandSize;
    public int startingMaxHP;
    public CardGroup limbo;
    public int potionSlots;
    public ArrayList<AbstractPotion> potions;
    public int damagedThisCombat;
    public int cardsPlayedThisTurn;
    public AbstractCardAP cardInUse;
    public int maxEnergy;
    public int gold;
    public ArrayList<AbstractCardAP> cardsPlayedThisTurnList = new ArrayList<>();

    public ArrayList<AbstractRelicAP> relics = new ArrayList<>();


    public AbstractPlayerAP(AbstractPlayer copy, GameState state) {
        super(copy, state);

        this.currEnergy = EnergyPanel.getCurrentEnergy();
        this.gameHandSize = copy.gameHandSize;
        this.masterHandSize = copy.masterHandSize;
        this.startingMaxHP = copy.startingMaxHP;
        this.masterDeck = new CardGroupAP(copy.masterDeck, this.state);
        this.drawPile = new CardGroupAP(copy.drawPile, this.state);
        this.hand = new CardGroupAP(copy.hand, this.state);
        this.discardPile = new CardGroupAP(copy.discardPile, this.state);
        this.exhaustPile = new CardGroupAP(copy.exhaustPile, this.state);
        this.limbo = copy.limbo;
        //TODO
        //this.relics = new ArrayList<AbstractRelicAP>();
        this.potionSlots = copy.potionSlots;
        this.potions = copy.potions;
        this.damagedThisCombat = copy.damagedThisCombat;
        this.cardsPlayedThisTurn = copy.cardsPlayedThisTurn;
        this.isPlayer = true;
        this.maxEnergy = copy.energy.energyMaster;
        this.gold = copy.gold;
    }


    public void useCard(AbstractCardAP c, AbstractMonsterAP target) {
        c.calculateCardDamage(target);
        c.use(this, target);

        SimulatorUtility.postPlayCard(c, this, state.monsters.monsters);

        if (!c.dontTriggerOnUseCard) {
            this.hand.triggerOnOtherCardPlayed(c);
        }
        this.hand.removeCard(c);
        cardsPlayedThisTurnList.add(c);

        //System.out.println("Played card: " + c.nameAP + " on " + target.name + "[" + state.monsters.monsters.indexOf(target) + "]");
        this.cardInUse = c;
        if (c.costForTurn > 0 && !c.freeToPlayOnce && (!this.hasPower("Corruption") || c.type != AbstractCard.CardType.SKILL)) {
            useEnergy(c.costForTurn);
        }
    }

    private void useEnergy(int e) {
        currEnergy -= e;
        if (currEnergy < 0) {
            currEnergy = 0;
        }
    }

    public void damage(DamageInfoAP info) {
        int damageAmount = info.output;

        boolean hadBlock = true;
        if (this.currentBlock == 0) {
            hadBlock = false;
        }

        if (damageAmount < 0) {
            damageAmount = 0;
        }

        if (damageAmount > 1 && this.hasPower("IntangiblePlayer")) {
            damageAmount = 1;
        }

        damageAmount = this.decrementBlock(info, damageAmount);
        Iterator var4;
        AbstractRelicAP r;
        if (info.owner == this) {
            var4 = this.relics.iterator();

            while (var4.hasNext()) {
                r = (AbstractRelicAP) var4.next();
                r.onAttack(info, damageAmount, this);
            }
        }

        AbstractPowerAP p;
        if (info.owner != null) {
            var4 = info.owner.powers.iterator();

            while (var4.hasNext()) {
                p = (AbstractPowerAP) var4.next();
                p.onAttack(info, damageAmount, this);
            }

            info.owner.checkForPowerRemoval();

            for (var4 = this.powers.iterator(); var4.hasNext(); damageAmount = p.onAttacked(info, damageAmount)) {
                p = (AbstractPowerAP) var4.next();
            }

            checkForPowerRemoval();

            for (var4 = this.relics.iterator(); var4.hasNext(); damageAmount = r.onAttacked(info, damageAmount)) {
                r = (AbstractRelicAP) var4.next();
            }
        } else {
            //System.out.println("NO OWNER, DON'T TRIGGER POWERS");
        }

        if (damageAmount > 0) {
            for (var4 = this.powers.iterator(); var4.hasNext(); damageAmount = p.onLoseHp(damageAmount)) {
                p = (AbstractPowerAP) var4.next();
            }

            checkForPowerRemoval();

            var4 = this.relics.iterator();

            while (var4.hasNext()) {
                r = (AbstractRelicAP) var4.next();
                r.onLoseHp(damageAmount);
            }

            if (info.owner != null) {
                var4 = info.owner.powers.iterator();

                while (var4.hasNext()) {
                    p = (AbstractPowerAP) var4.next();
                    p.onInflictDamage(info, damageAmount, this);
                }
                info.owner.checkForPowerRemoval();
            }

            if (info.type == DamageInfo.DamageType.HP_LOSS) {
                state.hpLossThisCombat += damageAmount;
            }

            state.damageReceivedThisTurn += damageAmount;
            state.damageReceivedThisCombat += damageAmount;

            this.currentHealth -= damageAmount;

            if (damageAmount > 0) {
                this.updateCardsOnDamage();
                ++this.damagedThisCombat;
            }

            if (this.currentHealth < 0) {
                this.currentHealth = 0;
            }

            if ((float) this.currentHealth <= (float) this.maxHealth / 2.0F && !this.isBloodied) {
                this.isBloodied = true;
                var4 = this.relics.iterator();

                while (var4.hasNext()) {
                    r = (AbstractRelicAP) var4.next();
                    if (r != null) {
                        r.onBloodied();
                    }
                }
            }

            if (this.currentHealth < 1) {
//                if (!this.hasRelic("Mark of the Bloom")) {
//                    if (this.hasPotion("FairyPotion")) {
//                        var4 = this.potions.iterator();
//
//                        while(var4.hasNext()) {
//                            AbstractPotion pot = (AbstractPotion)var4.next();
//                            if (pot.ID.equals("FairyPotion")) {
//                                pot.flash();
//                                this.currentHealth = 0;
//                                pot.use(this);
//                                AbstractDungeon.topPanel.destroyPotion(pot.slot);
//                                return;
//                            }
//                        }
//                    } else
//                    if (this.hasRelic("Lizard Tail") && ((LizardTail)this.getRelic("Lizard Tail")).counter == -1) {
//                        this.currentHealth = 0;
//                        this.getRelic("Lizard Tail").onTrigger();
//                        return;
//                    }
//                }

                this.isDead = true;
                this.currentHealth = 0;
                if (this.currentBlock > 0) {
                    this.loseBlock();
                }
            }
        }
    }

    @Override
    public void loseBlock(int amount, boolean noAnimation) {
        this.currentBlock -= amount;
        if (this.currentBlock < 0) {
            this.currentBlock = 0;
        }

    }

    @Override
    protected int decrementBlock(DamageInfo info, int damageAmount) {
        if (info.type != DamageInfo.DamageType.HP_LOSS && this.currentBlock > 0) {
            if (damageAmount > this.currentBlock) {
                damageAmount -= this.currentBlock;

                this.loseBlock();
                this.brokeBlock();
            } else if (damageAmount == this.currentBlock) {
                damageAmount = 0;
                this.loseBlock();
                this.brokeBlock();
            } else {
                this.loseBlock(damageAmount);
                damageAmount = 0;
            }
        }

        return damageAmount;
    }

    @Override
    public void damage(DamageInfo damageInfo) {
        System.out.println("USING INCORRECT FUNCTION");
    }

    private void brokeBlock() {
        for (AbstractRelicAP r : state.player.relics) {
            r.onBlockBroken(this);
        }
    }

    private void updateCardsOnDamage() {
        Iterator var1 = this.hand.group.iterator();

        AbstractCardAP c;
        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            c.tookDamage();
        }

        var1 = this.discardPile.group.iterator();

        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            c.tookDamage();
        }

        var1 = this.drawPile.group.iterator();

        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            c.tookDamage();
        }
    }

    @Override
    public boolean hasPower(String targetID) {
        for (AbstractPowerAP p : this.powers) {
            if (p.ID.equals(targetID)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void render(SpriteBatch spriteBatch) {

    }

    @Override
    public AbstractPowerAP getPower(String targetID) {
        Iterator var2 = this.powers.iterator();

        AbstractPowerAP p;
        do {
            if (!var2.hasNext()) {
                return null;
            }

            p = (AbstractPowerAP) var2.next();
        } while (!p.ID.equals(targetID));

        return p;
    }

    public AbstractRelicAP getRelic(String targetID) {
        Iterator var2 = this.relics.iterator();

        AbstractRelicAP r;
        do {
            if (!var2.hasNext()) {
                return null;
            }

            r = (AbstractRelicAP) var2.next();
        } while (!r.relicId.equals(targetID));

        return r;
    }

    public boolean hasRelic(String targetID) {
        Iterator var2 = this.relics.iterator();

        AbstractRelicAP r;
        do {
            if (!var2.hasNext()) {
                return false;
            }

            r = (AbstractRelicAP) var2.next();
        } while (!r.relicId.equals(targetID));

        return true;
    }

    public void onCardDrawOrDiscard() {
        Iterator var1 = this.powers.iterator();

        while (var1.hasNext()) {
            AbstractPower p = (AbstractPower) var1.next();
            p.onDrawOrDiscard();
        }

        var1 = this.relics.iterator();

        while (var1.hasNext()) {
            AbstractRelicAP r = (AbstractRelicAP) var1.next();
            r.onDrawOrDiscard();
        }

        if (this.hasPower("Corruption")) {
            var1 = this.hand.group.iterator();

            while (var1.hasNext()) {
                AbstractCardAP c = (AbstractCardAP) var1.next();
                if (c.type == AbstractCard.CardType.SKILL && c.costForTurn != 0) {
                    c.modifyCostForCombat(-9);
                }
            }
        }

        this.hand.applyPowers();
    }


    public void startTurn() {
        this.loseBlock();
        applyStartOfTurnRelics();
        applyStartOfTurnPowers();
        this.draw(this.masterHandSize);
        this.currEnergy = this.maxEnergy;
        applyStartOfTurnPostDrawRelics();
        applyStartOfTurnPostDrawPowers();
        applyStartOfTurnCards();
    }

    private void applyStartOfTurnRelics() {
        Iterator var1 = this.relics.iterator();

        while (var1.hasNext()) {
            AbstractRelicAP r = (AbstractRelicAP) var1.next();
            if (r != null) {
                r.atTurnStart();
            }
        }
    }

    private void applyStartOfTurnPostDrawRelics() {
        Iterator var1 = this.relics.iterator();

        while (var1.hasNext()) {
            AbstractRelicAP r = (AbstractRelicAP) var1.next();
            if (r != null) {
                r.atTurnStartPostDraw();
            }
        }

    }

    private void applyStartOfTurnCards() {
        Iterator var1 = this.drawPile.group.iterator();

        AbstractCardAP c;
        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            if (c != null) {
                c.atTurnStart();
            }
        }

        var1 = this.hand.group.iterator();

        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            if (c != null) {
                c.atTurnStart();
            }
        }

        var1 = this.discardPile.group.iterator();

        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            if (c != null) {
                c.atTurnStart();
            }
        }

    }

    public void endTurn() {
        applyEndOfTurnTriggers();

        while (this.hand.group.size() > 0) {
            this.discardPile.group.add(this.hand.group.get(0));
            this.hand.group.remove(this.hand.group.get(0));
        }
    }

    public boolean takeRandomTurn(boolean firstTurn) {
        if (!firstTurn) this.startTurn();
        boolean shouldContinue;
        while (true) {
            ArrayList<AbstractCardAP> playableCards = this.hand.getPlayableCards();
            ArrayList<AbstractMonsterAP> aliveMonsters = state.monsters.getAliveMonsters();

            if (playableCards.size() == 0) {
                shouldContinue = true;
                break;
            }
            if (aliveMonsters.size() == 0) {
                shouldContinue = false;
                break;
            }

            AbstractCardAP card = playableCards.get(AbstractDungeon.aiRng.random(playableCards.size() - 1));
            AbstractMonsterAP target = aliveMonsters.get(AbstractDungeon.aiRng.random(aliveMonsters.size() - 1));
            if(card.canUse(state.player, target)) {
                if (card.type == AbstractCard.CardType.ATTACK) {
                    if (MCTree.verbose) {
                        System.out.println("Target " + target.name + "[" + state.monsters.monsters.indexOf(target) + "]" + " currently has " + target.currentHealth + " and " + target.currentBlock);
                        System.out.println("Using " + card.name + " on " + target.name + "[" + state.monsters.monsters.indexOf(target) + "]");
                    }
                    useCard(card, target);
                    if (MCTree.verbose) {
                        System.out.println("Target " + target.name + "[" + state.monsters.monsters.indexOf(target) + "]" + " currently has " + target.currentHealth + " and " + target.currentBlock);
                        System.out.println("--");
                    }
                } else {
                    if (MCTree.verbose) {
                        System.out.println("Player now has HP " + state.player.currentHealth + " and " + state.player.currentBlock);
                        System.out.println("Using " + card.name);
                    }
                    useCard(card, target);
                    if (MCTree.verbose) {
                        System.out.println("Player now has HP " + state.player.currentHealth + " and " + state.player.currentBlock);
                        System.out.println("--");
                    }

                }
            }
            else{
                continue;
            }

            if(MCTree.verbose) {
                System.out.println(state);
            }



            if (SimulatorUtility.isCombatOver(state)) {
                shouldContinue = false;
                break;
            }
        }

        if(MCTree.verbose) {
            System.out.println("Ending Turn");
            System.out.println("--");
        }
        this.endTurn();

        return shouldContinue;
    }

    public void draw(int numCards) {
        for (int i = 0; i < numCards; ++i) {
            if (this.drawPile.isEmpty()) {
                this.drawPile.group.addAll(this.discardPile.group);
                this.drawPile.shuffle();
                this.discardPile.group.clear();
            }
            assert(drawPile.size() != 0);
            AbstractCardAP c = this.drawPile.getTopCard();
            if (this.hasPower("Confusion") && c.cost >= 0) {
                int newCost = AbstractDungeon.cardRandomRng.random(3);
                if (c.cost != newCost) {
                    c.cost = newCost;
                    c.costForTurn = c.cost;
                    c.isCostModified = true;
                }
            }

            this.hand.group.add(c);
            this.drawPile.removeCard(c);
            c.triggerWhenDrawn();

            if (this.hasPower("Corruption") && c.type == AbstractCard.CardType.SKILL) {
                c.setCostForTurn(-9);
            }

            Iterator var6 = this.relics.iterator();

            while (var6.hasNext()) {
                AbstractRelicAP r = (AbstractRelicAP) var6.next();
                r.onCardDraw(c);
            }
        }

    }

    public void draw() {
        if (!(this.hand.size() >= 10)) {
            this.draw(1);
            this.onCardDrawOrDiscard();
        }
    }

    @Override
    public int compareTo(Object o) {
        AbstractPlayerAP otherPlayer = (AbstractPlayerAP) o;

        if (this.currentHealth != otherPlayer.currentHealth || this.currEnergy != otherPlayer.currEnergy || this.currentBlock != otherPlayer.currentBlock) {
            return -1;
        }

        if (this.hand.compareTo(otherPlayer.hand) != 0 || this.drawPile.compareTo(otherPlayer.drawPile) != 0 || this.discardPile.compareTo(otherPlayer.discardPile) != 0) {
            return -1;
        }

        for (AbstractPowerAP power : this.powers) {
            if (!otherPlayer.hasPower(power.ID)) {
                return -1;
            }
        }

        return 0;
    }
}
