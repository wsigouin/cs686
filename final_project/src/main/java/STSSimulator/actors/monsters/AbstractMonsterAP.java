package STSSimulator.actors.monsters;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.powers.AbstractPowerAP;
import STSSimulator.relics.AbstractRelicAP;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.EnemyMoveInfo;
import com.megacrit.cardcrawl.relics.AbstractRelic;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class AbstractMonsterAP extends AbstractCreatureAP implements Comparable {
    public int intentDmg;
    public EnemyMoveInfo move;
    public ArrayList<DamageInfoAP> damage = new ArrayList<>();
    public byte nextMove;
    public boolean escaped = false;
    public boolean cannotEscape = false;
    public ArrayList<Byte> moveHistory;
    public static String[] MOVES = AbstractMonster.MOVES;
    public AbstractMonster.EnemyType type;
    public boolean escapeNext;
    public boolean isMultiDmg;

    public AbstractMonsterAP(String id, int maxHealth, GameState state) {
        super(id, maxHealth, state);
        this.moveHistory = new ArrayList<>();
    }

    public AbstractMonsterAP(AbstractMonster copy, GameState state) {
        super(copy, state);
        this.escaped = copy.escaped;
        this.cannotEscape = copy.cannotEscape;
        this.nextMove = copy.nextMove;
        this.state = state;
        this.moveHistory = new ArrayList<>();
        this.moveHistory.addAll(copy.moveHistory);
        this.name = copy.name;
        this.type = copy.type;

        copyDamage(copy.damage);
    }

    public void damage(DamageInfoAP info) {
        if (info.output > 0 && this.hasPower("IntangiblePlayer")) {
            info.output = 1;
        }

        int damageAmount = info.output;
        if (!this.isDying && !this.isEscaping) {
            if (damageAmount < 0) {
                damageAmount = 0;
            }

            boolean hadBlock = true;
            if (this.currentBlock == 0) {
                hadBlock = false;
            }

            boolean weakenedToZero = damageAmount == 0;
            damageAmount = this.decrementBlock(info, damageAmount);
            Iterator var5;
            AbstractRelicAP r;
            if (info.owner instanceof AbstractPlayerAP) {
                var5 = state.player.relics.iterator();

                while (var5.hasNext()) {
                    r = (AbstractRelicAP) var5.next();
                    r.onAttack(info, damageAmount, this);
                }
                state.player.checkForPowerRemoval();
            }


            AbstractPowerAP p;
            if (info.owner != null) {
                for (AbstractPowerAP power : info.owner.powers) {
                    damageAmount = power.onAttacked(info, damageAmount);
                }
                info.owner.checkForPowerRemoval();

            }


            for (AbstractPowerAP power : this.powers) {
                damageAmount = power.onAttacked(info, damageAmount);
            }
            checkForPowerRemoval();

            for (var5 = state.player.relics.iterator(); var5.hasNext(); damageAmount = r.onAttackedMonster(info, damageAmount)) {
                r = (AbstractRelicAP) var5.next();
            }

            if (damageAmount > 0) {
                if (damageAmount >= 99 && !CardCrawlGame.overkill) {
                    CardCrawlGame.overkill = true;
                }

                this.currentHealth -= damageAmount;
                if (this.currentHealth < 0) {
                    this.currentHealth = 0;
                }
            }

            if (this.currentHealth <= 0) {
                this.die();

                if (this.currentBlock > 0) {
                    this.loseBlock();
                }
            }

        }
    }


    @Override
    public void loseBlock() {
        this.loseBlock(this.currentBlock, true);
    }

    @Override
    public void loseBlock(int amount, boolean noAnimation) {
        this.currentBlock -= amount;
        if (this.currentBlock < 0) {
            this.currentBlock = 0;
        }

    }

    @Override
    protected int decrementBlock(DamageInfo info, int damageAmount) {
        if (info.type != DamageInfo.DamageType.HP_LOSS && this.currentBlock > 0) {
            if (damageAmount > this.currentBlock) {
                damageAmount -= this.currentBlock;

                this.loseBlock();
                this.brokeBlock();
            } else if (damageAmount == this.currentBlock) {
                damageAmount = 0;
                this.loseBlock();
                this.brokeBlock();
            } else {
                this.loseBlock(damageAmount);
                damageAmount = 0;
            }
        }

        return damageAmount;
    }

    public void applyPowers() {
        Iterator var2 = this.damage.iterator();

        while (var2.hasNext()) {
            DamageInfo dmg = (DamageInfo) var2.next();
            dmg.applyPowers(this, state.player);
        }
    }

    private void calculateDamage(int dmg) {
        AbstractPlayerAP target = state.player;
        float tmp = (float) dmg;

        AbstractPowerAP p;
        Iterator var6;
        for (var6 = this.powers.iterator(); var6.hasNext(); tmp = p.atDamageGive(tmp, DamageInfo.DamageType.NORMAL)) {
            p = (AbstractPowerAP) var6.next();
        }

        for (var6 = target.powers.iterator(); var6.hasNext(); tmp = p.atDamageReceive(tmp, DamageInfo.DamageType.NORMAL)) {
            p = (AbstractPowerAP) var6.next();
        }

        for (var6 = this.powers.iterator(); var6.hasNext(); tmp = p.atDamageFinalGive(tmp, DamageInfo.DamageType.NORMAL)) {
            p = (AbstractPowerAP) var6.next();
        }

        for (var6 = target.powers.iterator(); var6.hasNext(); tmp = p.atDamageFinalReceive(tmp, DamageInfo.DamageType.NORMAL)) {
            p = (AbstractPowerAP) var6.next();
        }

        dmg = MathUtils.floor(tmp);
        if (dmg < 0) {
            dmg = 0;
        }

        this.intentDmg = dmg;

    }

    private void brokeBlock() {
        for (AbstractRelicAP r : state.player.relics) {
            r.onBlockBroken(this);
        }
    }

    public void die() {
        this.die(true);
    }

    public void die(boolean triggerRelics) {
        if (!this.isDying) {
            this.isDying = true;
            this.isDead = true;
            Iterator var2;
            if (this.currentHealth <= 0 && triggerRelics) {
                var2 = this.powers.iterator();

                while (var2.hasNext()) {
                    AbstractPowerAP p = (AbstractPowerAP) var2.next();
                    p.onDeath();
                }
            }
            checkForPowerRemoval();


            if (triggerRelics) {
                var2 = state.player.relics.iterator();

                while (var2.hasNext()) {
                    AbstractRelicAP r = (AbstractRelicAP) var2.next();
                    r.onMonsterDeath(this);
                }
            }

            if (this.currentHealth < 0) {
                this.currentHealth = 0;
            }
        }

    }

    public void setMove(String moveName, byte nextMove, int baseDamage, int multiplier, boolean isMultiDamage) {
        if (nextMove != -1) {
            this.moveHistory.add(nextMove);
        }

        this.move = new EnemyMoveInfo(nextMove, null, baseDamage, multiplier, isMultiDamage);
        this.nextMove = this.move.nextMove;

        this.calculateDamage(this.move.baseDamage);
        if (this.move.isMultiDamage) {
            this.isMultiDmg = true;
        } else {
            this.isMultiDmg = false;
        }

    }

    public void setMove(byte nextMove, int baseDamage, int multiplier, boolean isMultiDamage) {
        this.setMove((String) null, nextMove, baseDamage, multiplier, isMultiDamage);
    }

    public void setMove(byte nextMove, int baseDamage) {
        this.setMove((String) null, nextMove, baseDamage, 0, false);
    }

    public void setMove(String moveName, byte nextMove, int baseDamage) {
        this.setMove(moveName, nextMove, baseDamage, 0, false);
    }

    public void setMove(byte nextMove) {
        this.setMove(null, nextMove, -1, 0, false);
    }

    public void setMove(String moveName, byte nextMove) {
        this.setMove(moveName, nextMove, -1, 0, false);
    }

    @Override
    public boolean hasPower(String targetID) {
        Iterator var2 = this.powers.iterator();

        AbstractPowerAP p;
        do {
            if (!var2.hasNext()) {
                return false;
            }

            p = (AbstractPowerAP) var2.next();
        } while (!p.ID.equals(targetID));

        return true;
    }

    @Override
    public AbstractPowerAP getPower(String targetID) {
        Iterator var2 = this.powers.iterator();

        AbstractPowerAP p;
        do {
            if (!var2.hasNext()) {
                return null;
            }

            p = (AbstractPowerAP) var2.next();
        } while (!p.ID.equals(targetID));

        return p;
    }

    @Override
    public void applyStartOfTurnPowers() {
        Iterator var1 = this.powers.iterator();

        while (var1.hasNext()) {
            AbstractPowerAP p = (AbstractPowerAP) var1.next();
            p.atStartOfTurn();
        }

        checkForPowerRemoval();

    }

    public void copyDamage(ArrayList<DamageInfo> oldDamage) {
        ArrayList<DamageInfoAP> newDamage = new ArrayList<>();

        for (DamageInfo damage : oldDamage) {
            newDamage.add(new DamageInfoAP(damage, this));
        }
        this.damage = newDamage;
    }


    public void startTurn() {
        applyStartOfTurnPowers();
        applyStartOfTurnPostDrawPowers();
    }


    public boolean takeRandomTurn() {
        boolean shouldContinue = true;

        this.takeTurn();

        if (SimulatorUtility.isCombatOver(state)) {
            shouldContinue = false;
        }

        endTurn();

        return shouldContinue;
    }

    public void endTurn() {
        applyEndOfTurnTriggers();
    }

    public abstract void takeTurn();

    public abstract void getMove(int num);

    public void rollMove() {
        this.getMove(AbstractDungeon.aiRng.random(99));
    }

    protected boolean lastMove(byte move) {
        if (this.moveHistory.isEmpty()) {
            return false;
        } else {
            return (Byte) this.moveHistory.get(this.moveHistory.size() - 1) == move;
        }
    }

    protected boolean lastMoveBefore(byte move) {
        if (this.moveHistory.isEmpty()) {
            return false;
        } else if (this.moveHistory.size() < 2) {
            return false;
        } else {
            return (Byte) this.moveHistory.get(this.moveHistory.size() - 2) == move;
        }
    }

    protected boolean lastTwoMoves(byte move) {
        if (this.moveHistory.size() < 2) {
            return false;
        } else {
            return (Byte) this.moveHistory.get(this.moveHistory.size() - 1) == move && (Byte) this.moveHistory.get(this.moveHistory.size() - 2) == move;
        }
    }


    public void heal(int healAmount) {
        if (!this.isDying) {
            AbstractPowerAP p;
            for (Iterator var2 = this.powers.iterator(); var2.hasNext(); healAmount = p.onHeal(healAmount)) {
                p = (AbstractPowerAP) var2.next();
            }

            this.currentHealth += healAmount;
            if (this.currentHealth > this.maxHealth) {
                this.currentHealth = this.maxHealth;
            }
        }
    }

    public void escapeNext() {
    }


    //DEPRECATED
    public void damage(DamageInfo info) {
    }

    public void render(SpriteBatch spriteBatch) {
    }

    @Override
    public int compareTo(Object o) {
        AbstractMonsterAP otherMonster = (AbstractMonsterAP) o;

        if (this.currentHealth != otherMonster.currentHealth || this.currentBlock != otherMonster.currentBlock || this.nextMove != otherMonster.nextMove || !this.moveHistory.equals(otherMonster.moveHistory)) {
            return -1;
        }

        for (AbstractPowerAP power : this.powers) {
            if (!otherMonster.hasPower(power.ID)) {
                return -1;
            }
        }

        return 0;
    }
}
