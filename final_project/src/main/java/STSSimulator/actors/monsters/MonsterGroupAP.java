package STSSimulator.actors.monsters;

import STSSimulator.GameState;
import STSSimulator.actors.monsters.exordium.*;
import STSSimulator.powers.AbstractPowerAP;
import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.MonsterGroup;
import com.megacrit.cardcrawl.monsters.exordium.*;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.random.Random;
import com.megacrit.cardcrawl.rooms.MonsterRoomBoss;

import java.util.ArrayList;
import java.util.Iterator;

public class MonsterGroupAP implements Comparable {
    public ArrayList<AbstractMonsterAP> monsters;
    public GameState state;
    public Random random = new Random();

    public MonsterGroupAP(GameState state) {
        this.monsters = new ArrayList<>();
        this.state = state;
    }

    public MonsterGroupAP(MonsterGroup copy, GameState state) throws NoSuchFieldException, IllegalAccessException {
        this.monsters = new ArrayList<>();
        this.state = state;
        for (AbstractMonster monster : copy.monsters) {
            if (AbstractDungeon.getCurrRoom() instanceof MonsterRoomBoss) {
                if (monster instanceof TheGuardian) {
                    this.monsters.add(new TheGuardianAP(monster, state));
                } else if (monster instanceof Hexaghost) {
                    this.monsters.add(new HexaghostAP(monster, state));
                } else if (monster instanceof SlimeBoss) {
                    this.monsters.add(new SlimeBossAP(monster, state));
                }
            } else {
                if (monster instanceof LouseDefensive) {
                    this.monsters.add(new LouseDefensiveAP(monster, state));
                } else if (monster instanceof LouseNormal) {
                    this.monsters.add(new LouseNormalAP(monster, state));
                } else if (monster instanceof AcidSlime_L) {
                    this.monsters.add(new AcidSlime_LAP(monster, state));
                } else if (monster instanceof AcidSlime_M) {
                    this.monsters.add(new AcidSlime_MAP(monster, state));
                } else if (monster instanceof AcidSlime_S) {
                    this.monsters.add(new AcidSlime_SAP(monster, state));
                } else if (monster instanceof SpikeSlime_L) {
                    this.monsters.add(new SpikeSlime_LAP(monster, state));
                } else if (monster instanceof SpikeSlime_M) {
                    this.monsters.add(new SpikeSlime_MAP(monster, state));
                } else if (monster instanceof SpikeSlime_S) {
                    this.monsters.add(new SpikeSlime_SAP(monster, state));
                } else if (monster instanceof Cultist) {
                    this.monsters.add(new CultistAP(monster, state));
                } else if (monster instanceof GremlinFat) {
                    this.monsters.add(new GremlinFatAP(monster, state));
                } else if (monster instanceof GremlinNob) {
                    this.monsters.add(new GremlinNobAP(monster, state));
                } else if (monster instanceof GremlinThief) {
                    this.monsters.add(new GremlinThiefAP(monster, state));
                } else if (monster instanceof GremlinTsundere) {
                    this.monsters.add(new GremlinTsundereAP(monster, state));
                } else if (monster instanceof GremlinWizard) {
                    this.monsters.add(new GremlinWizardAP(monster, state));
                } else if (monster instanceof GremlinWarrior) {
                    this.monsters.add(new GremlinWarriorAP(monster, state));
                } else if (monster instanceof JawWorm) {
                    this.monsters.add(new JawWormAP(monster, state));
                } else if (monster instanceof Lagavulin) {
                    this.monsters.add(new LagavulinAP(monster, state));
                } else if (monster instanceof FungiBeast) {
                    this.monsters.add(new FungiBeastAP(monster, state));
                } else if (monster instanceof Looter) {
                    this.monsters.add(new LooterAP(monster, state));
                } else if (monster instanceof Sentry) {
                    this.monsters.add(new SentryAP(monster, state));
                } else if (monster instanceof SlaverRed) {
                    this.monsters.add(new SlaverRedAP(monster, state));
                } else if (monster instanceof SlaverBlue) {
                    this.monsters.add(new SlaverBlueAP(monster, state));
                } else {
                    System.out.println("-----MONSTER NOT IMPLEMENTED YET! -----");
                }
            }
        }
    }


    private void applyPreTurnLogic() {
        for (AbstractMonsterAP monster : this.monsters) {
            if (!monster.isDeadOrEscaped()) {
                if (!monster.hasPower("Barricade")) {
                    monster.loseBlock();
                }

                monster.applyStartOfTurnPowers();
            }
        }
    }

    public boolean takeTurn() {
        applyPreTurnLogic();
        for (AbstractMonsterAP monster : this.getAliveMonsters()) {
            if (!monster.takeRandomTurn()) {
                return false;
            }
        }
        applyEndOfTurnPowers();
        return true;
    }

    public boolean areAllDeadOrEscaped() {
        for (AbstractMonsterAP monster : this.monsters) {
            if (!monster.isDeadOrEscaped()) {
                return false;
            }
        }
        return true;
    }

    public ArrayList<AbstractMonsterAP> getAliveMonsters() {
        ArrayList<AbstractMonsterAP> aliveMonsters = new ArrayList<>();
        for (AbstractMonsterAP monster : this.monsters) {
            if (monster.currentHealth > 0 && !monster.escaped && !monster.isDeadOrEscaped()) {
                aliveMonsters.add(monster);
            }
        }

        return aliveMonsters;
    }

    public AbstractMonsterAP getRandomMonster() {
        return this.getRandomMonster((AbstractMonsterAP) null, false);
    }

    public AbstractMonsterAP getRandomMonster(boolean aliveOnly) {
        return this.getRandomMonster((AbstractMonsterAP) null, aliveOnly);
    }

    public AbstractMonsterAP getRandomMonster(AbstractMonsterAP exception, boolean aliveOnly, Random rng) {
        if (this.areAllDeadOrEscaped()) {
            return null;
        } else {
            ArrayList tmp;
            Iterator var5;
            AbstractMonsterAP m;
            if (exception == null) {
                if (aliveOnly) {
                    tmp = new ArrayList();
                    var5 = this.monsters.iterator();

                    while (var5.hasNext()) {
                        m = (AbstractMonsterAP) var5.next();
                        if (!m.halfDead && !m.isDying && !m.isEscaping) {
                            tmp.add(m);
                        }
                    }

                    if (tmp.size() <= 0) {
                        return null;
                    } else {
                        return (AbstractMonsterAP) tmp.get(rng.random(0, tmp.size() - 1));
                    }
                } else {
                    return (AbstractMonsterAP) this.monsters.get(rng.random(0, this.monsters.size() - 1));
                }
            } else if (this.monsters.size() == 1) {
                return (AbstractMonsterAP) this.monsters.get(0);
            } else if (aliveOnly) {
                tmp = new ArrayList();
                var5 = this.monsters.iterator();

                while (var5.hasNext()) {
                    m = (AbstractMonsterAP) var5.next();
                    if (!m.halfDead && !m.isDying && !m.isEscaping && !exception.equals(m)) {
                        tmp.add(m);
                    }
                }

                if (tmp.size() == 0) {
                    return null;
                } else {
                    return (AbstractMonsterAP) tmp.get(rng.random(0, tmp.size() - 1));
                }
            } else {
                tmp = new ArrayList();
                var5 = this.monsters.iterator();

                while (var5.hasNext()) {
                    m = (AbstractMonsterAP) var5.next();
                    if (!exception.equals(m)) {
                        tmp.add(m);
                    }
                }

                return (AbstractMonsterAP) tmp.get(rng.random(0, tmp.size() - 1));
            }
        }
    }

    public AbstractMonsterAP getRandomMonster(AbstractMonsterAP exception, boolean aliveOnly) {
        if (this.areAllDeadOrEscaped()) {
            return null;
        } else {
            ArrayList tmp;
            Iterator var4;
            AbstractMonsterAP m;
            if (exception == null) {
                if (aliveOnly) {
                    tmp = new ArrayList();
                    var4 = this.monsters.iterator();

                    while (var4.hasNext()) {
                        m = (AbstractMonsterAP) var4.next();
                        if (!m.halfDead && !m.isDying && !m.isEscaping) {
                            tmp.add(m);
                        }
                    }

                    if (tmp.size() <= 0) {
                        return null;
                    } else {
                        return (AbstractMonsterAP) tmp.get(MathUtils.random(0, tmp.size() - 1));
                    }
                } else {
                    return (AbstractMonsterAP) this.monsters.get(MathUtils.random(0, this.monsters.size() - 1));
                }
            } else if (this.monsters.size() == 1) {
                return (AbstractMonsterAP) this.monsters.get(0);
            } else if (aliveOnly) {
                tmp = new ArrayList();
                var4 = this.monsters.iterator();

                while (var4.hasNext()) {
                    m = (AbstractMonsterAP) var4.next();
                    if (!m.halfDead && !m.isDying && !m.isEscaping && !exception.equals(m)) {
                        tmp.add(m);
                    }
                }

                if (tmp.size() == 0) {
                    return null;
                } else {
                    return (AbstractMonsterAP) tmp.get(MathUtils.random(0, tmp.size() - 1));
                }
            } else {
                tmp = new ArrayList();
                var4 = this.monsters.iterator();

                while (var4.hasNext()) {
                    m = (AbstractMonsterAP) var4.next();
                    if (!exception.equals(m)) {
                        tmp.add(m);
                    }
                }

                return (AbstractMonsterAP) tmp.get(MathUtils.random(0, tmp.size() - 1));
            }
        }
    }


    private void applyEndOfTurnPowers() {
        for (AbstractMonsterAP monster : this.monsters) {
            if (!monster.isDeadOrEscaped()) {
                monster.applyEndOfTurnTriggers();
            }
        }

        int powerSize = state.player.powers.size();
        for(int i = 0; i < powerSize && i < state.player.powers.size(); i++ ){
            AbstractPowerAP p = state.player.powers.get(i);
            p.atEndOfRound();
            if(powerSize < state.player.powers.size()){
                powerSize = state.player.powers.size();
                i--;
            }
        }
        state.player.checkForPowerRemoval();


        for (AbstractMonsterAP monster : this.monsters) {

            powerSize = monster.powers.size();
            for(int i = 0; i < powerSize && i < monster.powers.size(); i++ ){
                AbstractPowerAP p = monster.powers.get(i);
                p.atEndOfRound();
                if(powerSize < monster.powers.size()){
                    powerSize = monster.powers.size();
                    i--;
                }
            }
            monster.checkForPowerRemoval();
        }

    }

    @Override
    public int compareTo(Object o) {
        MonsterGroupAP otherGroup = (MonsterGroupAP) o;

        for (int i = 0; i < this.monsters.size(); i++) {
            if (this.monsters.get(i).compareTo(otherGroup.monsters.get(i)) != 0) {
                return -1;
            }
        }

        return 0;
    }
}
