package STSSimulator.actors.monsters.exordium;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.status.SlimedAP;
import STSSimulator.powers.FrailPowerAP;
import STSSimulator.powers.SplitPowerAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.exordium.SpikeSlime_L;

import java.util.ArrayList;

public class SpikeSlime_LAP extends AbstractMonsterAP {
    public boolean splitTriggered;
    private static final String WOUND_NAME = "";
    private static final String WEAK_NAME = "";

    public SpikeSlime_LAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    public SpikeSlime_LAP(int newHealth, GameState state) {
        super("SpikeSlime_L", newHealth, state);
        this.splitTriggered = false;

        if (newHealth <= maxHealth / 2) {
            splitTriggered = true;
        }

        if (AbstractDungeon.ascensionLevel >= 2) {
            this.damage.add(new DamageInfoAP(this, 18));
        } else {
            this.damage.add(new DamageInfoAP(this, 16));
        }

        this.powers.add(new SplitPowerAP(this, state));

        rollMove();
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                state.player.discardPile.addCard(new SlimedAP(state));
                state.player.discardPile.addCard(new SlimedAP(state));
                this.rollMove();
                break;
            case 2:
                break;
            case 3:
                state.monsters.monsters.add(new SpikeSlime_MAP(this.currentHealth, state));
                state.monsters.monsters.add(new SpikeSlime_MAP(this.currentHealth, state));
                this.currentHealth = 0;
                this.currentBlock = 0;
                this.isDead = true;
                this.setMove((byte) 2);
                break;
            case 4:
                SimulatorUtility.applyPower(state.player, this, new FrailPowerAP(state.player, AbstractDungeon.ascensionLevel >= 17 ? 3 : 2, true, state), AbstractDungeon.ascensionLevel >= 17 ? 3 : 2, state);
                this.rollMove();
        }

    }
    @Override
    public void getMove(int num) {
        if (AbstractDungeon.ascensionLevel >= 17) {
            if (num < 30) {
                if (this.lastTwoMoves((byte)1)) {
                    this.setMove((byte)4);
                } else {
                    this.setMove((byte)1, this.damage.get(0).base);
                }
            } else if (this.lastMove((byte)4)) {
                this.setMove((byte)1, this.damage.get(0).base);
            } else {
                this.setMove((byte)4);
            }
        } else if (num < 30) {
            if (this.lastTwoMoves((byte)1)) {
                this.setMove((byte)4);
            } else {
                this.setMove((byte)1, this.damage.get(0).base);
            }
        } else if (this.lastTwoMoves((byte)4)) {
            this.setMove((byte)1,this.damage.get(0).base);
        } else {
            this.setMove((byte)4);
        }

    }

}
