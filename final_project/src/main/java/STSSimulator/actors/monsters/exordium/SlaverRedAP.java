package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.EntanglePowerAP;
import STSSimulator.powers.VulnerablePowerAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;


public class SlaverRedAP extends AbstractMonsterAP {

    private boolean usedEntangle;

    public SlaverRedAP(AbstractMonster copy, GameState state) {
        super(copy, state);
        usedEntangle = this.moveHistory.contains((byte) 2);
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                break;
            case 2:
                SimulatorUtility.applyPower(state.player, this, new EntanglePowerAP(state.player, state), 1, state);
                this.usedEntangle = true;
                break;
            case 3:
                SimulatorUtility.doDamage(this.damage.get(1), state.player);
                if (AbstractDungeon.ascensionLevel >= 17) {
                    SimulatorUtility.applyPower(state.player, this, new VulnerablePowerAP(state.player, 2, true, state), 2, state);
                } else {
                    SimulatorUtility.applyPower(state.player, this, new VulnerablePowerAP(state.player, 2, true, state), 2, state);
                }
        }

        this.rollMove();
    }

    @Override
    public void getMove(int num) {
        if (num >= 75 && !this.usedEntangle) {
            this.setMove((byte) 2);
        } else if (num >= 55 && this.usedEntangle && !this.lastTwoMoves((byte) 1)) {
            this.setMove((byte) 1, this.damage.get(0).base);
        } else if (AbstractDungeon.ascensionLevel >= 17) {
            if (!this.lastMove((byte) 3)) {
                this.setMove((byte) 3, this.damage.get(1).base);
            } else {
                this.setMove((byte) 1, this.damage.get(0).base);
            }
        } else if (!this.lastTwoMoves((byte) 3)) {
            this.setMove((byte) 3, this.damage.get(1).base);
        } else {
            this.setMove((byte) 1, this.damage.get(0).base);
        }
    }
}
