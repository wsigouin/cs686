package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import java.util.Iterator;

public class GremlinTsundereAP extends AbstractMonsterAP {

    public int blockAmt;

    public GremlinTsundereAP(AbstractMonster copy, GameState state) {
        super(copy, state);
        if (AbstractDungeon.ascensionLevel >= 17) {
            this.blockAmt = 11;
        } else if (AbstractDungeon.ascensionLevel >= 7) {
            this.blockAmt = 8;
        } else {
            this.blockAmt = 7;
        }
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                AbstractMonsterAP target = state.monsters.getRandomMonster(true);
                SimulatorUtility.gainBlock(target, blockAmt, state);

                int aliveCount = 0;
                Iterator var2 = state.monsters.monsters.iterator();

                while (var2.hasNext()) {
                    AbstractMonsterAP m = (AbstractMonsterAP) var2.next();
                    if (!m.isDying && !m.isEscaping) {
                        ++aliveCount;
                    }
                }

                if (this.escapeNext) {
                    this.setMove(null, (byte) 99);
                } else if (aliveCount > 1) {
                    this.setMove((byte) 1);
                } else {
                    this.setMove((byte) 2, this.damage.get(0).base);
                }
                break;
            case 2:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                if (this.escapeNext) {
                    this.setMove(null, (byte) 99);
                } else {
                    this.setMove((byte) 2, this.damage.get(0).base);
                }
                break;
            case 99:
                this.escaped = true;
        }
    }

    @Override
    public void getMove(int num) {
        this.setMove((byte) 1);
    }


    public void escapeNext() {
        if (!this.cannotEscape && !this.escapeNext) {
            this.escapeNext = true;
        }
    }


}
