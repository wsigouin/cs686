package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.SharpHidePowerAP;
import STSSimulator.powers.VulnerablePowerAP;
import STSSimulator.powers.WeakPowerAP;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.exordium.TheGuardian;
import com.megacrit.cardcrawl.powers.AbstractPower;

import java.lang.reflect.Field;

public class TheGuardianAP extends AbstractMonsterAP {

    public static final int HP = 240;
    public static final int A_2_HP = 250;
    private static final int DMG_THRESHOLD = 30;
    private static final int A_2_DMG_THRESHOLD = 35;
    private static final int A_19_DMG_THRESHOLD = 40;
    private int dmgThreshold;
    private int dmgThresholdIncrease = 10;
    private int dmgTaken;
    private static final int FIERCE_BASH_DMG = 32;
    private static final int A_2_FIERCE_BASH_DMG = 36;
    private static final int ROLL_DMG = 9;
    private static final int A_2_ROLL_DMG = 10;
    private int fierceBashDamage;
    private int whirlwindDamage = 5;
    private int twinSlamDamage = 8;
    private int rollDamage;
    private int whirlwindCount = 4;
    private int DEFENSIVE_BLOCK = 20;
    private int blockAmount = 9;
    private int thornsDamage = 3;
    private int VENT_DEBUFF = 2;

    private boolean isOpen = true;
    private boolean closeUpTriggered = false;

    public TheGuardianAP(AbstractMonster copy, GameState state) throws NoSuchFieldException, IllegalAccessException {
        super(copy, state);

        this.isOpen = !this.hasPower("Sharp Hide");

        Field field = TheGuardian.class.getDeclaredField("dmgThreshold");
        field.setAccessible(true);
        dmgThreshold = (int) field.get(copy);
        System.out.println("Damage Thresh: " + dmgThreshold);

        field = TheGuardian.class.getDeclaredField("dmgTaken");
        field.setAccessible(true);
        dmgTaken = (int) field.get(copy);
        System.out.println("Damage Taken: " + dmgTaken);
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                this.useCloseUp();
                break;
            case 2:
                this.useFierceBash();
                break;
            case 3:
                this.useRollAttack();
                break;
            case 4:
                this.useTwinSmash();
                break;
            case 5:
                this.useWhirlwind();
                break;
            case 6:
                this.useChargeUp();
                break;
            case 7:
                this.useVentSteam();
                break;
            default:
                System.out.println(this.name + " ERROR");
        }

    }

    private void useFierceBash() {
        SimulatorUtility.doDamage(this.damage.get(0), state.player);
        this.setMove((byte) 7);
    }

    private void useVentSteam() {
        SimulatorUtility.applyPower(state.player, this, new WeakPowerAP(state.player, this.VENT_DEBUFF, true, state), this.VENT_DEBUFF, state);
        SimulatorUtility.applyPower(state.player, this, new VulnerablePowerAP(state.player, this.VENT_DEBUFF, true, state), this.VENT_DEBUFF, state);

        this.setMove((byte) 5, this.whirlwindDamage, this.whirlwindCount, true);
    }

    private void useCloseUp() {
        if (AbstractDungeon.ascensionLevel >= 19) {
            SimulatorUtility.applyPower(this, this, new SharpHidePowerAP(this, this.thornsDamage + 1, state), this.thornsDamage + 1, state);
        } else {
            SimulatorUtility.applyPower(this, this, new SharpHidePowerAP(this, this.thornsDamage, state), this.thornsDamage, state);

        }

        this.setMove((byte) 3, this.damage.get(1).base);
    }

    private void useTwinSmash() {
        this.loseBlock();
        this.dmgTaken = 0;
        this.isOpen = true;
        this.closeUpTriggered = true;
        for (int i = 0; i < 2; i++) {
            SimulatorUtility.doDamage(this.damage.get(3), state.player);
        }

        SimulatorUtility.removePower(this, this, "Sharp Hide", null, state);
        this.setMove((byte) 5, this.whirlwindDamage, this.whirlwindCount, true);
    }

    private void useRollAttack() {
        SimulatorUtility.doDamage(this.damage.get(1), state.player);
        this.setMove((byte) 4, this.twinSlamDamage, 2, true);
    }

    private void useWhirlwind() {

        for (int i = 0; i < this.whirlwindCount; ++i) {
            SimulatorUtility.doDamage(this.damage.get(2), state.player);
        }

        this.setMove((byte) 6);
    }

    private void useChargeUp() {
        SimulatorUtility.gainBlock(this, this.blockAmount, state);
        this.setMove((byte) 2, this.damage.get(0).base);
    }

    @Override
    public void getMove(int num) {
        if (this.isOpen) {
            this.setMove((byte) 6);
        } else {
            this.setMove((byte) 3, this.damage.get(1).base);
        }

    }


    public void damage(DamageInfo info) {
        int tmpHealth = this.currentHealth;
        super.damage(info);
        if (this.isOpen && !this.closeUpTriggered && tmpHealth > this.currentHealth && !this.isDeadOrEscaped()) {
            this.dmgTaken += tmpHealth - this.currentHealth;
            if (this.getPower("Mode Shift") != null) {
                AbstractPower var10000 = this.getPower("Mode Shift");
                var10000.amount -= tmpHealth - this.currentHealth;
            }

            if (this.dmgTaken >= this.dmgThreshold) {
                this.dmgTaken = 0;
                SimulatorUtility.gainBlock(this, this.DEFENSIVE_BLOCK, state);
                this.dmgThreshold += this.dmgThresholdIncrease;
                this.isOpen = false;
                this.closeUpTriggered = true;
            }
        }

    }


}
