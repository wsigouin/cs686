package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class SpikeSlime_SAP extends AbstractMonsterAP {
    public SpikeSlime_SAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    @Override
    public void takeTurn() {
        if (this.nextMove == 1) {
            SimulatorUtility.doDamage(this.damage.get(0), state.player);
            this.setMove(null, (byte) 2);
        }
    }

    @Override
    public void getMove(int num) {
        this.setMove((byte) 1, this.damage.get(0).base);
    }

}
