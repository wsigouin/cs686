package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.exordium.Looter;

import java.lang.reflect.Field;

public class LooterAP extends AbstractMonsterAP {
    private int slashCount = 0;
    private int stolenGold = 0;
    private int escapeDef = 6;


    private final int goldAmt;

    public LooterAP(AbstractMonster copy, GameState state) throws NoSuchFieldException, IllegalAccessException {
        super(copy, state);
        if (AbstractDungeon.ascensionLevel >= 17) {
            this.goldAmt = 20;
        } else {
            this.goldAmt = 15;
        }


        Field field = Looter.class.getDeclaredField("slashCount");
        field.setAccessible(true);
        slashCount = (int) field.get(copy);
        System.out.println("Slash Count: " + slashCount);

        field = Looter.class.getDeclaredField("stolenGold");
        field.setAccessible(true);
        stolenGold = (int) field.get(copy);
        System.out.println("Stolen Gold: " + stolenGold);
    }

    @Override
    public void takeTurn() {
        int goldToSteal;
        switch (this.nextMove) {
            case 1:
                goldToSteal = Math.min(this.goldAmt, state.player.gold);
                this.stolenGold = this.stolenGold + goldToSteal;
                state.player.gold -= goldToSteal;
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                ++this.slashCount;
                if (this.slashCount == 2) {
                    if (AbstractDungeon.aiRng.randomBoolean(0.5F)) {
                        this.setMove((byte) 2);
                    } else {
                        this.setMove((byte) 4, this.damage.get(1).base);
                    }
                } else {
                    this.setMove((byte) 4, this.damage.get(0).base);
                }
                break;
            case 2:
                SimulatorUtility.gainBlock(this, this.escapeDef, state);
                this.setMove((byte) 3);
                break;
            case 3:
                this.escaped = true;
                break;
            case 4:
                ++this.slashCount;
                goldToSteal = Math.min(this.goldAmt, state.player.gold);
                this.stolenGold = this.stolenGold + goldToSteal;
                state.player.gold -= goldToSteal;
                SimulatorUtility.doDamage(this.damage.get(1), state.player);

                this.setMove((byte) 2);
        }
    }

    @Override
    public void getMove(int num) {
        this.setMove((byte) 1, this.damage.get(0).base);
    }


    @Override
    public void die() {
        if (this.stolenGold > 0) {
            state.player.gold += stolenGold;
        }
        super.die();
    }
}
