package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class SampleMonsterAP extends AbstractMonsterAP {
    public SampleMonsterAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    @Override
    public void takeTurn() {

    }

    @Override
    public void getMove(int num) {

    }

}
