package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.StrengthPowerAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class JawWormAP extends AbstractMonsterAP {
    public int bellowStr;
    public int bellowBlock;
    public int thrashBlock;


    public JawWormAP(AbstractMonster copy, GameState state) {
        super(copy, state);
        if (AbstractDungeon.ascensionLevel >= 17) {
            this.bellowStr = 5;
            this.bellowBlock = 9;
        } else if (AbstractDungeon.ascensionLevel >= 2) {
            this.bellowStr = 4;
            this.bellowBlock = 6;
            this.thrashBlock = 5;
        } else {
            this.bellowStr = 3;
            this.bellowBlock = 6;
            this.thrashBlock = 5;
        }

    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                break;
            case 2:
                SimulatorUtility.applyPower(this, this, new StrengthPowerAP(this, this.bellowStr, state), this.bellowStr, state);
                SimulatorUtility.gainBlock(this, this.bellowBlock, state);
                break;
            case 3:
                SimulatorUtility.doDamage(this.damage.get(1), state.player);
                SimulatorUtility.gainBlock(this, this.thrashBlock, state);

        }
        this.rollMove();
    }

    @Override
    public void getMove(int num) {
        if (num < 25) {
            if (this.lastMove((byte) 1)) {
                if (AbstractDungeon.aiRng.randomBoolean(0.5625F)) {
                    this.setMove((byte) 2);
                } else {
                    this.setMove((byte) 3, this.damage.get(1).base);
                }
            } else {
                this.setMove((byte) 1, this.damage.get(0).base);
            }
        } else if (num < 55) {
            if (this.lastTwoMoves((byte) 3)) {
                if (AbstractDungeon.aiRng.randomBoolean(0.357F)) {
                    this.setMove((byte) 1, this.damage.get(0).base);
                } else {
                    this.setMove((byte) 2);
                }
            } else {
                this.setMove((byte) 3, this.damage.get(1).base);
            }
        } else if (this.lastMove((byte) 2)) {
            if (AbstractDungeon.aiRng.randomBoolean(0.416F)) {
                this.setMove((byte) 1, this.damage.get(0).base);
            } else {
                this.setMove((byte) 3, this.damage.get(1).base);
            }
        } else {
            this.setMove((byte) 2);
        }
    }

}
