package STSSimulator.actors.monsters.exordium;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.status.SlimedAP;
import STSSimulator.powers.WeakPowerAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.exordium.AcidSlime_M;


public class AcidSlime_MAP extends AbstractMonsterAP {
    public AcidSlime_MAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    public AcidSlime_MAP(int newHealth, GameState state) {
        super("AcidSlime_M", newHealth, state);

        if (AbstractDungeon.ascensionLevel >= 2) {
            this.damage.add(new DamageInfoAP(this, 8));
            this.damage.add(new DamageInfoAP(this, 12));
        } else {
            this.damage.add(new DamageInfoAP(this, 7));
            this.damage.add(new DamageInfoAP(this, 10));
        }
        rollMove();
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                state.player.discardPile.addCard(new SlimedAP(state));

                rollMove();
                break;
            case 2:
                SimulatorUtility.doDamage(this.damage.get(1), state.player);
                rollMove();
            case 3:
            default:
                break;
            case 4:
                SimulatorUtility.applyPower(state.player, this, new WeakPowerAP(state.player, 1, true, state), 1, state);
                rollMove();
        }
    }

    @Override
    public void getMove(int num) {
        if (AbstractDungeon.ascensionLevel >= 17) {
            if (num < 40) {
                if (this.lastTwoMoves((byte) 1)) {
                    if (AbstractDungeon.aiRng.randomBoolean()) {
                        this.setMove((byte) 2, this.damage.get(1).base);
                    } else {
                        this.setMove((byte) 4);
                    }
                } else {
                    this.setMove((byte) 1, this.damage.get(0).base);
                }
            } else if (num < 80) {
                if (this.lastTwoMoves((byte) 2)) {
                    if (AbstractDungeon.aiRng.randomBoolean(0.5F)) {
                        this.setMove((byte) 1, this.damage.get(0).base);
                    } else {
                        this.setMove((byte) 4);
                    }
                } else {
                    this.setMove((byte) 2, this.damage.get(1).base);
                }
            } else if (this.lastMove((byte) 4)) {
                if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
                    this.setMove((byte) 1, this.damage.get(0).base);
                } else {
                    this.setMove((byte) 2, this.damage.get(1).base);
                }
            } else {
                this.setMove((byte) 4);
            }
        } else if (num < 30) {
            if (this.lastTwoMoves((byte) 1)) {
                if (AbstractDungeon.aiRng.randomBoolean()) {
                    this.setMove((byte) 2, this.damage.get(1).base);
                } else {
                    this.setMove((byte) 4);
                }
            } else {
                this.setMove((byte) 1, this.damage.get(0).base);
            }
        } else if (num < 70) {
            if (this.lastMove((byte) 2)) {
                if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
                    this.setMove((byte) 1, this.damage.get(0).base);
                } else {
                    this.setMove((byte) 4);
                }
            } else {
                this.setMove((byte) 2, this.damage.get(1).base);
            }
        } else if (this.lastTwoMoves((byte) 4)) {
            if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
                this.setMove((byte) 1, this.damage.get(0).base);
            } else {
                this.setMove((byte) 2, this.damage.get(1).base);
            }
        } else {
            this.setMove((byte) 4);
        }
    }

}
