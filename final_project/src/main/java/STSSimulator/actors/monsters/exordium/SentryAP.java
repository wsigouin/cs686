package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.status.DazedAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class SentryAP extends AbstractMonsterAP {
    private final int dazedAmt;

    public SentryAP(AbstractMonster copy, GameState state) {
        super(copy, state);

        if (AbstractDungeon.ascensionLevel >= 18) {
            this.dazedAmt = 3;
        } else {
            this.dazedAmt = 2;
        }
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 3:
                for (int i = 0; i < dazedAmt; i++) {
                    state.player.discardPile.addCard(new DazedAP(state));
                }
                break;
            case 4:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
        }
        this.rollMove();
    }

    @Override
    public void getMove(int num) {
        if (this.lastMove((byte) 4)) {
            this.setMove((byte) 3);
        } else {
            this.setMove((byte) 4, this.damage.get(0).base);
        }
    }

}