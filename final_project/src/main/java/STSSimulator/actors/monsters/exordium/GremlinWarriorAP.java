package STSSimulator.actors.monsters.exordium;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class GremlinWarriorAP extends AbstractMonsterAP {
    public GremlinWarriorAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(new DamageInfoAP(this, this.damage.get(0).base, DamageInfo.DamageType.NORMAL), state.player);

                if (this.escapeNext) {
                    this.setMove(null, (byte) 99);
                } else {
                    this.setMove(null, (byte) 1, this.damage.get(0).base);
                }
                break;
            case 99:
                this.escaped = true;
        }
    }

    @Override
    public void getMove(int num) {
        this.setMove(null, (byte) 1, this.damage.get(0).base);
    }

}
