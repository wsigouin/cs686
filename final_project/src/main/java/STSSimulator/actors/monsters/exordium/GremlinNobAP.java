package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.AngerPowerAP;
import STSSimulator.powers.VulnerablePowerAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class GremlinNobAP extends AbstractMonsterAP {
    private boolean usedBellow;
    private boolean canVuln;


    public GremlinNobAP(AbstractMonster copy, GameState state) {
        super(copy, state);
        this.usedBellow = true;
        this.type = AbstractMonster.EnemyType.ELITE;
        this.canVuln = true;
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                break;
            case 2:
                SimulatorUtility.doDamage(this.damage.get(1), state.player);
                if (this.canVuln) {
                    SimulatorUtility.applyPower(state.player, this, new VulnerablePowerAP(this, 2, true, state), 2, state);
                }
                break;
            case 3:
                if (AbstractDungeon.ascensionLevel >= 18) {
                    SimulatorUtility.applyPower(this, this, new AngerPowerAP(this, 3, state), 3, state);
                } else {
                    SimulatorUtility.applyPower(this, this, new AngerPowerAP(this, 2, state), 2, state);
                }
        }

        this.rollMove();
    }

    @Override
    public void getMove(int num) {
        if (!this.usedBellow) {
            this.usedBellow = true;
            this.setMove(null, (byte) 3);
        } else {
            if (AbstractDungeon.ascensionLevel >= 18) {
                if (!this.lastMove((byte) 2) && !this.lastMoveBefore((byte) 2)) {
                    if (this.canVuln) {
                        this.setMove((byte) 2, this.damage.get(1).base);
                    } else {
                        this.setMove((byte) 2, this.damage.get(1).base);
                    }

                    return;
                }

                if (this.lastTwoMoves((byte) 1)) {
                    if (this.canVuln) {
                        this.setMove((byte) 2, this.damage.get(1).base);
                    } else {
                        this.setMove((byte) 2, this.damage.get(1).base);
                    }
                } else {
                    this.setMove((byte) 1, this.damage.get(0).base);
                }
            } else {
                if (num < 33) {
                    if (this.canVuln) {
                        this.setMove((byte) 2, this.damage.get(1).base);
                    } else {
                        this.setMove((byte) 2, this.damage.get(1).base);
                    }

                    return;
                }

                if (this.lastTwoMoves((byte) 1)) {
                    if (this.canVuln) {
                        this.setMove((byte) 2, this.damage.get(1).base);
                    } else {
                        this.setMove((byte) 2, this.damage.get(1).base);
                    }
                } else {
                    this.setMove((byte) 1, this.damage.get(0).base);
                }
            }

        }

    }
}
