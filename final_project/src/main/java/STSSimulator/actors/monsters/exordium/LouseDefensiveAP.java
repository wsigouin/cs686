package STSSimulator.actors.monsters.exordium;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.WeakPowerAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.monsters.AbstractMonster;


public class LouseDefensiveAP extends AbstractMonsterAP {
    public LouseDefensiveAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 3:
                SimulatorUtility.doDamage(new DamageInfoAP(this, this.damage.get(0).base, DamageInfo.DamageType.NORMAL), state.player);
                //AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_LIGHT));
                break;
            case 4:
                SimulatorUtility.applyPower(state.player, this, new WeakPowerAP(this, 2, true, state), 2, state);
                //AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, 2, true), 2));
        }
        this.rollMove();
    }

    @Override
    public void getMove(int num) {
        if (num < 25) {
            if (this.lastTwoMoves((byte) 4)) {
                this.setMove((byte) 3, this.damage.get(0).base);
            } else {
                this.setMove(null, (byte) 4);
            }
        } else if (this.lastTwoMoves((byte) 3)) {
            this.setMove(null, (byte) 4);
        } else {
            this.setMove((byte) 3, this.damage.get(0).base);
        }
    }


}
