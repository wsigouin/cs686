package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class GremlinWizardAP extends AbstractMonsterAP {
    public int currentCharge = 1;

    public GremlinWizardAP(AbstractMonster copy, GameState state) {
        super(copy, state);
        if (nextMove == (byte) 1) {
            this.currentCharge = 3;
        } else if (moveHistory.size() == 0) {
            this.currentCharge = 1;
        } else if (lastTwoMoves((byte) 2)) {
            this.currentCharge = 2;
        } else if (lastMove((byte) 2)) {
            this.currentCharge = 1;
        } else {
            this.currentCharge = 0;
        }
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                this.currentCharge = 0;
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                if (this.escapeNext) {
                    this.setMove(null, (byte) 99);
                } else if (AbstractDungeon.ascensionLevel >= 17) {
                    this.setMove((byte) 1, this.damage.get(0).base);
                } else {
                    this.setMove((byte) 2);
                }
                break;
            case 2:
                ++this.currentCharge;
                if (this.escapeNext) {
                    this.setMove(null, (byte) 99);
                } else if (this.currentCharge == 3) {
                    this.setMove((byte) 1, this.damage.get(0).base);
                } else {
                    this.setMove((byte) 2);
                }
                break;
            case 99:
                this.escaped = true;
        }
    }

    @Override
    public void getMove(int num) {
        this.setMove((byte) 2);
    }

}
