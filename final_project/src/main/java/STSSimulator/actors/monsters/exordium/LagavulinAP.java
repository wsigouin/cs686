package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.DexterityPowerAP;
import STSSimulator.powers.StrengthPowerAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.exordium.Lagavulin;
import com.megacrit.cardcrawl.monsters.exordium.Looter;

import java.lang.reflect.Field;

public class LagavulinAP extends AbstractMonsterAP {
    public boolean isAsleep = true;
    public int idleCount = 0;
    public int attackDmg;
    public int debuff;
    public int debuffTurnCount = 0;
    //TODO REFLECT
    public LagavulinAP(AbstractMonster copy, GameState state) throws NoSuchFieldException, IllegalAccessException {
        super(copy, state);

        Field field = Lagavulin.class.getDeclaredField("isOut");
        field.setAccessible(true);
        isAsleep = (boolean) field.get(copy);

        field = Lagavulin.class.getDeclaredField("idleCount");
        field.setAccessible(true);
        idleCount = (int) field.get(copy);

        field = Lagavulin.class.getDeclaredField("debuffTurnCount");
        field.setAccessible(true);
        debuffTurnCount = (int) field.get(copy);

        if (AbstractDungeon.ascensionLevel >= 3) {
            this.attackDmg = 20;
        } else {
            this.attackDmg = 18;
        }

        if (AbstractDungeon.ascensionLevel >= 18) {
            this.debuff = -2;
        } else {
            this.debuff = -1;
        }

    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                this.debuffTurnCount = 0;
                SimulatorUtility.applyPower(state.player, this, new DexterityPowerAP(this, this.debuff, state), this.debuff, state);
                SimulatorUtility.applyPower(state.player, this, new StrengthPowerAP(this, this.debuff, state), this.debuff, state);

                this.rollMove();
            case 2:
            default:
                break;
            case 3:
                ++this.debuffTurnCount;
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                this.rollMove();
                break;
            case 4:
                this.rollMove();
                break;
            case 5:
                ++this.idleCount;
                if (this.idleCount >= 3) {
                    this.isAsleep = false;
                    this.setMove((byte) 3);
                } else {
                    this.setMove((byte) 5);
                }

                switch (this.idleCount) {
                    case 1:
                        this.rollMove();
                        return;
                    case 2:
                        this.rollMove();
                        return;
                    default:
                        return;
                }
            case 6:
                this.setMove((byte) 3, this.damage.get(0).base);
                this.isAsleep = false;
                this.rollMove();
        }
    }

    @Override
    public void getMove(int num) {
        if (!this.isAsleep) {
            if (this.debuffTurnCount < 2) {
                if (this.lastTwoMoves((byte) 3)) {
                    this.setMove((byte) 1);
                } else {
                    this.setMove((byte) 3, this.damage.get(0).base);
                }
            } else {
                this.setMove((byte) 1);
            }
        } else {
            this.setMove((byte) 5);
        }
    }

    public void damage(DamageInfo info) {
        int previousHealth = this.currentHealth;
        super.damage(info);
        if (this.currentHealth != previousHealth && this.isAsleep) {
            this.setMove((byte) 4);
            this.isAsleep = false;
            SimulatorUtility.reducePower(this, this, "Metallicize", 8, state);
        }

    }

}
