package STSSimulator.actors.monsters.exordium;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.status.SlimedAP;
import STSSimulator.powers.SplitPowerAP;
import STSSimulator.powers.WeakPowerAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.exordium.AcidSlime_L;

import java.util.ArrayList;

public class AcidSlime_LAP extends AbstractMonsterAP {
    public boolean splitTriggered;
    private static final String WOUND_NAME = "";
    private static final String WEAK_NAME = "";

    public AcidSlime_LAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    public AcidSlime_LAP(int newHealth, GameState state) {
        super("AcidSlime_L", newHealth, state);

        this.splitTriggered = false;

        if (newHealth <= maxHealth / 2) {
            splitTriggered = true;
        }

        if (AbstractDungeon.ascensionLevel >= 2) {
            this.damage.add(new DamageInfoAP(this, 12));
            this.damage.add(new DamageInfoAP(this, 18));
        } else {
            this.damage.add(new DamageInfoAP(this, 11));
            this.damage.add(new DamageInfoAP(this, 16));
        }

        this.powers.add(new SplitPowerAP(this, state));
        rollMove();

    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                state.player.discardPile.addCard(new SlimedAP(state));
                this.rollMove();
                break;
            case 2:
                SimulatorUtility.doDamage(this.damage.get(1), state.player);
                this.rollMove();
                break;
            case 3:
                state.monsters.monsters.add(new AcidSlime_MAP(this.currentHealth, state));
                state.monsters.monsters.add(new AcidSlime_MAP(this.currentHealth, state));
                this.currentHealth = 0;
                this.currentBlock = 0;
                this.isDead = true;
                this.setMove((byte) 5);
                break;
            case 4:
                SimulatorUtility.applyPower(state.player, this, new WeakPowerAP(state.player, 2, true, state), 2, state);
                this.rollMove();
                break;
            default:
                break;
        }

    }

    @Override
    public void getMove(int num) {
        if (AbstractDungeon.ascensionLevel >= 17) {
            if (num < 40) {
                if (this.lastTwoMoves((byte) 1)) {
                    if (AbstractDungeon.aiRng.randomBoolean(0.6F)) {
                        this.setMove((byte) 2, this.damage.get(1).base);
                    } else {
                        this.setMove(WEAK_NAME, (byte) 4);
                    }
                } else {
                    this.setMove(WOUND_NAME, (byte) 1, this.damage.get(0).base);
                }
            } else if (num < 70) {
                if (this.lastTwoMoves((byte) 2)) {
                    if (AbstractDungeon.aiRng.randomBoolean(0.6F)) {
                        this.setMove(WOUND_NAME, (byte) 1, this.damage.get(0).base);
                    } else {
                        this.setMove(WEAK_NAME, (byte) 4);
                    }
                } else {
                    this.setMove((byte) 2, this.damage.get(1).base);
                }
            } else if (this.lastMove((byte) 4)) {
                if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
                    this.setMove(WOUND_NAME, (byte) 1, this.damage.get(0).base);
                } else {
                    this.setMove((byte) 2, this.damage.get(1).base);
                }
            } else {
                this.setMove(WEAK_NAME, (byte) 4);
            }
        } else if (num < 30) {
            if (this.lastTwoMoves((byte) 1)) {
                if (AbstractDungeon.aiRng.randomBoolean()) {
                    this.setMove((byte) 2, this.damage.get(1).base);
                } else {
                    this.setMove(WEAK_NAME, (byte) 4);
                }
            } else {
                this.setMove(WOUND_NAME, (byte) 1, this.damage.get(0).base);
            }
        } else if (num < 70) {
            if (this.lastMove((byte) 2)) {
                if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
                    this.setMove(WOUND_NAME, (byte) 1, this.damage.get(0).base);
                } else {
                    this.setMove(WEAK_NAME, (byte) 4);
                }
            } else {
                this.setMove((byte) 2, this.damage.get(1).base);
            }
        } else if (this.lastTwoMoves((byte) 4)) {
            if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
                this.setMove(WOUND_NAME, (byte) 1, this.damage.get(0).base);
            } else {
                this.setMove((byte) 2, this.damage.get(1).base);
            }
        } else {
            this.setMove(WEAK_NAME, (byte) 4);
        }
    }

}
