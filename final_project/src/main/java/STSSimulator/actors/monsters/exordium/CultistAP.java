package STSSimulator.actors.monsters.exordium;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.RitualPowerAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class CultistAP extends AbstractMonsterAP {
    private int ritualAmount;

    public CultistAP(AbstractMonster copy, GameState state) {
        super(copy, state);
        this.ritualAmount = 3;
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(new DamageInfoAP(this, this.damage.get(0).base, DamageInfo.DamageType.NORMAL), state.player);
                break;
            case 3:
                SimulatorUtility.applyPower(this, this, new RitualPowerAP(this, ritualAmount, state), ritualAmount, state);

        }

        this.rollMove();
    }

    @Override
    public void getMove(int num) {
        this.setMove((byte) 1, this.damage.get(0).base);
    }

}