package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.status.SlimedAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class SlimeBossAP extends AbstractMonsterAP {
    private int numSlime;

    public SlimeBossAP(AbstractMonster copy, GameState state) {
        super(copy, state);

        if (AbstractDungeon.ascensionLevel >= 19) {
            numSlime = 5;
        } else {
            numSlime = 3;
        }
    }


    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(this.damage.get(1), state.player);
                break;
            case 2:
                this.setMove((byte) 1, this.damage.get(1).base);
                break;
            case 3:
                state.monsters.monsters.add(new AcidSlime_LAP(this.currentHealth, state));
                state.monsters.monsters.add(new SpikeSlime_LAP(this.currentHealth, state));
                this.currentHealth = 0;
                this.currentBlock = 0;
                this.isDead = true;
                this.setMove((byte) 5);
                break;
            case 4:
                for (int i = 0; i < numSlime; i++) {
                    state.player.discardPile.addCard(new SlimedAP(state));
                }

                this.setMove((byte) 2);
                break;
            default:
                break;
        }

    }

    @Override
    public void getMove(int num) {
        System.out.println("Slime Boss: Shouldn't call this!!!!!!!!!!!!!!!!!!!!!!!");
    }

}
