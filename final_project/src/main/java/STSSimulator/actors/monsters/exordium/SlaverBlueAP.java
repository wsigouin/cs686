package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.WeakPowerAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class SlaverBlueAP extends AbstractMonsterAP {

    public SlaverBlueAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                break;
            case 4:
                SimulatorUtility.doDamage(this.damage.get(1), state.player);
                if (AbstractDungeon.ascensionLevel >= 17) {
                    SimulatorUtility.applyPower(state.player, this, new WeakPowerAP(state.player, 2, true, state), 2, state);
                } else {
                    SimulatorUtility.applyPower(state.player, this, new WeakPowerAP(state.player, 1, true, state), 1, state);
                }
        }
        this.rollMove();
    }

    @Override
    public void getMove(int num) {
        if (num >= 40 && !this.lastTwoMoves((byte) 1)) {
            this.setMove((byte) 1, this.damage.get(0).base);
        } else if (AbstractDungeon.ascensionLevel >= 17) {
            if (!this.lastMove((byte) 4)) {
                this.setMove((byte) 4, this.damage.get(1).base);
            } else {
                this.setMove((byte) 1, this.damage.get(0).base);
            }
        } else if (!this.lastTwoMoves((byte) 4)) {
            this.setMove((byte) 4, this.damage.get(1).base);
        } else {
            this.setMove((byte) 1, this.damage.get(0).base);
        }
    }
}
