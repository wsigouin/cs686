package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.cards.status.BurnAP;
import STSSimulator.powers.StrengthPowerAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.cards.status.Burn;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.exordium.Hexaghost;

import java.lang.reflect.Field;
import java.util.Iterator;

public class HexaghostAP extends AbstractMonsterAP {
    private final int strAmount;
    private final int searBurnCount;
    private boolean burnUpgraded;
    public int orbActiveCount;
    private int infernoHits = 6;
    private int strengthenBlockAmt = 12;
    private int fireTackleCount = 2;


    public HexaghostAP(AbstractMonster copy, GameState state) throws NoSuchFieldException, IllegalAccessException {
        super(copy, state);

        Field field = Hexaghost.class.getDeclaredField("orbActiveCount");
        field.setAccessible(true);
        orbActiveCount = (int) field.get(copy);
        System.out.println("Perceived Orbs: " + orbActiveCount);


        field = Hexaghost.class.getDeclaredField("burnUpgraded");
        field.setAccessible(true);
        this.burnUpgraded = (boolean) field.get(copy);

        System.out.println("Burn Upgraded: " + burnUpgraded);

        if (AbstractDungeon.ascensionLevel >= 19) {
            this.strAmount = 3;
            this.searBurnCount = 2;
        } else if (AbstractDungeon.ascensionLevel >= 4) {
            this.strAmount = 2;
            this.searBurnCount = 1;
        } else {
            this.strAmount = 2;
            this.searBurnCount = 1;
        }
        if(this.nextMove == -1){
            this.nextMove = 5;
        }
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                for (int i = 0; i < 6; ++i) {
                    SimulatorUtility.doDamage(this.damage.get(2), state.player);
                }

                this.orbActiveCount = 0;
                this.rollMove();
                break;
            case 2:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                ++this.orbActiveCount;

                if (orbActiveCount == 6) {
                    this.setMove((byte) 6, ((DamageInfo) this.damage.get(3)).base, this.infernoHits, true);

                } else {
                    this.rollMove();
                }

                break;
            case 3:
                SimulatorUtility.gainBlock(this, this.strengthenBlockAmt, state);
                SimulatorUtility.applyPower(this, this, new StrengthPowerAP(this, this.strAmount, state), this.strAmount, state);
                ++this.orbActiveCount;

                if (orbActiveCount == 6) {
                    this.setMove((byte) 6, ((DamageInfo) this.damage.get(3)).base, this.infernoHits, true);
                } else {
                    this.rollMove();
                }

                break;
            case 4:
                SimulatorUtility.doDamage(this.damage.get(1), state.player);

                for (int i = 0; i < searBurnCount; i++) {
                    BurnAP c = new BurnAP(state);

                    if (this.burnUpgraded) {
                        c.upgrade();
                    }

                    state.player.discardPile.addCard(c);
                }
                this.rollMove();
                break;
            case 5:
                ++this.orbActiveCount;
                int d = state.player.currentHealth / 12 + 1;
                this.damage.get(2).base = d;
                this.applyPowers();
                this.setMove((byte) 1, this.damage.get(2).base, 6, true);
                break;
            case 6:

                for (int i = 0; i < this.infernoHits; ++i) {
                    SimulatorUtility.doDamage(this.damage.get(3), state.player);
                }

                if (!this.burnUpgraded) {
                    this.burnUpgraded = true;
                }

                this.orbActiveCount = 0;
                this.rollMove();
                break;
            default:
                //System.out.println("ERROR: Default Take Turn was called on " + this.name);
        }

    }

    public void upgradeBurns() {
        Iterator var1 = state.player.discardPile.group.iterator();

        AbstractCardAP c;
        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            if (c instanceof BurnAP) {
                c.upgrade();
            }
        }

        var1 = state.player.drawPile.group.iterator();

        while (var1.hasNext()) {
            c = (AbstractCardAP) var1.next();
            if (c instanceof BurnAP) {
                c.upgrade();
            }
        }


        for (int i = 0; i < searBurnCount; i++) {
            c = new BurnAP(new Burn(), state);

            if (this.burnUpgraded) {
                c.upgrade();
            }

            state.player.discardPile.addCard(c);
        }


    }

    @Override
    public void getMove(int num) {

        switch (this.orbActiveCount) {
            case 0:
                this.setMove((byte) 4, this.damage.get(1).base);
                break;
            case 1:
                this.setMove((byte) 2, this.damage.get(0).base, this.fireTackleCount, true);
                break;
            case 2:
                this.setMove((byte) 4, this.damage.get(1).base);
                break;
            case 3:
                this.setMove((byte) 3);
                break;
            case 4:
                this.setMove((byte) 2, this.damage.get(0).base, this.fireTackleCount, true);
                break;
            case 5:
                this.setMove((byte) 4, this.damage.get(1).base);
                break;
            case 6:
                this.setMove((byte) 6, this.damage.get(3).base, this.infernoHits, true);
        }
    }


}
