package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.FrailPowerAP;
import STSSimulator.powers.WeakPowerAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class GremlinFatAP extends AbstractMonsterAP {
    public GremlinFatAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 2:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                SimulatorUtility.applyPower(state.player, this, new WeakPowerAP(state.player, 1, true, state), 1, state);
                if (AbstractDungeon.ascensionLevel >= 17) {
                    SimulatorUtility.applyPower(state.player, this, new FrailPowerAP(state.player, 1, true, state), 1, state);
                }

                if (this.escapeNext) {
                    this.setMove(null, (byte) 99);
                } else {
                    this.rollMove();
                }
                break;
            case 99:
                this.escaped = true;
        }
    }

    @Override
    public void getMove(int num) {
        this.setMove((byte) 2, this.damage.get(0).base);
    }

}
