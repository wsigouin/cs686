package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.WeakPowerAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class AcidSlime_SAP extends AbstractMonsterAP {
    public AcidSlime_SAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                this.setMove(null, (byte) 2);
                break;
            case 2:
                SimulatorUtility.applyPower(state.player, this, new WeakPowerAP(state.player, 1, true, state), 1, state);
                this.setMove((byte) 1, this.damage.get(0).base);
        }
    }

    @Override
    public void getMove(int num) {
        if (AbstractDungeon.ascensionLevel >= 17) {
            if (this.lastTwoMoves((byte) 1)) {
                this.setMove((byte) 1, this.damage.get(0).base);
            } else {
                this.setMove(null, (byte) 2);
            }
        } else if (AbstractDungeon.aiRng.randomBoolean()) {
            this.setMove((byte) 1, this.damage.get(0).base);
        } else {
            this.setMove((byte) 2);
        }
    }

}
