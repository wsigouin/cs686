package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.StrengthPowerAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class FungiBeastAP extends AbstractMonsterAP {
    private int strAmt;

    public FungiBeastAP(AbstractMonster copy, GameState state) {
        super(copy, state);

        if (AbstractDungeon.ascensionLevel >= 2) {
            this.strAmt = 4;
        } else {
            this.strAmt = 3;
        }
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                break;
            case 2:
                if (AbstractDungeon.ascensionLevel >= 17) {
                    SimulatorUtility.applyPower(this, this, new StrengthPowerAP(this, this.strAmt + 1, state), this.strAmt + 1, state);
                } else {
                    SimulatorUtility.applyPower(this, this, new StrengthPowerAP(this, this.strAmt, state), this.strAmt, state);
                }
        }

        this.rollMove();
    }

    @Override
    public void getMove(int num) {
        if (num < 60) {
            if (this.lastTwoMoves((byte) 1)) {
                this.setMove((byte) 2);
            } else {
                this.setMove((byte) 1, this.damage.get(0).base);
            }
        } else if (this.lastMove((byte) 2)) {
            this.setMove((byte) 1, this.damage.get(0).base);
        } else {
            this.setMove((byte) 2);
        }
    }

}
