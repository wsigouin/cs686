package STSSimulator.actors.monsters.exordium;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class GremlinThiefAP extends AbstractMonsterAP {
    public int thiefDamage;

    public GremlinThiefAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    @Override
    public void takeTurn() {
        if (AbstractDungeon.ascensionLevel >= 2) {
            this.thiefDamage = 10;
        } else {
            this.thiefDamage = 9;
        }
    }

    @Override
    public void getMove(int num) {
        switch (this.nextMove) {
            case 1:
                SimulatorUtility.doDamage(this.damage.get(0), state.player);
                if (!this.escapeNext) {
                    this.setMove(null, (byte) 1, this.thiefDamage);
                } else {
                    this.setMove(null, (byte) 99);
                }
                break;
            case 99:
                this.escaped = true;
        }
    }

}
