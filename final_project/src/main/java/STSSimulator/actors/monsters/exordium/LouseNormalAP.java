package STSSimulator.actors.monsters.exordium;

import STSSimulator.DamageInfoAP;
import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.powers.StrengthPowerAP;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class LouseNormalAP extends AbstractMonsterAP {
    public LouseNormalAP(AbstractMonster copy, GameState state) {
        super(copy, state);
    }

    @Override
    public void takeTurn() {
        switch (this.nextMove) {
            case 3:
                SimulatorUtility.doDamage(new DamageInfoAP(this, this.damage.get(0).base, DamageInfo.DamageType.NORMAL), state.player);
                break;
            case 4:
                SimulatorUtility.applyPower(this, this, new StrengthPowerAP(this, 3, state), 3, state);
        }
        this.rollMove();
    }

    @Override
    public void getMove(int num) {
        if (num < 25) {
            if (this.lastTwoMoves((byte) 4)) {
                this.setMove((byte) 3, this.damage.get(0).base);
            } else {
                this.setMove((byte) 4);
            }
        } else if (this.lastTwoMoves((byte) 3)) {
            this.setMove((byte) 4);
        } else {
            this.setMove((byte) 3, this.damage.get(0).base);
        }

    }

}
