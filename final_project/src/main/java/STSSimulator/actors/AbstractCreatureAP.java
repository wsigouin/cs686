package STSSimulator.actors;

import STSSimulator.GameState;
import STSSimulator.SimulatorUtility;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.cards.red.DarkEmbraceAP;
import STSSimulator.powers.*;
import com.megacrit.cardcrawl.cards.red.DarkEmbrace;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.powers.*;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class AbstractCreatureAP extends AbstractCreature {
    public GameState state;
    public ArrayList<AbstractPowerAP> powers = new ArrayList<>();

    public AbstractCreatureAP(String id, int maxHealth, GameState state) {
        this.id = id;
        this.name = id;
        this.state = state;
        this.currentHealth = maxHealth;
        this.maxHealth = maxHealth;
    }


    public AbstractCreatureAP(AbstractCreature copy, GameState state) {
        this.state = state;
        this.name = copy.name;
        this.currentBlock = copy.currentBlock;
        this.currentHealth = copy.currentHealth;
        this.maxHealth = copy.maxHealth;

        convertPowers(copy.powers, state);
    }


    public void convertPowers(ArrayList<AbstractPower> oldPowers, GameState state) {
        ArrayList<AbstractPowerAP> newPowers = new ArrayList<>();
        for (AbstractPower oldPower : oldPowers) {
            if (oldPower instanceof WeakPower) {
                newPowers.add(new WeakPowerAP(oldPower, this, state));
            } else if (oldPower instanceof CurlUpPower) {
                newPowers.add(new CurlUpPowerAP(oldPower, this, state));
            }else if (oldPower instanceof BerserkPower) {
                newPowers.add(new BerserkPowerAP(oldPower, this, state));
            }  else if (oldPower instanceof BrutalityPower) {
                newPowers.add(new BrutalityPowerAP(oldPower, this, state));
            }else if (oldPower instanceof DarkEmbracePower) {
                newPowers.add(new DarkEmbracePowerAP(oldPower, this, state));
            }else if (oldPower instanceof CombustPower) {
                newPowers.add(new CombustPowerAP(oldPower, this, state));
            }else if (oldPower instanceof DemonFormPower) {
                newPowers.add(new DemonFormPowerAP(oldPower, this, state));
            }else if (oldPower instanceof VulnerablePower) {
                newPowers.add(new VulnerablePowerAP(oldPower, this, state));
            } else if (oldPower instanceof StrengthPower) {
                newPowers.add(new StrengthPowerAP(oldPower, this, state));
            } else if (oldPower instanceof AngerPower) {
                newPowers.add(new AngerPowerAP(oldPower, this, state));
            }else if (oldPower instanceof CorruptionPower) {
                newPowers.add(new CorruptionPowerAP(oldPower, this, state));
            } else if (oldPower instanceof DexterityPower) {
                newPowers.add(new DexterityPowerAP(oldPower, this, state));
            } else if (oldPower instanceof EvolvePower) {
                newPowers.add(new EvolvePowerAP(oldPower, this, state));
            } else if (oldPower instanceof FeelNoPainPower) {
                newPowers.add(new FeelNoPainAP(oldPower, this, state));
            } else if (oldPower instanceof FireBreathingPower) {
                newPowers.add(new FireBreathingPowerAP(oldPower, this, state));
            } else if (oldPower instanceof FlameBarrierPower) {
                newPowers.add(new FlameBarrierPowerAP(oldPower, this, state));
            } else if (oldPower instanceof JuggernautPower) {
                newPowers.add(new JuggernautPowerAP(oldPower, this, state));
            } else if (oldPower instanceof LoseStrengthPower) {
                newPowers.add(new LoseStrengthPowerAP(oldPower, this, state));
            } else if (oldPower instanceof NoDrawPower) {
                newPowers.add(new NoDrawPowerAP(oldPower, this, state));
            }else if (oldPower instanceof FrailPower) {
                newPowers.add(new FrailPowerAP(oldPower, this, state));
            } else if (oldPower instanceof RitualPower) {
                newPowers.add(new RitualPowerAP(oldPower, this, state));
            } else if (oldPower instanceof SplitPower) {
                newPowers.add(new SplitPowerAP(oldPower, this, state));
            } else if (oldPower instanceof EntanglePower) {
                newPowers.add(new EntanglePowerAP(oldPower, this, state));
            } else if (oldPower instanceof MetallicizePower) {
                newPowers.add(new MetallicizePowerAP(oldPower, this, state));
            } else if (oldPower instanceof SharpHidePower) {
                newPowers.add(new SharpHidePowerAP(oldPower, this, state));
            } else if (oldPower instanceof SporeCloudPower) {
                newPowers.add(new SporeCloudPowerAP(oldPower, this, state));
            } else if (oldPower instanceof ArtifactPower) {
                newPowers.add(new ArtifactPowerAP(oldPower, this, state));
            } else if (oldPower instanceof ThieveryPower) {
                //Irrelevant Power
            } else {
                System.out.println("----" + oldPower.name + " POWER NOT IMPLEMENTED YET -----");
            }
        }
        this.powers = newPowers;
    }

    @Override
    public boolean isDeadOrEscaped() {
        if (!this.isDead && this.currentHealth > 0) {
            if (!this.isPlayer) {
                AbstractMonsterAP m = (AbstractMonsterAP) this;
                if (m.isEscaping || m.escaped) {
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }

    public void checkForPowerRemoval() {
        ArrayList<AbstractPowerAP> powers = new ArrayList<>(this.powers);
        Iterator var1 = powers.iterator();
        while (var1.hasNext()) {
            AbstractPowerAP p = (AbstractPowerAP) var1.next();

            if (p.removed) {
                SimulatorUtility.removePower(p.owner, p.owner,p.ID, p, state);

            }
        }

        for (int i = 0; i < powers.size(); i++) {
            if (powers.get(i).removed) {
                if (SimulatorUtility.removePower(powers.get(i).owner, powers.get(i).owner, powers.get(i).ID, powers.get(i), state)) {
                    i--;
                }
            }
        }
    }

    @Override
    public void applyStartOfTurnPowers() {
        ArrayList<AbstractPowerAP> powers = new ArrayList<>(this.powers);
        Iterator var1 = powers.iterator();
        while (var1.hasNext()) {
            AbstractPowerAP p = (AbstractPowerAP) var1.next();
            p.atStartOfTurn();
        }
        this.checkForPowerRemoval();

    }

    @Override
    public void applyTurnPowers() {
        ArrayList<AbstractPowerAP> powers = new ArrayList<>(this.powers);
        Iterator var1 = powers.iterator();
        while (var1.hasNext()) {
            AbstractPowerAP p = (AbstractPowerAP) var1.next();
            p.duringTurn();
        }
        this.checkForPowerRemoval();

    }

    @Override
    public void applyStartOfTurnPostDrawPowers() {
        ArrayList<AbstractPowerAP> powers = new ArrayList<>(this.powers);
        Iterator var1 = powers.iterator();
        while (var1.hasNext()) {
            AbstractPowerAP p = (AbstractPowerAP) var1.next();
            p.atStartOfTurnPostDraw();
        }
        this.checkForPowerRemoval();

    }

    @Override
    public void applyEndOfTurnTriggers() {
        ArrayList<AbstractPowerAP> powers = new ArrayList<>(this.powers);
        Iterator var1 = powers.iterator();

        while (var1.hasNext()) {
            AbstractPowerAP p = (AbstractPowerAP) var1.next();
            p.atEndOfTurn(this.isPlayer);
        }

        this.checkForPowerRemoval();
    }

    public AbstractPowerAP getPower(String targetID) {
        ArrayList<AbstractPowerAP> powers = new ArrayList<>(this.powers);
        Iterator var1 = powers.iterator();

        AbstractPowerAP p;
        do {
            if (!var1.hasNext()) {
                return null;
            }

            p = (AbstractPowerAP) var1.next();
        } while (!p.ID.equals(targetID));

        return p;
    }


}
