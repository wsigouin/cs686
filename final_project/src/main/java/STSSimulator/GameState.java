package STSSimulator;

import MonteCarlo.MCAction;
import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import STSSimulator.actors.monsters.MonsterGroupAP;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.powers.AbstractPowerAP;
import STSSimulator.relics.AbstractRelicAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.MonsterGroup;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.rits.cloning.Cloner;

import java.util.ArrayList;

public class GameState implements Comparable {
    public MonsterGroupAP monsters;
    public AbstractPlayerAP player;
    public static Cloner cloner = new Cloner();
    public int hpLossThisCombat = 0;
    public int damageReceivedThisTurn;
    public int damageReceivedThisCombat;
    public AbstractCardAP cardPlayed;
    public boolean isDone = false;
    public AbstractCreatureAP target = null;

    public GameState() {
    }

    public GameState(AbstractPlayerAP player, MonsterGroupAP monsters) {
        this.player = player;
        this.monsters = monsters;
    }

    public GameState(AbstractPlayer player, MonsterGroup monsters) throws NoSuchFieldException, IllegalAccessException {
        this.player = new AbstractPlayerAP(player, this);
        this.monsters = new MonsterGroupAP(monsters, this);
    }

    public GameState deepClone() {

        GameState state = cloner.deepClone(this);

        state.player.state = state;
        state.player.drawPile.shuffle();
        state.monsters.state = state;
        for (AbstractPower power : state.player.powers) {
            ((AbstractPowerAP) power).state = state;
        }

        for (AbstractRelicAP relic : state.player.relics) {
            relic.state = state;
        }

        for (AbstractCardAP card : state.player.hand.group) {
            card.state = state;
        }

        for (AbstractCardAP card : state.player.discardPile.group) {
            card.state = state;
        }

        for (AbstractCardAP card : state.player.exhaustPile.group) {
            card.state = state;
        }

        for (AbstractCardAP card : state.player.masterDeck.group) {
            card.state = state;
        }

        for (AbstractCardAP card : state.player.drawPile.group) {
            card.state = state;
        }

        for (AbstractMonsterAP monster : state.monsters.monsters) {
            monster.state = state;
            for (AbstractPowerAP power : monster.powers) {
                power.state = state;
            }
        }

        return state;
    }

    public GameState applyAction(MCAction action) {
        GameState newState = this.deepClone();

        switch (action.action) {
            case PLAY_CARD:
                AbstractCardAP card = newState.player.hand.getPlayableCards().get(action.card);

                if (card.target == AbstractCard.CardTarget.ENEMY || card.target == AbstractCard.CardTarget.SELF_AND_ENEMY) {
                    newState.player.useCard(card, newState.monsters.getAliveMonsters().get(action.target));
                } else {
                    newState.player.useCard(card, null);
                }
                break;
            case END_TURN:
                newState.player.endTurn();
                break;
            case NONE:
                assert (false);
        }

        return newState;

    }

    //DEPRICATED
    public void expand() {
        if (this.isDone) {
            return;
        }
        GameState newState;
        ArrayList<AbstractCardAP> playableCardsMaster = this.player.hand.getPlayableCards();
        ArrayList<AbstractMonsterAP> aliveMonstersMaster = this.monsters.getAliveMonsters();

        for (int card_index = 0; card_index < playableCardsMaster.size(); card_index++) {
            //DANGER OLD LOGIC IF REUSING THIS
            if (playableCardsMaster.get(card_index).type == AbstractCard.CardType.ATTACK) {
                for (int monster_index = 0; monster_index < aliveMonstersMaster.size(); monster_index++) {
                    newState = this.deepClone();

                    AbstractCardAP card = newState.player.hand.getPlayableCards().get(card_index);
                    AbstractMonsterAP monster = newState.monsters.getAliveMonsters().get(monster_index);
                    newState.player.useCard(card, monster);
                    newState.isDone = SimulatorUtility.isCombatOver(newState);
                    newState.cardPlayed = card;
                    newState.target = monster;
                }
            } else {
                newState = this.deepClone();
                AbstractCardAP card = newState.player.hand.getPlayableCards().get(card_index);
                newState.player.useCard(card, null);
                newState.isDone = SimulatorUtility.isCombatOver(newState);
                newState.cardPlayed = card;
            }

        }

        newState = this.deepClone();
        newState.player.endTurn();
        newState.monsters.takeTurn();
        newState.isDone = SimulatorUtility.isCombatOver(newState);
        newState.cardPlayed = null;

        if (!newState.isDone) {
            newState.player.startTurn();
        }

    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append("Player\n");
        ret.append("Health: ").append(this.player.currentHealth).append("\n");
        ret.append("Energy: ").append(this.player.currEnergy).append("\n");
        ret.append("Block: ").append(this.player.currentBlock).append("\n");
        ret.append("Powers:\n");

        for (AbstractPowerAP power : this.player.powers) {
            ret.append("\t").append(power.name).append(" with ").append(power.amount).append(" stacks.\n");
        }
        ret.append("Hand: \n");
        for (AbstractCardAP card : this.player.hand.group) {
            ret.append("\t").append(card.name).append(" with ").append(card.costForTurn).append(" cost!\n");
        }

        ret.append("There are : ").append(this.monsters.monsters.size()).append(" monsters:\n");
        for (AbstractMonsterAP monster : this.monsters.monsters) {
            ret.append("\t").append(monster.name).append(" has ").append(monster.currentHealth).append(" health and ").append(monster.currentBlock).append(" block.\n");
            for (AbstractPowerAP power : monster.powers) {
                ret.append("\t\tHas power ").append(power.ID).append(" with ").append(power.amount).append(" stacks!\n");
            }
        }
        ret.append("Player dead: ").append(this.player.isDead).append("\n");


        return ret.toString();
    }


    @Override
    public int compareTo(Object o) {
        GameState otherState = (GameState) o;
        if (this.player.compareTo(otherState.player) != 0) {
            return -1;
        }

        if (this.monsters.compareTo(otherState.monsters) != 0) {
            return -1;
        }

        return 0;
    }
}
