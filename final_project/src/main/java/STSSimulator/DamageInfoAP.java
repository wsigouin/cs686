package STSSimulator;

import STSSimulator.actors.AbstractCreatureAP;
import STSSimulator.actors.AbstractPlayerAP;
import STSSimulator.actors.monsters.AbstractMonsterAP;
import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.powers.AbstractPower;

import java.util.Iterator;

public class DamageInfoAP extends DamageInfo {
    public AbstractCreatureAP owner;

    public DamageInfoAP(DamageInfo info, AbstractMonsterAP newOwner) {
        super(newOwner, info.base);
        this.owner = newOwner;
    }

    public DamageInfoAP(AbstractCreature owner, int base) {
        super(owner, base);
    }

    public DamageInfoAP(AbstractCreatureAP damageSource, int base, DamageType type) {
        super(damageSource, base, type);
        this.owner = damageSource;
    }

    public void applyPowers(AbstractMonsterAP owner, AbstractPlayerAP target) {
        this.output = this.base;
        this.isModified = false;
        float tmp = (float) this.output;
        AbstractPower p;
        Iterator var6;

        var6 = owner.powers.iterator();

        while (var6.hasNext()) {
            p = (AbstractPower) var6.next();
            tmp = p.atDamageGive(tmp, this.type);
            if (this.base != (int) tmp) {
                this.isModified = true;
            }
        }

        var6 = target.powers.iterator();

        while (var6.hasNext()) {
            p = (AbstractPower) var6.next();
            tmp = p.atDamageReceive(tmp, this.type);
            if (this.base != (int) tmp) {
                this.isModified = true;
            }
        }

        var6 = owner.powers.iterator();

        while (var6.hasNext()) {
            p = (AbstractPower) var6.next();
            tmp = p.atDamageFinalGive(tmp, this.type);
            if (this.base != (int) tmp) {
                this.isModified = true;
            }
        }

        var6 = target.powers.iterator();

        while (var6.hasNext()) {
            p = (AbstractPower) var6.next();
            tmp = p.atDamageFinalReceive(tmp, this.type);
            if (this.base != (int) tmp) {
                this.isModified = true;
            }
        }

        this.output = MathUtils.floor(tmp);
        if (this.output < 0) {
            this.output = 0;
        }

    }

}
