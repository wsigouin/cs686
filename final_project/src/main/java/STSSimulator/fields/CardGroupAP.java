package STSSimulator.fields;

import STSSimulator.GameState;
import STSSimulator.cards.AbstractCardAP;
import STSSimulator.cards.red.*;
import STSSimulator.cards.status.BurnAP;
import STSSimulator.cards.status.DazedAP;
import STSSimulator.cards.status.SlimedAP;
import STSSimulator.powers.AbstractPowerAP;
import STSSimulator.relics.AbstractRelicAP;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.cards.red.*;
import com.megacrit.cardcrawl.cards.status.Burn;
import com.megacrit.cardcrawl.cards.status.Dazed;
import com.megacrit.cardcrawl.cards.status.Slimed;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class CardGroupAP implements Comparable {
    GameState state;
    public ArrayList<AbstractCardAP> group = new ArrayList<>();

    public CardGroupAP(CardGroup g, GameState state) {
        this.state = state;

        AbstractCardAP newCard = null;

        for (AbstractCard c : g.group) {
            if (c instanceof Defend_Red) {
                newCard = new Defend_RedAP(c, state);
            } else if (c instanceof Strike_Red) {
                newCard = new Strike_RedAP(c, state);
            } else if (c instanceof PerfectedStrike) {
                newCard = new PerfectedStrikeAP(c, state);
            } else if (c instanceof Bash) {
                newCard = new BashAP(c, state);
            } else if (c instanceof Burn) {
                newCard = new BurnAP(c, state);
            } else if (c instanceof Dazed) {
                newCard = new DazedAP(c, state);
            } else if (c instanceof Slimed) {
                newCard = new SlimedAP(c, state);
            } else if (c instanceof Anger) {
                newCard = new AngerAP(c, state);
            } else if (c instanceof BattleTrance) {
                newCard = new BattleTranceAP(c, state);
            } else if (c instanceof Berserk) {
                newCard = new BerserkAP(c, state);
            } else if (c instanceof BloodForBlood) {
                newCard = new BloodForBloodAP(c, state);
            } else if (c instanceof Bloodletting) {
                newCard = new BloodlettingAP(c, state);
            } else if (c instanceof Bludgeon) {
                newCard = new BludgeonAP(c, state);
            } else if (c instanceof BodySlam) {
                newCard = new BodySlamAP(c, state);
            } else if (c instanceof Brutality) {
                newCard = new BrutalityAP(c, state);
            } else if (c instanceof Carnage) {
                newCard = new CarnageAP(c, state);
            } else if (c instanceof Clash) {
                newCard = new ClashAP(c, state);
            } else if (c instanceof Cleave) {
                newCard = new CleaveAP(c, state);
            } else if (c instanceof Clothesline) {
                newCard = new ClotheslineAP(c, state);
            } else if (c instanceof Combust) {
                newCard = new CombustAP(c, state);
            } else if (c instanceof Corruption) {
                newCard = new CorruptionAP(c, state);
            } else if (c instanceof DarkEmbrace) {
                newCard = new DarkEmbraceAP(c, state);
            } else if (c instanceof DemonForm) {
                newCard = new DemonFormAP(c, state);
            } else if (c instanceof Disarm) {
                newCard = new DisarmAP(c, state);
            } else if (c instanceof Dropkick) {
                newCard = new DropkickAP(c, state);
            } else if (c instanceof Entrench) {
                newCard = new EntrenchAP(c, state);
            } else if (c instanceof Evolve) {
                newCard = new EvolveAP(c, state);
            } else if (c instanceof Feed) {
                newCard = new FeedAP(c, state);
            } else if (c instanceof FeelNoPain) {
                newCard = new FeelNoPainAP(c, state);
            } else if (c instanceof FiendFire) {
                newCard = new FiendFireAP(c, state);
            } else if (c instanceof FireBreathing) {
                newCard = new FireBreathingAP(c, state);
            } else if (c instanceof Flex) {
                newCard = new FlexAP(c, state);
            } else if (c instanceof GhostlyArmor) {
                newCard = new GhostlyArmorAP(c, state);
            } else if (c instanceof HeavyBlade) {
                newCard = new HeavyBladeAP(c, state);
            } else if (c instanceof Hemokinesis) {
                newCard = new HemokinesisAP(c, state);
            } else if (c instanceof Immolate) {
                newCard = new ImmolateAP(c, state);
            } else if (c instanceof Impervious) {
                newCard = new ImperviousAP(c, state);
            } else if (c instanceof Inflame) {
                newCard = new InflameAP(c, state);
            } else if (c instanceof Intimidate) {
                newCard = new IntimidateAP(c, state);
            } else if (c instanceof IronWave) {
                newCard = new IronWaveAP(c, state);
            } else if (c instanceof Juggernaut) {
                newCard = new JuggernautAP(c, state);
            } else {
                System.out.println("--------------CARD NOT IMPLEMENTED YET--------");
            }


            this.group.add(newCard);
        }

    }

    public void triggerOnOtherCardPlayed(AbstractCardAP usedCard) {
        Iterator var2 = this.group.iterator();

        while (var2.hasNext()) {
            AbstractCardAP c = (AbstractCardAP) var2.next();
            if (c != usedCard) {
                c.triggerOnOtherCardPlayed(usedCard);
            }
        }

        var2 = state.player.powers.iterator();

        while (var2.hasNext()) {
            AbstractPowerAP p = (AbstractPowerAP) var2.next();
            p.onAfterCardPlayed(usedCard);
        }

    }

    public void removeCard(AbstractCardAP c) {
        this.group.remove(c);
    }

    public void addCard(AbstractCardAP c) {
        this.group.add(c);
    }

    public void applyPowers() {
        for (AbstractCardAP abstractCardAP : this.group) {
            AbstractCardAP c = abstractCardAP;
            c.applyPowers();
        }

    }

    public void moveToDiscardPile(AbstractCardAP c) {
        state.player.hand.group.remove(c);
        state.player.discardPile.group.add(c);
        state.player.onCardDrawOrDiscard();
    }

    public void empower(AbstractCardAP c) {
    }

    public void moveToExhaustPile(AbstractCardAP c) {
        Iterator var2 = state.player.relics.iterator();

        while (var2.hasNext()) {
            AbstractRelicAP r = (AbstractRelicAP) var2.next();
            r.onExhaust(c);
        }

        var2 = state.player.powers.iterator();

        while (var2.hasNext()) {
            AbstractPowerAP p = (AbstractPowerAP) var2.next();
            p.onExhaust(c);
        }

        c.triggerOnExhaust();
        state.player.hand.group.remove(c);
        state.player.exhaustPile.group.add(c);
        state.player.onCardDrawOrDiscard();
    }

    public AbstractCardAP getTopCard() {
        return this.group.get(this.group.size() - 1);
    }

    public void removeTopCard() {
        this.group.remove(this.group.size() - 1);
    }

    public boolean isEmpty() {
        return group.isEmpty();
    }

    public int size() {
        return group.size();
    }

    public boolean hasCard(AbstractCardAP card) {
        for (AbstractCardAP c : this.group) {
            if (c.name.equals(card.name)) {
                return true;
            }
        }
        return false;
    }


    public ArrayList<AbstractCardAP> getPlayableCards() {
        ArrayList<AbstractCardAP> playableCards = new ArrayList<>();
        for (AbstractCardAP card : this.group) {
            if (card.canUse()) {
                playableCards.add(card);
            }
        }

        return playableCards;
    }

    public void shuffle() {
        Collections.shuffle(this.group, new java.util.Random(AbstractDungeon.shuffleRng.randomLong()));
    }


    @Override
    public int compareTo(Object o) {
        CardGroupAP otherGroup = (CardGroupAP) o;

        if (this.size() != otherGroup.size()) {
            return -1;
        }

        return 0;
    }
}
