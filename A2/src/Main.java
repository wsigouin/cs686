import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {

        int gainType;
        if (args.length <= 1) {
            gainType = 2;
        } else {
            gainType = Integer.parseInt(args[1]);
        }

        String featuresFileName = "datasets/trainLabel.txt";
        String trainDataFileName = "datasets/trainData.txt";
        String trainLabelsFileName = "datasets/trainLabel.txt";

        String testDataFileName = "datasets/testData.txt";
        String testLabelFileName = "datasets/testLabel.txt";

        String testResultsFileName = "testResults.txt";
        String trainResultsFileName = "trainResults.txt";


        File testResultsFile = new File(testResultsFileName);

        PrintWriter testOut = new PrintWriter(testResultsFile);


        File trainResultsFile = new File(trainResultsFileName);

        PrintWriter trainOut = new PrintWriter(trainResultsFile);



        File featuresFile = new File(featuresFileName);

        Scanner featuresIn = new Scanner(featuresFile);

        ArrayList<String> features = new ArrayList<>();

        System.out.println("Loading features...");

        while (featuresIn.hasNextLine()) {
            features.add(featuresIn.nextLine());
        }

        featuresIn.close();


        ArrayList<DataItem> trainItems = populateData(trainDataFileName, trainLabelsFileName);


        ArrayList<DataItem> testItems = populateData(testDataFileName, testLabelFileName);


        double numFirstClassifier = 0;

        for (DataItem item : trainItems) {
            if (item.classification == 1) {
                numFirstClassifier++;
            }
        }

        double initialEntropy = getEntropy(numFirstClassifier, trainItems.size());



        for(int maxIterations = 1; maxIterations <= 100; maxIterations++) {

            PriorityQueue<Node> queue = new PriorityQueue<>();

            Node root = new Node(-1, initialEntropy, 1 - initialEntropy);
            root.dataItems = trainItems;

            queue.add(root);

            System.out.println("Loading complete!");
            System.out.println("Building Tree...");
            Node currNode = queue.remove();

            for (int iteration = 0; iteration < maxIterations; iteration++) {

                Node newLeft = null;
                Node newRight = null;
                double bestInformationGain = -1;

                for (int i = 1; i <= features.size(); i++) {
                    double numTrueAndLabel = 0;
                    double numFalseAndLabel = 0;
                    ArrayList<DataItem> trueSet = new ArrayList<>();
                    ArrayList<DataItem> falseSet = new ArrayList<>();

                    for (DataItem datum : currNode.dataItems) {
                        if (datum.hasFeature(i)) {
                            if (datum.classification == 2) {
                                numTrueAndLabel++;
                            }
                            trueSet.add(datum);

                        } else {
                            if (datum.classification == 2) {
                                numFalseAndLabel++;
                            }
                            falseSet.add(datum);

                        }
                    }

                    double featureTrueEntropy = getEntropy(numTrueAndLabel, trueSet.size());

                    double featureFalseEntropy = getEntropy(numFalseAndLabel, falseSet.size());

                    double currentGain;

                    if (gainType == 1) {
                        currentGain = getInformationGainA(currNode.entropy, featureTrueEntropy, featureFalseEntropy);

                    } else {
                        currentGain = getInformationGainB(currNode.entropy, featureTrueEntropy, featureFalseEntropy, (double) trueSet.size() / currNode.dataItems.size(), (double) falseSet.size() / currNode.dataItems.size());
                    }

                    if (Double.isNaN(currentGain)) {
                        currentGain = 0;
                    }

                    if (currentGain > bestInformationGain) {
                        newRight = new Node(i, featureTrueEntropy, 1 - featureTrueEntropy, trueSet, numTrueAndLabel >= (trueSet.size() / 2) ? 2 : 1);
                        newRight.hasFeature = true;
                        newLeft = new Node(-1, featureFalseEntropy, 1 - featureFalseEntropy, falseSet, numFalseAndLabel >= (falseSet.size() / 2) ? 2 : 1);
                        bestInformationGain = currentGain;
                    }
                }

                assert (newLeft != null && newRight != null);

                currNode.left = newLeft;
                currNode.right = newRight;

                currNode.featureIndex = newRight.featureIndex;


                currNode.right.featureIndex = -1;

                queue.add(newLeft);
                queue.add(newRight);

                currNode = queue.remove();

                //if (currNode.informationGain == 1) break;

            }

            System.out.println("Tree Created...");
            //System.out.println(printTree(root));

            System.out.println("Testing...");
            testSet(root, trainItems);


            testOut.println(maxIterations +"\t" + testSet(root, testItems));
            trainOut.println(maxIterations +"\t" + testSet(root, trainItems));
            System.out.println(maxIterations + "%");
        }

        testOut.close();
        trainOut.close();


    }

    public static double log(double value) {
        if (value == 0) return 0;
        return Math.log(value) / Math.log(2);
    }

    public static double getEntropy(double numHas, double total) {
        double numNot = total - numHas;
        return -(numHas / total) * log(numHas / total) - (numNot / total) * log(numNot / total);
    }

    public static double getInformationGainA(double initialEntropy, double featureOneEntropy, double featureTwoEntropy) {
        return initialEntropy - (0.5 * featureOneEntropy + 0.5 * featureTwoEntropy);
    }

    public static double getInformationGainB(double initialEntropy, double featureTrueEntropy, double featureFalseEntropy, double featureTrueFraction, double featureFalseFraction) {
        return initialEntropy - (featureTrueFraction * featureTrueEntropy + featureFalseFraction * featureFalseEntropy);
    }

    public static String printTree(Node node) {
        if (node == null) return "";
        if (node.left == null && node.right == null) return "" + node.featureIndex + "[" + node.label + "]";
        return node.featureIndex + "[" + node.label + "]" + "(" + printTree(node.left) + ", " + printTree(node.right) + ")";
    }

    public static ArrayList<DataItem> populateData(String dataFileName, String labelFileName) throws FileNotFoundException {

        ArrayList<DataItem> items = new ArrayList<>();

        File dataFile = new File(dataFileName);
        File labelFile = new File(labelFileName);

        Scanner dataIn = new Scanner(dataFile);
        Scanner labelsIn = new Scanner(labelFile);

        int currItem = dataIn.nextInt();
        int currDatum = dataIn.nextInt();


        DataItem dataItem = new DataItem(labelsIn.nextInt());

        int currFeatureIndex = 1;

        while (true) {
            if (currItem == currFeatureIndex) {
                if (!dataItem.addFeature(currDatum)) {
                    System.err.println("Adding duplicate! Quitting...");
                    System.exit(1);
                }

                if (!dataIn.hasNextInt()) break;
                currItem = dataIn.nextInt();
                currDatum = dataIn.nextInt();
            } else {
                if (!labelsIn.hasNextInt()) break;
                items.add(dataItem);
                dataItem = new DataItem(labelsIn.nextInt());
                currFeatureIndex++;
            }

        }

        items.add(dataItem);
        dataIn.close();
        labelsIn.close();

        return items;
    }

    public static double testSet(Node root, ArrayList<DataItem> testItems){
        int testCorrect = 0;

        for (DataItem datum : testItems) {
            Node currNode = root;

            while (currNode.featureIndex != -1) {
                if (datum.hasFeature(currNode.featureIndex)) {
                    currNode = currNode.right;
                } else {
                    currNode = currNode.left;
                }
            }

            if (currNode.label == datum.classification) {
                testCorrect++;
            }
        }

        return (double)testCorrect / testItems.size() * 100;
    }


}
