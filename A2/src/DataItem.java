import java.util.HashMap;

public class DataItem {
    public int classification;
    private HashMap<Integer, Boolean> features;

    public DataItem(int classification){
        this.classification = classification;
        features = new HashMap<>();
    }

    public boolean hasFeature(int featureIndex){
        return features.containsKey(featureIndex);
    }

    public boolean addFeature(int featureIndex){
        if(features.containsKey(featureIndex)){
            return false;
        }
        else {
            features.put(featureIndex, true);
            return true;
        }
    }


}
