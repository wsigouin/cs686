import java.util.ArrayList;

public class Node implements Comparable<Node> {
    public int featureIndex;
    public boolean hasFeature;
    public double informationGain;
    public double entropy;
    public int label;
    public Node left;
    public Node right;
    ArrayList<DataItem> dataItems;

    public Node(int featureIndex) {
        this.featureIndex = featureIndex;

        left = null;
        right = null;
        dataItems = new ArrayList<>();
    }

    public Node(int featureIndex, double entropy, double informationGain) {
        this.featureIndex = featureIndex;
        this.informationGain = informationGain;
        this.entropy = entropy;

        left = null;
        right = null;
        dataItems = new ArrayList<>();
    }


    public Node(int featureIndex, double entropy, double informationGain, ArrayList<DataItem> dataItems, int label) {
        this.featureIndex = featureIndex;
        this.informationGain = informationGain;
        this.dataItems = dataItems;
        this.entropy = entropy;
        this.label = label;

        left = null;
        right = null;
    }



    @Override
    public int compareTo(Node node) {
        return this.entropy < node.entropy ? 1 : -1;
    }
}
