import re
import variable_elimination as ve
import copy
import random as rand

f0_original = [
    ["Disease", "Value"],
    [0, 0.5],
    [1, 0.25],
    [2, 0.25]
]

f1_original = [
    ["Trim", "Value"],
    [0, 0.9],
    [1, 0.1]
]

f2_original = [
    ["Disease", "For", "Value"],
    [0, 1, 0.01],
    [0, 0, 0.99],
    [1, 1, 0.8],
    [1, 0, 0.2],
    [2, 1, 0.2],
    [2, 0, 0.8]
]

f3_original = [
    ["Disease", "Deg", "Value"],
    [0, 1, 0.01],
    [0, 0, 0.99],
    [1, 1, 0.2],
    [1, 0, 0.8],
    [2, 1, 0.8],
    [2, 0, 0.2]
]

f4_original = [
    ["Disease", "Trim", "Slop", "Value"],
    [0, 1, 1, 0.01],
    [0, 1, 0, 0.99],
    [0, 0, 1, 0.01],
    [0, 0, 0,  0.99],
    [1, 1, 1, 0.01],
    [1, 1, 0, 0.99],
    [1, 0, 1, 0.8],
    [1, 0, 0, 0.2],
    [2, 1, 1, 0.01],
    [2, 1, 0, 0.99],
    [2, 0, 1, 0.8],
    [2, 0, 0, 0.2]
]

f0 = []
f1 = []
f2 = []
f3 = []
f4 = []


slop_index = 0
for_index = 1
deg_index = 2
trim_index = 3
disease_index = 4
value_index = 5

slop_range = 2
for_range = 2
deg_range = 2
trim_range = 2


def reset_factors():
    global f0
    global f1
    global f2
    global f3
    global f4

    f0 = copy.deepcopy(f0_original)
    f1 = copy.deepcopy(f1_original)
    f2 = copy.deepcopy(f2_original)
    f3 = copy.deepcopy(f3_original)
    f4 = copy.deepcopy(f4_original)


def randomize_factors(x):
    rand.uniform(0, x)

    rand1 = rand.uniform(0, x)
    rand2 = rand.uniform(0, x)
    rand3 = rand.uniform(0, x)
    f0[1][-1] = (f0[1][-1] + rand1)
    f0[2][-1] = (f0[2][-1] + rand2)
    f0[3][-1] = (f0[3][-1] + rand3)
    f0[1][-1] = f0[1][-1] / (f0[1][-1] + f0[2][-1] + f0[3][-1])
    f0[2][-1] = f0[2][-1] / (f0[1][-1] + f0[2][-1] + f0[3][-1])
    f0[3][-1] = f0[3][-1] / (f0[1][-1] + f0[2][-1] + f0[3][-1])

    other_factors = [f1, f2, f3, f4]
    for factor in other_factors:
        for i in range(int((len(factor) - 1)/2)):
            index = i * 2 + 1
            rand1 = rand.uniform(0, x)
            rand2 = rand.uniform(0, x)
            factor[index][-1] = (factor[index][-1] + rand1)
            factor[index + 1][-1] = (factor[index + 1][-1] + rand2)

            factor[index][-1] = factor[index][-1] / (factor[index + 1][-1] + factor[index][-1])
            factor[index + 1][-1] = factor[index + 1][-1] / (factor[index + 1][-1] + factor[index][-1])


def expectation():
    table = []
    file = open('datasets/traindata.txt', 'r')
    master_table = compute_probabilities()
    for line in file:
        vals = list(map(lambda x: int(x), re.findall(r'-?\d+', line)))

        results_to_add = list(filter(lambda x: x[:-3] == vals[:-1], master_table))
        assert(len(results_to_add) == 3)
        table.extend(results_to_add)

    assert len(table) == 6000
    assert(round(sum(list(map(lambda x: x[-1], table)))) == 2000)

    return table


def compute_probabilities():
    hidden_vars = ["Disease", "Trim", "For", "Deg", "Slop"]
    query_vars = ["Disease"]

    response = []
    for s in range(slop_range):
        for f in range(for_range):
            for d in range(deg_range):
                for t in range(trim_range):
                    factors = [f0, f1, f2, f3, f4]

                    evidences = [
                        ["Slop", s],
                        ["For", f],
                        ["Deg", d],
                        ["Trim", t]
                    ]

                    result = ve.inference(factors, query_vars, hidden_vars, evidences, False)
                    normalized_result = copy.deepcopy(result)
                    ve.normalize(normalized_result)
                    result = result[1:]
                    normalized_result = normalized_result[1:]

                    for i in range(len(result)):
                        result[i].append(normalized_result[i][len(normalized_result[i]) - 1])
                        response.append([s, f, d, t] + result[i])

    return response


def update_factors(table):
    for row in f0[1:]:
        row[-1] = sum(list(map(lambda x: x[value_index] if x[disease_index] == row[0] else 0, table))) \
            / sum(list(map(lambda x: x[value_index], table)))

    for row in f1[1:]:
        row[-1] = sum(list(map(lambda x: x[value_index] if x[trim_index] == row[0] else 0, table))) \
                 / sum(list(map(lambda x: x[value_index], table)))

    for row in f2[1:]:
        row[-1] = sum(list(map(lambda x: x[value_index] if x[disease_index] == row[0] and x[for_index] == row[1] else 0, table))) \
                 / sum(list(map(lambda x: x[value_index] if x[disease_index] == row[0] else 0, table)))

    for row in f3[1:]:
        row[-1] = sum(
            list(map(lambda x: x[value_index] if x[disease_index] == row[0] and x[deg_index] == row[1] else 0, table))) \
                 / sum(list(map(lambda x: x[value_index] if x[disease_index] == row[0] else 0, table)))

    for row in f4[1:]:
        row[-1] = sum(
            list(map(lambda x: x[value_index] if x[disease_index] == row[0] and x[trim_index] == row[1] and x[slop_index] == row [2] else 0, table))) \
                 / sum(list(map(lambda x: x[value_index] if x[disease_index] == row[0] and x[trim_index] == row[1] else 0, table)))


def test():
    total_correct = 0
    total = 0
    file = open('datasets/testdata.txt', 'r')

    hidden_vars = ["Disease", "Trim", "For", "Deg", "Slop"]
    query_vars = ["Disease"]
    for line in file:
        total += 1
        factors = [f0, f1, f2, f3, f4]
        vals = list(map(lambda x: int(x), re.findall(r'-?\d+', line)))

        evidences = [
            ["Slop", vals[0]],
            ["For", vals[1]],
            ["Deg", vals[2]],
            ["Trim", vals[3]]
        ]
        result = ve.inference(factors, query_vars, hidden_vars, evidences, True)[1:]

        curr_max = 0
        curr_index = - 1
        for i in range(len(result)):
            if result[i][-1] > curr_max:
                curr_max = result[i][1]
                curr_index = result[i][0]

        if curr_index == vals[4]:
            total_correct += 1
    return total_correct / total


def expectation_maximization(x):
    randomize_factors(x)
    result = [test()]
    old_total = -1
    for i in range(100):
        tab = expectation()
        total = sum(list(map(lambda row: row[-2], tab)))
        update_factors(tab)
        result.append(test())

        if abs(total - old_total) <= 0.01:
            break
        else:
            old_total = total

    return result


for i in range(20):
    ran_range = i * (4/20)
    print(ran_range)
    for j in range(20):
        reset_factors()
        print(expectation_maximization(ran_range))


