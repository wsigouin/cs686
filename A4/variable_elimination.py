##################################################################################################
# CS 686 Assignment 4
# William Sigouin
##################################################################################################


def restrict(factor, variable, value):
    """
    Restricts a factor to a specific value for a certain variable, eliminating the
    variable from the factor
    """
    if not contains_variable(factor, variable):
        return factor

    index = factor[0].index(variable)
    new_factor = list(filter(lambda x: x[index] == value, factor[1:]))
    new_factor.insert(0, factor[0])
    if len(factor[0]) >= 3:
        new_factor = list(map(lambda x: x[:index] + x[index+1:], new_factor))

    return new_factor


def multiply(factor1, factor2):
    """
    Multiplies two factors together in the most naive fashion, by multiplying each row from
    one factor with all those from another getting a result
    uses remove_contradictions to resolve issues with the naive implementation
    """
    new_factor = []
    temp = (factor1[0][:-1] + factor2[0][:-1])
    temp.append("Value")
    new_factor.append(temp)

    for factor1_row in factor1[1:]:
        for factor2_row in factor2[1:]:
            new_row = (factor1_row[:-1] + factor2_row[:-1])
            new_row.append(factor1_row[len(factor1_row) - 1] * factor2_row[len(factor2_row) - 1])
            new_factor.append(new_row)

    remove_contradictions(new_factor)
    return new_factor


def sum_out(factor, variable):
    """
    sums out a factor by summing all rows that are identical save for the variable
    in question, then removing that column
    """
    index = factor[0].index(variable)
    new_factor = [factor[0][:index] + factor[0][index+1:]]
    already_seen = []
    curr_row_index = 1

    while True:
        next_seen = factor[curr_row_index][:index] + factor[curr_row_index][index + 1:-1]

        if next_seen not in already_seen:
            total = \
                sum(
                    list(
                        map(lambda x: x[len(x) - 1],
                            filter(lambda x: x[:index] + x[index + 1:-1] == next_seen, factor))))

            already_seen.append(next_seen.copy())

            next_seen.append(total)
            new_factor.append(next_seen)

        curr_row_index += 1

        if curr_row_index >= len(factor):
            break

    return new_factor


def normalize(factor):
    """
    Normalizes a factor by summing all entries and dividing all entries
    by this sum
    """
    total = sum(list(map(lambda x: x[len(x) - 1], factor[1:])))

    for row in factor[1:]:
        row[len(row) - 1] = row[len(row) - 1] / total


def inference(factor_list, query_variables, ordered_list_of_hidden_variables, evidence_list, shouldNormalize):
    """
    primary function for variable elimination algorithm
    Carries out algorithm as follows:
        - Restrict all factors using the evidence variables
        - For each variable not in evidence or query:
            - Multiply all factors together that contain the variable
            - Sum out variable from these factors
            - Replace factors with new combined factor in master list
        - If the set contains > 1 factor, multiply factors together
        - Normalize final factor
    """

    evidence_vars = []
    irrelevant_factors = []

    # Restricts all factors using the evidence variables
    for factor in factor_list:
        for evidence in evidence_list:
            if evidence[0] not in evidence_vars:
                evidence_vars.append(evidence[0])

            if contains_variable(factor, evidence[0]):
                if len(factor[0]) > 2:
                    index = factor_list.index(factor)
                    factor_list.remove(factor)
                    factor = restrict(factor, evidence[0], evidence[1])
                    factor_list.insert(index, factor)
                else:
                    irrelevant_factors.append(factor)

    for factor in irrelevant_factors:
        factor_list.remove(factor)


    # For each variable not in evidence or query:
    for variable in ordered_list_of_hidden_variables:
        if variable in query_variables or variable in evidence_vars:
            continue

        new_factor = []
        relevant_factors = []
        for factor in factor_list:

            # Multiply all factors together that contain the variable
            if contains_variable(factor, variable):
                if not new_factor:
                    new_factor = factor
                else:
                    new_factor = multiply(new_factor, factor)
                relevant_factors.append(factor)
        # Sum out variable from these factors
        new_factor = sum_out(new_factor, variable)

        # Replace factors with new combined factor in master list
        for factor in relevant_factors:
            factor_list.remove(factor)
        factor_list.append(new_factor)

    # If the set contains > 1 factor, multiply factors together
    if len(factor_list) > 1:
        result = []
        for factor in factor_list:
            if not result:
                result = factor
            else:
                result = multiply(factor, result)
    else:
        result = factor_list[0]

    # Normalize final factor
    if shouldNormalize:
        normalize(result)

    return result


def remove_contradictions(factor):
    """
    removes duplicates and contradictory rows from the factor
    """
    while len(factor[0]) != len(set(factor[0])):
        found_item = -1
        for i in factor[0]:
            if factor[0].count(i) > 1:
                found_item = i
                break

        if found_item == -1:
            break

        first_index = factor[0].index(found_item)
        second_index = first_index + factor[0][first_index + 1:].index(found_item) + 1

        factor[0].pop(second_index)

        for item in factor[1:]:
            if item[first_index] != item[second_index]:
                factor.remove(item)
            else:
                item.pop(second_index)


def contains_variable(factor, variable):
    """
    checks if a factor contains a certain variable
    """
    return variable in factor[0]


def pretty_print_factor(factor):
    for i in factor:
        print(i)
