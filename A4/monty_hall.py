import random
import simulator as sim
import numpy as np

Q = np.array([
    [0, 0, 1, 0],
    [0, 0, 2, 0],
    [0, 0, 3, 0],
    [1, 1, 1, 0],
    [1, 1, 2, 0],
    [1, 1, 3, 0],
    [1, 2, 1, 0],
    [1, 2, 2, 0],
    [1, 2, 3, 0],
    [1, 3, 1, 0],
    [1, 3, 2, 0],
    [1, 3, 3, 0],
    [2, 1, 1, 0],
    [2, 1, 2, 0],
    [2, 1, 3, 0],
    [2, 2, 1, 0],
    [2, 2, 2, 0],
    [2, 2, 3, 0],
    [2, 3, 1, 0],
    [2, 3, 2, 0],
    [2, 3, 3, 0],
    [3, 1, 1, 0],
    [3, 1, 2, 0],
    [3, 1, 3, 0],
    [3, 2, 1, 0],
    [3, 2, 2, 0],
    [3, 2, 3, 0],
    [3, 3, 1, 0],
    [3, 3, 2, 0],
    [3, 3, 3, 0]
], dtype=float)

learning_rate = 0.1
discount_factor = 0.75


def get_best_action(s, learning):
    rand = random.randint(1, 10)
    Qp = np.array(list(filter(lambda x: x[0] == s[0] and x[1] == s[1], Q)))
    if rand == 10 and learning:
        return Qp[random.randint(0, len(Qp) - 1)][-2]
    else:
        return Qp[np.argmax(Qp[:, -1])][-2]


def learn():
    s = (0, 0)
    iterations = 1
    error = 0
    while True:
        a = get_best_action(s, True)
        (sp, r) = sim.simulator(s, a)
        index = np.where((Q[:, 0] == s[0]) & (Q[:, 1] == s[1]) & (Q[:, 2] == a))[0][0]
        second_part = (r + discount_factor *
            max(list(map(lambda row: row[-1], list(filter(lambda x: x[0] == sp[0] and x[1] == sp[1], Q))))))

        error += abs(second_part - Q[index][-1])

        Q[index][-1] = (1 - learning_rate) * Q[index][-1] + learning_rate * second_part

        if iterations % 1000 == 0:
            if (error/1000) < 0.1:
                break
            else:
                error = 0

        s = sp
        iterations += 1
    print(iterations)


def play():
    s = (0, 0)
    wins_count = 0
    r = 0
    for i in range(1000):
        for j in range(2):
            a = get_best_action(s, False)
            (sp, r) = sim.simulator(s, a)
            s = sp
        if r == 1:
            wins_count += 1
    print(wins_count)


print("Learning...")
learn()
print(Q)
print("Playing...")
play()

